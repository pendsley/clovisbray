package com.pendsley.clovisbray;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.pendsley.clovisbray.model.BungieErrorCode;
import com.pendsley.clovisbray.model.BungieResponse;
import com.pendsley.clovisbray.model.Manifest;
import com.pendsley.clovisbray.model.MobileWorldContentPaths;
import com.pendsley.clovisbray.network.BungieDestiny2Service;
import com.pendsley.clovisbray.operation.DeleteInventoryOperation;
import com.pendsley.clovisbray.persistence.dao.AccountOperation;
import com.pendsley.clovisbray.persistence.dao.ManifestOperation;
import com.pendsley.clovisbray.util.HandlerExecutor;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ManifestDownloadControllerTest {

    @Mock
    private BungieDestiny2Service destiny2Service;
    @Mock
    private ManifestOperation manifestOperation;
    @Mock
    private DeleteInventoryOperation deleteInventoryOperation;
    @Mock
    private File file;
    @Mock
    private ManifestDownloadController.ManifestDownloadListener listener;
    @Mock
    private ListeningExecutorService listeningExecutorService;
    @Mock
    private HandlerExecutor handlerExecutor;
    @Mock
    private ListenableFuture downloadFuture;
    @Mock
    private Call<BungieResponse<Manifest>> manifestCall;
    @Captor
    private ArgumentCaptor<Callback<BungieResponse<Manifest>>> manifestCallbackCaptor;
    @Captor
    private ArgumentCaptor<Runnable> downloadFutureListener;

    private ManifestDownloadController manifestDownloadController;

    @Before
    public void setUp() {

        when(destiny2Service.getManifest()).thenReturn(manifestCall);
        //noinspection unchecked
        when(listeningExecutorService.submit(any(Runnable.class))).thenReturn(downloadFuture);

        manifestDownloadController = new ManifestDownloadController(destiny2Service,
                manifestOperation,
                deleteInventoryOperation,
                listeningExecutorService,
                handlerExecutor);
    }

    @Test
    public void retrieveManifest_downloadsManifest_whenNoManifestOnDevice() {

        String newVersion = "123";
        when(manifestOperation.getLocalManifestVersion()).thenReturn(null);

        manifestDownloadController.retrieveManifest(file, listener);

        verify(manifestCall).enqueue(manifestCallbackCaptor.capture());
        manifestCallbackCaptor.getValue().onResponse(manifestCall,
                Response.success(createBungieResponse(newVersion, BungieErrorCode.SUCCESS.getErrorCode())));

        verify(downloadFuture).addListener(downloadFutureListener.capture(), eq(handlerExecutor));
        downloadFutureListener.getValue().run();

        verify(manifestOperation).deleteManifestInfo();
        verify(manifestOperation).insertManifestInfo(newVersion);
        verify(listener).onManifestDownloadFinished(null);
    }

    @Test
    public void retrieveManifest_doesNothing_whenManifestVersionMatchesExisting() {

        String newVersion = "123";
        when(manifestOperation.getLocalManifestVersion()).thenReturn(newVersion);

        manifestDownloadController.retrieveManifest(file, listener);

        verify(manifestCall).enqueue(manifestCallbackCaptor.capture());
        manifestCallbackCaptor.getValue().onResponse(manifestCall,
                Response.success(createBungieResponse(newVersion, BungieErrorCode.SUCCESS.getErrorCode())));

        verify(manifestOperation, never()).deleteManifestInfo();
        verify(listener).onManifestDownloadFinished(null);
    }

    @Test
    public void retrieveManifest_deletesLocalManifest_whenManifestVersionDoesNotMatchExisting() {

        String newVersion = "123";
        when(manifestOperation.getLocalManifestVersion()).thenReturn("456");

        manifestDownloadController.retrieveManifest(file, listener);

        verify(manifestCall).enqueue(manifestCallbackCaptor.capture());
        manifestCallbackCaptor.getValue().onResponse(manifestCall,
                Response.success(createBungieResponse(newVersion, BungieErrorCode.SUCCESS.getErrorCode())));

        verify(downloadFuture).addListener(downloadFutureListener.capture(), eq(handlerExecutor));
        downloadFutureListener.getValue().run();

        verify(manifestOperation).deleteManifestInfo();
        verify(manifestOperation).insertManifestInfo(newVersion);
        verify(listener).onManifestDownloadFinished(null);
    }

    @Test
    public void retrieveManifest_deletesCharacterInfoAndInventory_whenManifestVersionDoesNotMatchExisting() {

        String newVersion = "123";
        when(manifestOperation.getLocalManifestVersion()).thenReturn("456");

        manifestDownloadController.retrieveManifest(file, listener);

        verify(manifestCall).enqueue(manifestCallbackCaptor.capture());
        manifestCallbackCaptor.getValue().onResponse(manifestCall,
                Response.success(createBungieResponse(newVersion, BungieErrorCode.SUCCESS.getErrorCode())));

        verify(downloadFuture).addListener(downloadFutureListener.capture(), eq(handlerExecutor));
        downloadFutureListener.getValue().run();

        verify(deleteInventoryOperation).deleteAllItems();
    }

    @Test
    public void retrieveManifest_notifiesListenerWithError_whenWebCallFails() {

        manifestDownloadController.retrieveManifest(file, listener);

        verify(manifestCall).enqueue(manifestCallbackCaptor.capture());

        Throwable error = new RuntimeException();
        manifestCallbackCaptor.getValue().onFailure(manifestCall, error);

        verify(listener).onManifestDownloadFinished(error);
    }

    @Test
    public void retrieveManifest_notifiesListenerWithError_whenResponseIsNull() {

        manifestDownloadController.retrieveManifest(file, listener);

        verify(manifestCall).enqueue(manifestCallbackCaptor.capture());
        manifestCallbackCaptor.getValue().onResponse(manifestCall,
                Response.success(null));

        verify(listener).onManifestDownloadFinished(any(RuntimeException.class));
    }

    private BungieResponse<Manifest> createBungieResponse(String version,
                                                          int bungieErrorCode) {

        BungieResponse<Manifest> responseBody = new BungieResponse<>();
        responseBody.setErrorCode(bungieErrorCode);
        Manifest manifest = new Manifest();
        manifest.setVersion(version);
        MobileWorldContentPaths mobileWorldContentPaths = new MobileWorldContentPaths();
        mobileWorldContentPaths.setEn("en");
        manifest.setMobileWorldContentPaths(mobileWorldContentPaths);
        responseBody.setResponse(manifest);

        return responseBody;
    }
}