package com.pendsley.clovisbray.operation;

import com.pendsley.clovisbray.persistence.dao.ItemStatsDao;
import com.pendsley.clovisbray.persistence.dao.LocalInventoryDao;
import com.pendsley.clovisbray.persistence.dao.PerkDao;
import com.pendsley.clovisbray.persistence.dao.PerkOperation;
import com.pendsley.clovisbray.persistence.dao.SocketDao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class DeleteInventoryOperationTest {

    @Mock
    private LocalInventoryDao localInventoryDao;
    @Mock
    private PerkDao perkDao;
    @Mock
    private SocketDao socketDao;
    @Mock
    private ItemStatsDao itemStatsDao;

    private DeleteInventoryOperation deleteInventoryOperation;

    @Before
    public void setUp() {
        deleteInventoryOperation = new DeleteInventoryOperation(localInventoryDao, perkDao,
                socketDao, itemStatsDao);
    }

    @Test
    public void deleteAllItems_deletesEverything() {

        deleteInventoryOperation.deleteAllItems();

        verify(localInventoryDao).deleteAllItems();
        verify(perkDao).deleteAllPerks();
        verify(itemStatsDao).deleteAllStats();
        verify(socketDao).deleteAllSockets();
    }
}