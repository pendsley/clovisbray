package com.pendsley.clovisbray.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;

import com.pendsley.clovisbray.ClovisBrayApplication;
import com.pendsley.clovisbray.authentication.BungieAuthenticationActivity;
import com.pendsley.clovisbray.inject.AuthenticationModule;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Landing activity. Determines whether we should run the user through the authentication process,
 * or go straight to the main activity.
 *
 * @author Phil Endsley
 */
public class LoginActivity extends AppCompatActivity {

    @Inject
    @Named(AuthenticationModule.AUTHENTICATED)
    boolean authenticated;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((ClovisBrayApplication) getApplication()).getAuthenticationComponent().inject(this);

        Intent intent = new Intent(this, BungieAuthenticationActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
