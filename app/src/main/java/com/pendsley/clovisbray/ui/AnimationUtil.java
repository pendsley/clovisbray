package com.pendsley.clovisbray.ui;

import android.content.Context;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

/**
 * Util class for animations.
 *
 * @author Phil Endsley
 */
public class AnimationUtil {

    private AnimationUtil() {
    }

    public static LayoutAnimationController fastFadeInLayoutAnimationController(Context context) {
        Animation fadeIn = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
        fadeIn.setInterpolator(new FastOutSlowInInterpolator());
        return new LayoutAnimationController(fadeIn, .05f);
    }
}
