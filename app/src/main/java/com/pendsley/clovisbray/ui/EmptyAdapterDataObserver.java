package com.pendsley.clovisbray.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * {@link android.support.v7.widget.RecyclerView.AdapterDataObserver} that displays an empty view
 * when there are no items in the adapter. If there are items, the recycler view is displayed.
 *
 * @author Phil Endsley
 */
public class EmptyAdapterDataObserver extends RecyclerView.AdapterDataObserver {

    private final RecyclerView recyclerView;
    private final View emptyView;

    public EmptyAdapterDataObserver(RecyclerView recyclerView,
                                    View emptyView) {
        this.recyclerView = recyclerView;
        this.emptyView = emptyView;
    }

    @Override
    public void onChanged() {
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        if (adapter == null) {
            return;
        }

        if (adapter.getItemCount() == 0) {
            recyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }
}
