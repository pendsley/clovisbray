package com.pendsley.clovisbray;

import android.util.Log;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.pendsley.clovisbray.model.BungieErrorCode;
import com.pendsley.clovisbray.model.BungieResponse;
import com.pendsley.clovisbray.model.Manifest;
import com.pendsley.clovisbray.network.BungieDestiny2Service;
import com.pendsley.clovisbray.operation.DeleteInventoryOperation;
import com.pendsley.clovisbray.persistence.dao.AccountOperation;
import com.pendsley.clovisbray.persistence.dao.ManifestOperation;
import com.pendsley.clovisbray.util.HandlerExecutor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.Executors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Controller for downloading the Destiny Manifest.
 *
 * @author Phil Endsley
 */
public class ManifestDownloadController {

    private static final String TAG = "ManifestDownload";

    private final BungieDestiny2Service destiny2Service;
    private final ManifestOperation manifestOperation;
    private final DeleteInventoryOperation deleteInventoryOperation;
    private final ListeningExecutorService downloadExecutorService;
    private final HandlerExecutor handlerExecutor;

    @Inject
    ManifestDownloadController(BungieDestiny2Service destiny2Service,
                               ManifestOperation manifestOperation,
                               DeleteInventoryOperation deleteInventoryOperation) {
        this(destiny2Service, manifestOperation, deleteInventoryOperation,
                MoreExecutors.listeningDecorator(Executors.newSingleThreadExecutor()),
                new HandlerExecutor());
    }

    ManifestDownloadController(BungieDestiny2Service destiny2Service,
                               ManifestOperation manifestOperation,
                               DeleteInventoryOperation deleteInventoryOperation,
                               ListeningExecutorService downloadExecutorService,
                               HandlerExecutor handlerExecutor) {
        this.destiny2Service = destiny2Service;
        this.manifestOperation = manifestOperation;
        this.deleteInventoryOperation = deleteInventoryOperation;
        this.downloadExecutorService = downloadExecutorService;
        this.handlerExecutor = handlerExecutor;
    }

    void retrieveManifest(final File manifestDatabaseLocation,
                          final ManifestDownloadListener manifestDownloadListener) {

        updateProgress(manifestDownloadListener, R.string.checking_manifest_version, 0);
        destiny2Service.getManifest().enqueue(new Callback<BungieResponse<Manifest>>() {
            @Override
            public void onResponse(Call<BungieResponse<Manifest>> call, Response<BungieResponse<Manifest>> response) {

                BungieResponse<Manifest> bungieResponse = response.body();

                if (bungieResponse == null) {
                    manifestDownloadListener.onManifestDownloadFinished(new RuntimeException(BungieErrorCode.OTHER.getErrorMessage()));
                    return;
                }

                BungieErrorCode errorCode = BungieErrorCode.forCode(bungieResponse.getErrorCode());

                if (BungieErrorCode.SUCCESS == errorCode) {
                    Manifest manifest = bungieResponse.getResponse();

                    handleManifestResult(manifest, manifestDatabaseLocation, manifestDownloadListener);
                } else {
                    manifestDownloadListener.onManifestDownloadFinished(new RuntimeException(errorCode.getErrorMessage()));
                }
            }

            @Override
            public void onFailure(Call<BungieResponse<Manifest>> call, Throwable t) {
                Log.e(TAG, "Error while getting manifest details", t);
                manifestDownloadListener.onManifestDownloadFinished(t);
            }
        });
    }

    private void handleManifestResult(Manifest manifest,
                                      File manifestDatabaseLocation,
                                      final ManifestDownloadListener manifestDownloadListener) {

        // See if the manifest has changed compared to our previously downloaded version
        boolean manifestUpToDate = isManifestUpToDate(manifest.getVersion());
        if (!manifestUpToDate) {
            manifestOperation.deleteManifestInfo();
            manifestOperation.insertManifestInfo(manifest.getVersion());
            deleteInventoryOperation.deleteAllItems();
            final ListenableFuture downloadFuture = downloadManifest(manifest.getMobileWorldContentPaths().getEn(),
                    manifestDatabaseLocation,
                    manifestDownloadListener);

            downloadFuture.addListener(() -> manifestDownloadListener.onManifestDownloadFinished(null),
                    handlerExecutor);

            return;
        }

        manifestDownloadListener.onManifestDownloadFinished(null);

    }

    private boolean isManifestUpToDate(String remoteManifestVersion) {

        String localManifestVersion = manifestOperation.getLocalManifestVersion();

        return remoteManifestVersion.equals(localManifestVersion);
    }

    private ListenableFuture downloadManifest(final String relativeUrl,
                                              final File manifestDatabaseLocation,
                                              final ManifestDownloadListener manifestDownloadListener) {

        return downloadExecutorService.submit(() -> {

            try {
                updateProgress(manifestDownloadListener, R.string.downloading_manifest, 0);
                URL url = new URL("https://www.bungie.net" + relativeUrl);
                URLConnection connection = url.openConnection();

                connection.connect();

                ZipInputStream inputStream = new ZipInputStream(url.openStream());
                ZipEntry entry = inputStream.getNextEntry();
                long fileLength = entry.getSize();

                OutputStream outputStream = new FileOutputStream(manifestDatabaseLocation);

                byte data[] = new byte[1024];
                int currentLength;
                float total = 0;
                while ((currentLength = inputStream.read(data)) != -1) {
                    total += currentLength;
                    updateProgress(manifestDownloadListener, R.string.downloading_manifest, (int)((total / fileLength) * 100));
                    outputStream.write(data, 0, currentLength);

                }

                outputStream.flush();
                outputStream.close();
                inputStream.close();
            } catch (IOException e) {
                // Log and rethrow, so we fail appropriately
                Log.e(TAG, e.getLocalizedMessage(), e);
                throw new RuntimeException(e);
            }

        });

    }

    private void updateProgress(final ManifestDownloadListener manifestDownloadListener,
                                final int messageId,
                                final int progress) {
        handlerExecutor.execute(() -> manifestDownloadListener.manifestProgressChanged(messageId, progress));
    }

    public interface ManifestDownloadListener {

        void manifestProgressChanged(int messageId,
                                     int percentFinished);

        void onManifestDownloadFinished(Throwable t);
    }
}
