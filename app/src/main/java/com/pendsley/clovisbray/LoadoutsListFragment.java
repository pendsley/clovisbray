package com.pendsley.clovisbray;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.pendsley.clovisbray.authentication.ReinjectFieldsEvent;
import com.pendsley.clovisbray.model.Loadout;
import com.pendsley.clovisbray.presenter.LoadoutsListPresenter;
import com.pendsley.clovisbray.ui.AnimationUtil;
import com.pendsley.clovisbray.ui.DividerItemDecorator;
import com.pendsley.clovisbray.ui.EmptyAdapterDataObserver;
import com.pendsley.clovisbray.util.BusProvider;
import com.pendsley.clovisbray.viewcontroller.LoadoutsListViewController;
import com.squareup.otto.Subscribe;

import java.util.List;

import javax.inject.Inject;

import static android.support.design.widget.Snackbar.LENGTH_LONG;
import static android.support.design.widget.Snackbar.LENGTH_SHORT;

/**
 * Fragment for managing character loadouts.
 *
 * @author Phil Endsley
 */
public class LoadoutsListFragment extends Fragment implements LoadoutsListViewController {

    public static final String TAG = "LoadoutsListFragment";

    private static final String STATE_CHARACTER_ID = "characterId";

    @Inject
    LoadoutsListPresenter loadoutsListPresenter;

    @Inject
    BusProvider busProvider;

    private View root;
    private RecyclerView loadoutsRecyclerView;
    private View emptyView;

    private LoadoutAdapter loadoutAdapter;
    private EmptyAdapterDataObserver emptyAdapterDataObserver;

    private String characterId;

    public static LoadoutsListFragment create(String characterId) {
        Bundle arguments = new Bundle(1);
        arguments.putString(STATE_CHARACTER_ID, characterId);

        LoadoutsListFragment fragment = new LoadoutsListFragment();
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectMembers();

        setHasOptionsMenu(true);

        characterId = getArguments().getString(STATE_CHARACTER_ID);
    }

    @Override
    public void onResume() {
        super.onResume();
        busProvider.getBus().register(this);

        getActivity().setTitle(R.string.app_name);
    }

    @Override
    public void onPause() {
        super.onPause();

        busProvider.getBus().unregister(this);

        if (emptyAdapterDataObserver != null && loadoutAdapter != null && loadoutAdapter.hasObservers()){
            loadoutAdapter.unregisterAdapterDataObserver(emptyAdapterDataObserver);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        loadoutsListPresenter.dispose();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_loadouts, container, false);

        this.root = root;

        loadoutsRecyclerView = (RecyclerView) root.findViewById(R.id.loadouts_list);
        emptyView = root.findViewById(R.id.empty_view);

        Context context = getContext();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        loadoutsRecyclerView.setLayoutManager(layoutManager);
        loadoutsRecyclerView.addItemDecoration(new DividerItemDecorator(context));

        LayoutAnimationController layoutAnimationController =
                AnimationUtil.fastFadeInLayoutAnimationController(context);
        loadoutsRecyclerView.setLayoutAnimation(layoutAnimationController);

        final FloatingActionButton createLoadoutButton = (FloatingActionButton) root.findViewById(R.id.button_create_loadout);
        createLoadoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadoutsListPresenter.createLoadout(characterId);
            }
        });

        createLoadoutButton.post(new Runnable() {
            @Override
            public void run() {
                createLoadoutButton.requestLayout();
                createLoadoutButton.setVisibility(View.VISIBLE);
            }
        });

        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loadoutsListPresenter.setLoadoutsListViewController(this);

        loadoutsListPresenter.loadLoadoutsForCharacter(characterId);
    }

    @Override
    public void onActivityResult(int requestCode,
                                 int resultCode,
                                 Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (EditLoadoutActivity.REQUEST_CODE == requestCode) {
            loadoutsListPresenter.loadLoadoutsForCharacter(characterId);
        }

    }

    @Override
    public void displayLoadouts(List<Loadout> loadouts) {

        emptyAdapterDataObserver = new EmptyAdapterDataObserver(loadoutsRecyclerView, emptyView);

        loadoutAdapter = new LoadoutAdapter(loadouts);
        loadoutAdapter.registerAdapterDataObserver(emptyAdapterDataObserver);
        loadoutsRecyclerView.setAdapter(loadoutAdapter);
        loadoutAdapter.notifyDataSetChanged();
    }

    @Override
    public void showLoadoutDetails(Loadout loadout) {

        Intent intent = new Intent(getActivity(), EditLoadoutActivity.class);
        intent.putExtra(EditLoadoutActivity.STATE_LOADOUT, loadout);
        startActivityForResult(intent, EditLoadoutActivity.REQUEST_CODE);

    }

    @Override
    public void loadoutEquipped(Loadout loadout,
                                List<Throwable> errors) {
        if (errors.isEmpty()) {
            showInformationMessage(getString(R.string.loadout_equipped, loadout.getName()));
        } else {
            String errorMessage = Joiner.on(". ").join(Lists.transform(errors, new Function<Throwable, String>() {
                @Override
                public String apply(Throwable error) {
                    return error.getMessage();
                }
            }));
            showErrorMessage(errorMessage);
        }
    }

    private void showInformationMessage(String message) {
        showMessage(message, LENGTH_SHORT);
    }

    private void showErrorMessage(String errorMessage) {
        showMessage(errorMessage, LENGTH_LONG);
    }

    private void showMessage(String message,
                             int duration) {

        @SuppressWarnings("deprecation") // Have to use due to API restrictions
        int textColor = root.getContext().getResources().getColor(R.color.secondary_text);
        final Snackbar snackbar = Snackbar.make(root, message, duration)
                .setActionTextColor(textColor);
        snackbar.setAction(R.string.dismiss, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }

    @Subscribe
    public void onRefreshToken(ReinjectFieldsEvent event) {
        injectMembers();
    }

    private void injectMembers() {
        ((ClovisBrayApplication) getActivity().getApplication()).getApiComponent().inject(this);
    }

    private class LoadoutAdapter extends RecyclerView.Adapter<LoadoutNameViewHolder> {

        private final List<Loadout> loadouts;

        LoadoutAdapter(List<Loadout> loadouts) {
            this.loadouts = loadouts;
        }

        @Override
        public LoadoutNameViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_loadout_title, parent, false);

            return new LoadoutNameViewHolder(view);
        }

        @Override
        public void onBindViewHolder(LoadoutNameViewHolder holder, int position) {
            holder.bindData(loadouts.get(position));
        }

        @Override
        public int getItemCount() {
            return loadouts.size();
        }
    }

    private class LoadoutNameViewHolder extends RecyclerView.ViewHolder {

        private final TextView loadoutNameView;
        private final ImageView editLoadoutView;

        LoadoutNameViewHolder(View itemView) {
            super(itemView);

            loadoutNameView = (TextView) itemView.findViewById(R.id.loadout_name);
            editLoadoutView = (ImageView) itemView.findViewById(R.id.edit_loadout);
        }

        void bindData(final Loadout loadout) {

            loadoutNameView.setText(loadout.getName());

            loadoutNameView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loadoutsListPresenter.equipLoadout(loadout);
                }
            });
            editLoadoutView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loadoutsListPresenter.editLoadout(loadout);
                }
            });
        }
    }
}

