package com.pendsley.clovisbray;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.IntentCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pendsley.clovisbray.authentication.ReinjectFieldsEvent;
import com.pendsley.clovisbray.model.CharacterLoadoutsDrawerItem;
import com.pendsley.clovisbray.model.DrawerItem;
import com.pendsley.clovisbray.model.ItemClassType;
import com.pendsley.clovisbray.model.LocalInventoryItem;
import com.pendsley.clovisbray.network.BungieDestinyService;
import com.pendsley.clovisbray.presenter.InventoryListDrawerPresenter;
import com.pendsley.clovisbray.util.BusProvider;
import com.pendsley.clovisbray.viewcontroller.InventoryDrawerViewController;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

/**
 * Activity for inventory functionality.
 *
 * @author Phil Endsley
 */
public class InventoryListActivity extends AppCompatActivity implements
        InventoryDrawerViewController,
        InventoryListFragment.ItemSelectedListener {

    @Inject
    InventoryListDrawerPresenter inventoryListDrawerPresenter;

    @Inject
    BusProvider busProvider;

    private DrawerLayout drawerLayout;
    private RecyclerView drawer;
    private ActionBarDrawerToggle actionBarDrawerToggle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectMembers();

        setContentView(R.layout.activity_inventory_weapons);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        inventoryListDrawerPresenter.setInventoryDrawerViewController(this);

        bindInventoryDrawerViews();
    }

    private void bindInventoryDrawerViews() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer = (RecyclerView) findViewById(R.id.drawer);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        drawer.setLayoutManager(layoutManager);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this,
                drawerLayout,
                R.string.drawer_open,
                R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
        };

        drawerLayout.addDrawerListener(actionBarDrawerToggle);

        // We explicitly set the action bar before we call this method
        //noinspection ConstantConditions
        getSupportActionBar().setElevation(4);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        inventoryListDrawerPresenter.syncDrawerState();
        inventoryListDrawerPresenter.initializeInventoryDrawer();

        if (savedInstanceState == null) {

            InventoryListFragment fragment = new InventoryListFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.inventory_container, fragment, InventoryListFragment.TAG)
                    .commit();

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        busProvider.getBus().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        busProvider.getBus().unregister(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return actionBarDrawerToggle.onOptionsItemSelected(item) ||
                super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(LocalInventoryItem selectedItem) {
        showTalentGridFragment(selectedItem);
    }

    @Subscribe
    public void onRefreshToken(ReinjectFieldsEvent event) {
        injectMembers();
    }

    public void showLoadoutsForCharacter(String characterId) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.inventory_container, LoadoutsListFragment.create(characterId), LoadoutsListFragment.TAG)
                .addToBackStack(LoadoutsListFragment.TAG)
                .commit();
    }

    private void injectMembers() {
        ((ClovisBrayApplication) getApplication()).getApiComponent().inject(this);
    }

    private void showTalentGridFragment(LocalInventoryItem item) {

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.inventory_container, ItemDetailsFragment.create(item), ItemDetailsFragment.TAG)
                .addToBackStack(ItemDetailsFragment.TAG)
                .commit();
    }

    @Override
    public void syncDrawerState() {
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void closeDrawer() {
        drawerLayout.closeDrawers();
    }

    @Override
    public void initializeDrawerItems(List<DrawerItem> drawerItems) {

        InventoryDrawerAdapter adapter = new InventoryDrawerAdapter(drawerItems);
        drawer.setAdapter(adapter);
    }

    @Override
    public void navigateToInventoryList(ItemClassType itemClassType) {

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.inventory_container, InventoryListFragment.create(null, itemClassType), InventoryListFragment.TAG)
                .addToBackStack(InventoryListFragment.TAG)
                .commit();
    }

    @Override
    public void navigateToLoadoutsList(String characterId) {
        showLoadoutsForCharacter(characterId);
    }

    @Override
    public void navigateToPostmaster() {

        startActivity(new Intent(this, PostmasterActivity.class));
    }

    @Override
    public void navigateToFactions() {

        startActivity(new Intent(this, FactionsActivity.class));
    }

    @SuppressLint("WrongConstant")
    @Override
    public void navigateToPlatformSelection() {

        Intent intent = new Intent(this, PlatformSelectionActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }

    @Override
    public Context getContext() {
        return this;
    }

    private final class InventoryDrawerAdapter extends RecyclerView.Adapter<DrawerViewHolder> {

        private final List<DrawerItem> drawerItems;

        InventoryDrawerAdapter(List<DrawerItem> drawerItems) {
            this.drawerItems = drawerItems;
        }

        @Override
        public DrawerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_screen, parent, false);

            return new DrawerViewHolder(view);
        }

        @Override
        public void onBindViewHolder(DrawerViewHolder holder, int position) {
            holder.bindData(drawerItems.get(position));
        }

        @Override
        public int getItemCount() {
            return drawerItems.size();
        }

    }

    private final class DrawerViewHolder extends RecyclerView.ViewHolder {

        private final View root;
        private final ImageView backgroundView;
        private final TextView screenView;

        DrawerViewHolder(View itemView) {
            super(itemView);
            root = itemView;

            backgroundView = (ImageView) itemView.findViewById(R.id.character_background);
            screenView = (TextView) itemView.findViewById(R.id.description);
        }

        void bindData(final DrawerItem drawerItem) {
            screenView.setText(drawerItem.getScreenName());

            if (drawerItem instanceof CharacterLoadoutsDrawerItem) {
                String url = BungieDestinyService.BASE_URL + ((CharacterLoadoutsDrawerItem) drawerItem).getCharacter().getBackgroundPath();
                Picasso.with(backgroundView.getContext()).load(url).into(backgroundView);
            } else {
                backgroundView.setMaxHeight(100);
                backgroundView.setAdjustViewBounds(true);
                backgroundView.setImageDrawable(new ColorDrawable(Color.TRANSPARENT));
            }

            root.setOnClickListener(view -> inventoryListDrawerPresenter.drawerItemSelected(drawerItem));
        }
    }
}
