package com.pendsley.clovisbray;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LayoutAnimationController;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.common.base.Strings;
import com.pendsley.clovisbray.authentication.ReinjectFieldsEvent;
import com.pendsley.clovisbray.model.ItemSlot;
import com.pendsley.clovisbray.model.Loadout;
import com.pendsley.clovisbray.model.LocalInventoryItem;
import com.pendsley.clovisbray.presenter.LoadoutDetailsPresenter;
import com.pendsley.clovisbray.ui.AnimationUtil;
import com.pendsley.clovisbray.ui.DividerItemDecorator;
import com.pendsley.clovisbray.ui.EmptyAdapterDataObserver;
import com.pendsley.clovisbray.util.BusProvider;
import com.pendsley.clovisbray.viewcontroller.LoadoutDetailsViewController;
import com.squareup.otto.Subscribe;

import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

/**
 * Fragment for showing and modifying a single loadout's details.
 *
 * @author Phil Endsley
 */
public class LoadoutDetailsFragment extends Fragment implements LoadoutDetailsViewController {

    public static final String TAG = "LoadoutDetailsFragment";

    @Inject
    LoadoutDetailsPresenter loadoutDetailsPresenter;

    @Inject
    BusProvider busProvider;

    private View root;

    private EditText loadoutNameView;
    private RecyclerView loadoutItemsRecyclerView;
    private EmptyAdapterDataObserver emptyAdapterDataObserver;
    private View emptyView;
    private FloatingActionButton addItemSlotButton;
    private LoadoutItemsAdapter loadoutItemsAdapter;

    private LoadoutSlotSelectedListener loadoutSlotSelectedListener;
    private AddLoadoutSlotListener addLoadoutSlotListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectMembers();

        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity activity = getActivity();
        if (activity instanceof LoadoutSlotSelectedListener) {
            loadoutSlotSelectedListener = (LoadoutSlotSelectedListener) activity;
        }
        if (activity instanceof AddLoadoutSlotListener) {
            addLoadoutSlotListener = (AddLoadoutSlotListener) activity;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        busProvider.getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();

        busProvider.getBus().unregister(this);

        if (emptyAdapterDataObserver != null && loadoutItemsAdapter != null) {
            loadoutItemsAdapter.unregisterAdapterDataObserver(emptyAdapterDataObserver);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        loadoutDetailsPresenter.dispose();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_loadout_details, container, false);

        root = view;

        loadoutNameView = (EditText) view.findViewById(R.id.loadout_name);
        loadoutItemsRecyclerView = (RecyclerView) view.findViewById(R.id.loadout_items);
        emptyView = view.findViewById(R.id.empty_view);
        addItemSlotButton = (FloatingActionButton) view.findViewById(R.id.add_slot_button);

        Context context = getContext();

        RecyclerView.LayoutManager loadoutItemsLayoutManager = new LinearLayoutManager(context);
        loadoutItemsRecyclerView.setLayoutManager(loadoutItemsLayoutManager);
        loadoutItemsRecyclerView.addItemDecoration(new DividerItemDecorator(context));

        LayoutAnimationController animationController = AnimationUtil.fastFadeInLayoutAnimationController(context);
        loadoutItemsRecyclerView.setLayoutAnimation(animationController);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loadoutDetailsPresenter.setLoadoutDetailsViewController(this);

        loadoutDetailsPresenter.loadLoadout();

        addItemSlotButton.post(() -> addItemSlotButton.setVisibility(View.VISIBLE));

        addItemSlotButton.setOnClickListener(selectedView -> addLoadoutSlotListener.onAddLoadoutSlot());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_loadout_details, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.save_loadout:
                loadoutDetailsPresenter.saveLoadout();
                return true;
            case R.id.delete_loadout:
                loadoutDetailsPresenter.promptLoadoutDeletion();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void displayLoadout(final Loadout loadout,
                               List<LocalInventoryItem> loadoutItems) {
        loadoutNameView.setText(Strings.nullToEmpty(loadout.getName()));
        loadoutNameView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                loadout.setName(s.toString());
            }
        });

        emptyAdapterDataObserver = new EmptyAdapterDataObserver(loadoutItemsRecyclerView, emptyView);

        loadoutItemsAdapter = new LoadoutItemsAdapter(loadoutItems);
        loadoutItemsAdapter.registerAdapterDataObserver(emptyAdapterDataObserver);
        loadoutItemsRecyclerView.setAdapter(loadoutItemsAdapter);
        loadoutItemsAdapter.notifyDataSetChanged();
    }

    @Override
    public void clearLoadoutItemSlot(ItemSlot itemSlot) {

        loadoutItemsAdapter.removeItem(itemSlot);
    }

    @Override
    public void showDeleteLoadoutDialog() {
        new AlertDialog.Builder(getContext())
                .setTitle(R.string.delete_loadout_title)
                .setMessage(R.string.delete_loadout_message)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        loadoutDetailsPresenter.deleteLoadout();
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .show();
    }

    @Override
    public void showErrorMessage(int errorMessageRessource) {
        @SuppressWarnings("deprecation") // Have to use due to API restrictions
        final Snackbar snackbar = Snackbar.make(root, getString(errorMessageRessource), Snackbar.LENGTH_LONG)
                .setActionTextColor(root.getContext().getResources().getColor(R.color.secondary_text));
        snackbar.setAction(R.string.dismiss, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }

    @Override
    public void leaveLoadoutDetails() {
        getActivity().finish();
    }

    @Subscribe
    public void onRefreshToken(ReinjectFieldsEvent event) {
        injectMembers();
    }

    private void injectMembers() {
        ((ClovisBrayApplication) getActivity().getApplication()).getApiComponent().inject(this);
    }

    private class LoadoutItemsAdapter extends RecyclerView.Adapter<LoadoutItemViewHolder> {

        private final List<LocalInventoryItem> loadoutItems;

        LoadoutItemsAdapter(List<LocalInventoryItem> loadoutItems) {
            this.loadoutItems = loadoutItems;
        }

        @Override
        public LoadoutItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_loadout_item, parent, false);
            return new LoadoutItemViewHolder(root);
        }

        @Override
        public void onBindViewHolder(LoadoutItemViewHolder holder, int position) {
            holder.bindData(loadoutItems.get(position));
        }

        @Override
        public int getItemCount() {
            return loadoutItems.size();
        }

        void removeItem(ItemSlot itemSlot) {
            Iterator<LocalInventoryItem> loadoutItemsIterator = loadoutItems.iterator();
            while (loadoutItemsIterator.hasNext()) {
                LocalInventoryItem item = loadoutItemsIterator.next();

                if (item.getItemType().equalsIgnoreCase(itemSlot.getDescription())) {
                    loadoutItemsIterator.remove();
                    break;
                }
            }

            notifyDataSetChanged();
        }

    }

    private class LoadoutItemViewHolder extends RecyclerView.ViewHolder {

        private final View root;
        private final TextView categoryNameView;
        private final TextView itemNameView;
        private final TextView itemTypeView;
        private final TextView itemLightView;
        private final ImageButton clearButton;

        LoadoutItemViewHolder(View itemView) {
            super(itemView);

            root = itemView;

            categoryNameView = (TextView) root.findViewById(R.id.item_category);

            itemNameView = (TextView) root.findViewById(R.id.weapon_name_view);
            itemTypeView = (TextView) root.findViewById(R.id.weapon_type_view);
            itemLightView = (TextView) root.findViewById(R.id.weapon_light);
            clearButton = (ImageButton) root.findViewById(R.id.clear_weapon);
        }

        void bindData(final LocalInventoryItem inventoryItem) {

            itemNameView.setText(inventoryItem.getName());
            itemTypeView.setText(inventoryItem.getItemTypeName());
            itemLightView.setText(String.valueOf(inventoryItem.getLight()));
            clearButton.setVisibility(View.VISIBLE);

            categoryNameView.setText(inventoryItem.getItemType());

            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loadoutSlotSelectedListener.onSlotSelected(inventoryItem.getItemType());
                }
            });

            clearButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loadoutDetailsPresenter.clearLoadoutSlot(ItemSlot.fromDescription(inventoryItem.getItemType()));
                }
            });
        }

    }

    interface LoadoutSlotSelectedListener {

        void onSlotSelected(String categoryName);
    }

    interface AddLoadoutSlotListener {

        void onAddLoadoutSlot();
    }

}
