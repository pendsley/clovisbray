package com.pendsley.clovisbray;

import android.graphics.PorterDuff;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * View controller for downloading the manifest.
 *
 * @author Phil Endsleoy
 */
class ManifestDownloadViewController {

    private final TextView progressText;
    private final ProgressBar progressBar;
    private final TextView errorMessageView;
    private final Button retryButton;

    ManifestDownloadViewController(final InitialSyncActivity initialSyncActivity) {

        progressText = (TextView) initialSyncActivity.findViewById(R.id.progress_text);
        progressBar = (ProgressBar) initialSyncActivity.findViewById(R.id.manifest_progress_bar);
        progressBar.setIndeterminate(false);
        progressBar.getProgressDrawable()
                .setColorFilter(initialSyncActivity.getResources().getColor(R.color.primary_light),
                                PorterDuff.Mode.SRC_ATOP);
        errorMessageView = (TextView) initialSyncActivity.findViewById(R.id.error);
        retryButton = (Button) initialSyncActivity.findViewById(R.id.retry_button);
        retryButton.setOnClickListener(v -> initialSyncActivity.startDownload());
    }

    void showProgressBar(boolean showProgressBar) {
        if (showProgressBar) {
            progressBar.setVisibility(View.VISIBLE);
            progressText.setVisibility(View.VISIBLE);
            errorMessageView.setVisibility(View.GONE);
            retryButton.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            progressText.setVisibility(View.GONE);
            errorMessageView.setVisibility(View.VISIBLE);
            retryButton.setVisibility(View.VISIBLE);
        }
    }

    void updateProgress(int messageId,
                        int percentComplete) {
        progressText.setText(messageId);
        progressBar.setProgress(percentComplete);
    }

}
