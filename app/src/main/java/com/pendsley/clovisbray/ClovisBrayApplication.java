package com.pendsley.clovisbray;

import android.app.Activity;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.pendsley.clovisbray.authentication.ReinjectFieldsEvent;
import com.pendsley.clovisbray.authentication.persistence.ClovisBrayDbHelper;
import com.pendsley.clovisbray.authentication.services.TokenRefreshReceiver;
import com.pendsley.clovisbray.inject.AccountModule;
import com.pendsley.clovisbray.inject.AppModule;
import com.pendsley.clovisbray.inject.AuthenticationComponent;
import com.pendsley.clovisbray.inject.AuthenticationModule;
import com.pendsley.clovisbray.inject.BungieApiComponent;
import com.pendsley.clovisbray.inject.DaggerAuthenticationComponent;
import com.pendsley.clovisbray.inject.DaggerBungieApiComponent;
import com.pendsley.clovisbray.inject.DatabaseModule;
import com.pendsley.clovisbray.inject.NetworkingModule;
import com.pendsley.clovisbray.persistence.dao.ManifestHelper;
import com.pendsley.clovisbray.service.ClovisBrayFirebaseMessagingService;
import com.pendsley.clovisbray.util.BusProvider;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * The application.
 *
 * @author Phil Endsley
 */
public class ClovisBrayApplication extends Application {

    @Inject
    @Named(AuthenticationModule.REFRESH_TOKEN)
    String refreshToken;

    @Inject
    @Named(AuthenticationModule.REFRESH_TOKEN_EXPIRATION_TIME)
    Long refreshTokenExpirationTime;

    @Inject
    @Named(AuthenticationModule.ACCESS_TOKEN_EXPIRATION_TIME)
    Long accessTokenExpirationTime;

    @Inject
    BusProvider busProvider;

    private AuthenticationComponent authenticationComponent;
    private BungieApiComponent apiComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        createNotificationChannel();

        AppModule appModule = new AppModule(this);
        AuthenticationModule authenticationModule = new AuthenticationModule();
        DatabaseModule databaseModule = new DatabaseModule(new ClovisBrayDbHelper(this),
                new ManifestHelper(this));

        NetworkingModule networkingModule = new NetworkingModule();

        authenticationComponent = DaggerAuthenticationComponent.builder()
                .authenticationModule(authenticationModule)
                .databaseModule(databaseModule)
                .networkingModule(networkingModule)
                .appModule(appModule)
                .build();

        apiComponent = DaggerBungieApiComponent.builder()
                .appModule(appModule)
                .authenticationModule(authenticationModule)
                .databaseModule(databaseModule)
                .networkingModule(networkingModule)
                .accountModule(new AccountModule())
                .build();

        injectFields();
        busProvider.getBus().register(this);

        TokenRefreshReceiver tokenRefreshReceiver = new TokenRefreshReceiver();
        IntentFilter filter = new IntentFilter(TokenRefreshReceiver.TOKEN_REFRESH_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(tokenRefreshReceiver, filter);

        scheduleTokenRefresh();

        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                // No-op
            }

            @Override
            public void onActivityStarted(Activity activity) {
                // No-op
            }

            @Override
            public void onActivityResumed(Activity activity) {
                scheduleTokenRefresh();
            }

            @Override
            public void onActivityPaused(Activity activity) {
                // No-op
            }

            @Override
            public void onActivityStopped(Activity activity) {
                // No-op
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
                // No-op
            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                // No-op
            }
        });
    }



    @Subscribe
    public void onReinjectFields(ReinjectFieldsEvent event) {
        injectFields();

        if (event.refreshAccessTokens()) {
            scheduleTokenRefresh();
        }
    }

    public AuthenticationComponent getAuthenticationComponent() {
        return authenticationComponent;
    }

    public BungieApiComponent getApiComponent() {
        return apiComponent;
    }

    public void scheduleTokenRefresh() {

        Intent intent = new Intent();
        intent.setAction(TokenRefreshReceiver.TOKEN_REFRESH_ACTION);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void injectFields() {
        authenticationComponent.inject(this);
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.notification_channel_name);
            String description = getString(R.string.notification_channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel channel = new NotificationChannel(ClovisBrayFirebaseMessagingService.NOTIFICATION_CHANNEL_ID, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
    }
}
