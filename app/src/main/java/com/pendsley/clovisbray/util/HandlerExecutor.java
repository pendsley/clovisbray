package com.pendsley.clovisbray.util;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import java.util.concurrent.Executor;

/**
 * {@link Executor} that executes its runnable on the UI thread.
 *
 * @author Phil Endsley
 */
public class HandlerExecutor implements Executor {

    final Handler handler = new Handler(Looper.getMainLooper());

    @Override
    public void execute(@NonNull Runnable command) {
        handler.post(command);
    }
}
