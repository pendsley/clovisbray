package com.pendsley.clovisbray.util;

import com.pendsley.clovisbray.model.MainThreadBus;
import com.squareup.otto.Bus;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Helper class to provide an event bus instance for the application.
 *
 * @author Phil Endsley
 */
@Singleton
public final class BusProvider {

    private final Bus bus = new MainThreadBus();

    @Inject
    BusProvider() {
    }

    public Bus getBus() {
        return bus;
    }
}
