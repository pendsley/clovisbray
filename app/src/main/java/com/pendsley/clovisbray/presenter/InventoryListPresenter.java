package com.pendsley.clovisbray.presenter;

import android.content.Context;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.common.collect.Lists;
import com.pendsley.clovisbray.R;
import com.pendsley.clovisbray.inject.AccountModule;
import com.pendsley.clovisbray.model.Character;
import com.pendsley.clovisbray.model.DestinyAssets;
import com.pendsley.clovisbray.model.InventoryFilterOption;
import com.pendsley.clovisbray.model.ItemClassType;
import com.pendsley.clovisbray.model.LocalInventoryItem;
import com.pendsley.clovisbray.operation.InventorySyncOperation;
import com.pendsley.clovisbray.persistence.dao.AccountOperation;
import com.pendsley.clovisbray.persistence.datastore.InventoryCache;
import com.pendsley.clovisbray.viewcontroller.InventoryListViewController;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

/**
 * Presenter for the list of inventory items.
 *
 * @author Phil Endsley
 */
public class InventoryListPresenter {

    private static final String TAG = "InventoryListPresenter";

    private final InventoryCache inventoryCache;
    private final AccountOperation accountOperation;
    private final InventorySyncOperation inventorySyncOperation;
    private final List<Character> characters;
    private final CompositeDisposable disposables;

    private InventoryListViewController inventoryListViewController;

    @Inject
    InventoryListPresenter(InventoryCache inventoryCache,
                           AccountOperation accountOperation,
                           InventorySyncOperation inventorySyncOperation,
                           @Named(AccountModule.CHARACTERS) List<Character> characters) {
        this.inventoryCache = inventoryCache;
        this.accountOperation = accountOperation;
        this.inventorySyncOperation = inventorySyncOperation;
        this.characters = characters;
        disposables = new CompositeDisposable();
    }

    public void setInventoryListViewController(InventoryListViewController inventoryListViewController) {
        this.inventoryListViewController = inventoryListViewController;
    }


    public void syncInventoryItemsAndLoadByClassType(final ItemClassType itemClassType) {
        inventoryListViewController.showLoadingIndicator();

        inventoryCache.expireCache();
        Flowable<List<LocalInventoryItem>> flowable = inventorySyncOperation.syncCharactersAndInventory(null)
                .andThen(inventoryCache.getInventoryListFlowableByClassType(itemClassType))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        disposables.add(flowable.subscribeWith(new InventoryListItemsSubscriber()));
    }

    public void loadInventoryListByClassType(ItemClassType itemClassType) {
        inventoryListViewController.showLoadingIndicator();

        Flowable<List<LocalInventoryItem>> flowable =
                inventoryCache.getInventoryListFlowableByClassType(itemClassType)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());
        disposables.add(flowable.subscribeWith(new InventoryListItemsSubscriber()));

    }

    public void syncInventoryItemsAndLoadByCategory(final String itemCategory) {
        inventoryListViewController.showLoadingIndicator();

        inventoryCache.expireCache();
        Flowable<List<LocalInventoryItem>> flowable = inventorySyncOperation.syncCharactersAndInventory(null)
                .andThen(inventoryCache.getInventoryListFlowableByCategory(itemCategory))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        disposables.add(flowable.subscribeWith(new InventoryListItemsSubscriber()));
    }

    public void loadInventoryListByCategory(String itemCategory) {
        inventoryListViewController.showLoadingIndicator();

        Flowable<List<LocalInventoryItem>> flowable = inventoryCache.getInventoryListFlowableByCategory(itemCategory)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        disposables.add(flowable.subscribeWith(new InventoryListItemsSubscriber()));
    }

    public Map<String, String> loadCharacterEmblemPaths() {
        return accountOperation.getCharacterEmblemPaths();
    }

    public List<InventoryFilterOption> loadFilterOptions() {
        List<InventoryFilterOption> filterOptions = Lists.newArrayList();

        Context context = inventoryListViewController.context();

        String vaultDescription = context.getString(R.string.vault);
        InventoryFilterOption vaultOption = new InventoryFilterOption();
        vaultOption.setDescription(vaultDescription);
        vaultOption.setCharacterId("");

        String allDescription = context.getString(R.string.all);
        InventoryFilterOption allOption = new InventoryFilterOption();
        allOption.setEmblemBackgroundPath(DestinyAssets.ALL_FILTER_OPTION_BACKGROUND);
        allOption.setDescription(allDescription);
        allOption.setCharacterId(allDescription);

        filterOptions.add(allOption);

        List<Character> sortedCharacters = Lists.newArrayList(characters);
        Collections.sort(sortedCharacters, (character1, character2) -> character1.getCharacterId().compareTo(character2.getCharacterId()));

        for (Character character : sortedCharacters) {
            InventoryFilterOption filterOption = new InventoryFilterOption();
            filterOption.setEmblemBackgroundPath(character.getBackgroundPath());
            filterOption.setDescription(character.getCharacterClass().getClassName());
            filterOption.setCharacterId(character.getCharacterId());
            filterOptions.add(filterOption);
        }

        filterOptions.add(vaultOption);

        return filterOptions;
    }

    public void dispose() {
        if (!disposables.isDisposed()) {
            disposables.dispose();
        }
    }

    private void handleInventoryFinishedLoading(List<LocalInventoryItem> inventoryItems) {
        showInventoryList(inventoryItems);
    }

    private void showInventoryList(List<LocalInventoryItem> inventoryItems) {
        inventoryListViewController.hideLoadingIndicator();
        inventoryListViewController.displayInventoryList(inventoryItems);
    }

    private final class InventoryListItemsSubscriber extends DisposableSubscriber<List<LocalInventoryItem>> {

        @Override
        public void onNext(List<LocalInventoryItem> localInventoryItems) {
            handleInventoryFinishedLoading(localInventoryItems);
        }

        @Override
        public void onError(Throwable e) {
            Log.e(TAG, "Error getting inventory list", e);
            Crashlytics.logException(e);
        }

        @Override
        public void onComplete() {

        }
    }

}
