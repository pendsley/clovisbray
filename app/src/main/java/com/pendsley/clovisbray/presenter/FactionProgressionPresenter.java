package com.pendsley.clovisbray.presenter;

import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.pendsley.clovisbray.model.BungieErrorCode;
import com.pendsley.clovisbray.model.FactionProgression;
import com.pendsley.clovisbray.model.NetworkException;
import com.pendsley.clovisbray.operation.FactionOperation;
import com.pendsley.clovisbray.viewcontroller.FactionProgressionViewController;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

/**
 * Presenter for vendor progression.
 *
 * @author Phil Endsley
 */
public class FactionProgressionPresenter {

    private static final String TAG = "FactionProgPresenter";

    private final FactionOperation factionOperation;
    private final CompositeDisposable disposables;

    private FactionProgressionViewController factionProgressionViewController;

    @Inject
    FactionProgressionPresenter(FactionOperation factionOperation) {
        this.factionOperation = factionOperation;
        disposables = new CompositeDisposable();
    }

    public void setFactionProgressionViewController(FactionProgressionViewController factionProgressionViewController) {
        this.factionProgressionViewController = factionProgressionViewController;
    }

    public void loadVendors(String characterId) {

        factionProgressionViewController.showLoadingIndicator();
        Flowable<List<FactionProgression>> flowable = factionOperation.retrieveFactions(characterId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        disposables.add(flowable.subscribeWith(new ProgressionSubscriber()));
    }

    public void dispose() {
        disposables.dispose();
    }

    private void displayProgressions(List<FactionProgression> progressions) {
        factionProgressionViewController.displayFactionProgressions(progressions);
        factionProgressionViewController.hideLoadingIndicator();
    }

    private void displayError(int errorMessageResource) {
        factionProgressionViewController.hideLoadingIndicator();
        factionProgressionViewController.displayError(errorMessageResource);
    }

    private void displayError(String errorMessage) {
        factionProgressionViewController.hideLoadingIndicator();
        factionProgressionViewController.displayError(errorMessage);
    }

    private class ProgressionSubscriber extends DisposableSubscriber<List<FactionProgression>> {

        @Override
        public void onNext(List<FactionProgression> progressions) {
            displayProgressions(progressions);
        }

        @Override
        public void onError(Throwable t) {
            String message = t.getMessage();
            Log.e(TAG, message, t);
            Crashlytics.logException(t);

            if (t instanceof NetworkException) {
                displayError(((NetworkException) t).getErrorMessageResource());
            } else {
                displayError(BungieErrorCode.OTHER.getErrorMessage());
            }
        }

        @Override
        public void onComplete() {

        }
    }
}
