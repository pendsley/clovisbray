package com.pendsley.clovisbray.presenter;

import com.pendsley.clovisbray.model.ItemSlot;
import com.pendsley.clovisbray.persistence.datastore.LoadoutEditingCache;
import com.pendsley.clovisbray.viewcontroller.NewLoadoutSlotSelectionViewController;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

/**
 * Presenter for the available item slots of the current loadout being edited.
 *
 * @author Phil Endsley
 */
public class NewLoadoutSlotSelectionPresenter {

    private final LoadoutEditingCache loadoutEditingCache;
    private final CompositeDisposable disposables;

    private NewLoadoutSlotSelectionViewController newLoadoutSlotSelectionViewController;

    @Inject
    NewLoadoutSlotSelectionPresenter(LoadoutEditingCache loadoutEditingCache) {
        this.loadoutEditingCache = loadoutEditingCache;
        disposables = new CompositeDisposable();
    }

    public void setNewLoadoutSlotSelectionViewController(NewLoadoutSlotSelectionViewController newLoadoutSlotSelectionViewController) {
        this.newLoadoutSlotSelectionViewController = newLoadoutSlotSelectionViewController;
    }

    public void loadAvailableSlots() {
        Flowable<List<ItemSlot>> flowable = loadoutEditingCache.getAvailableSlotsFlowable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        disposables.add(flowable.subscribeWith(new AvailableSlotsSubscriber()));
    }

    public void dispose() {
        if (!disposables.isDisposed()) {
            disposables.dispose();
        }
    }

    private void availableSlotsLoaded(List<ItemSlot> availableSlots) {
        newLoadoutSlotSelectionViewController.displayAvailableSlots(availableSlots);
    }

    private final class AvailableSlotsSubscriber extends DisposableSubscriber<List<ItemSlot>> {

        @Override
        public void onNext(List<ItemSlot> itemSlots) {
            availableSlotsLoaded(itemSlots);
        }

        @Override
        public void onError(Throwable t) {

        }

        @Override
        public void onComplete() {

        }
    }
}
