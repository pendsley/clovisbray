package com.pendsley.clovisbray.presenter;

import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.pendsley.clovisbray.model.NetworkException;
import com.pendsley.clovisbray.model.UserInfo;
import com.pendsley.clovisbray.operation.CharacterOperation;
import com.pendsley.clovisbray.operation.PlatformSelectionOperation;
import com.pendsley.clovisbray.viewcontroller.PlatformSelectionViewController;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

/**
 * Presenter for platform selections. Manages interactions between operations and views.
 *
 * @author Phil Endsley
 */
public class PlatformSelectionPresenter {

    private static final String TAG = "PlatformSelectPrsntr";

    private final PlatformSelectionOperation platformSelectionOperation;
    private final CompositeDisposable disposables;

    private CharacterOperation characterOperation;
    private PlatformSelectionViewController platformSelectionViewController;

    @Inject
    PlatformSelectionPresenter(CharacterOperation characterOperation,
                               PlatformSelectionOperation platformSelectionOperation) {
        this.characterOperation = characterOperation;
        this.platformSelectionOperation = platformSelectionOperation;
        disposables = new CompositeDisposable();
    }

    public void setPlatformSelectionViewController(PlatformSelectionViewController platformSelectionViewController) {
        this.platformSelectionViewController = platformSelectionViewController;
    }

    public void dispose() {
        disposables.dispose();
    }

    public void loadPlatformUsers() {

        Flowable<List<UserInfo>> flowable = platformSelectionOperation.loadUserInfos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        disposables.add(flowable.subscribeWith(new PlatformUserInfosSubscriber()));
    }

    public void retrieveAndSaveCharacters(UserInfo userInfo) {

        Flowable<Void> flowable = characterOperation.retrieveAndSaveCharacters(userInfo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        disposables.add(flowable.subscribeWith(new RetrieveAndSaveCharactersSubscriber()));
    }

    void handlePlatformUsersLoaded(List<UserInfo> platformUserInfos) {
        platformSelectionViewController.displayPlatforms(platformUserInfos);
    }

    void displayError(int errorResrouceId) {
        platformSelectionViewController.displayError(errorResrouceId);
    }

    private final class PlatformUserInfosSubscriber extends DisposableSubscriber<List<UserInfo>> {

        @Override
        public void onNext(List<UserInfo> userInfos) {
            handlePlatformUsersLoaded(userInfos);
        }

        @Override
        public void onError(Throwable t) {
            Log.e(TAG, "Error loading platforrm user information");
        }

        @Override
        public void onComplete() {

        }
    }

    private class RetrieveAndSaveCharactersSubscriber extends DisposableSubscriber<Void> {

        @Override
        public void onNext(Void aVoid) {

        }

        @Override
        public void onError(Throwable t) {
            Log.e(TAG, t.getMessage(), t);
            Crashlytics.logException(t);
            if (t instanceof NetworkException) {
                displayError(((NetworkException) t).getErrorMessageResource());
            }
        }

        @Override
        public void onComplete() {
            platformSelectionViewController.launchManifestActivity();
        }
    }
}
