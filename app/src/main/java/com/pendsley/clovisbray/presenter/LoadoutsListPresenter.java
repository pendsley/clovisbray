package com.pendsley.clovisbray.presenter;

import android.support.annotation.Nullable;

import com.pendsley.clovisbray.model.EquipOperationResult;
import com.pendsley.clovisbray.model.Loadout;
import com.pendsley.clovisbray.operation.EquipLoadoutOperation;
import com.pendsley.clovisbray.operation.LoadoutsListOperation;
import com.pendsley.clovisbray.persistence.datastore.LoadoutEditingCache;
import com.pendsley.clovisbray.viewcontroller.LoadoutsListViewController;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

/**
 * Presenter for the list of loadouts.
 *
 * @author Phil Endsley
 */
public class LoadoutsListPresenter {

    private final LoadoutsListOperation loadoutsListOperation;
    private final EquipLoadoutOperation equipLoadoutOperation;
    private final LoadoutEditingCache loadoutEditingCache;

    private final CompositeDisposable disposables;

    private LoadoutsListViewController loadoutsListViewController;

    @Inject
    LoadoutsListPresenter(LoadoutsListOperation loadoutsListOperation,
                          EquipLoadoutOperation equipLoadoutOperation,
                          LoadoutEditingCache loadoutEditingCache) {
        this.loadoutsListOperation = loadoutsListOperation;
        this.equipLoadoutOperation = equipLoadoutOperation;
        this.loadoutEditingCache = loadoutEditingCache;

        disposables = new CompositeDisposable();
    }

    public void setLoadoutsListViewController(LoadoutsListViewController loadoutsListViewController) {
        this.loadoutsListViewController = loadoutsListViewController;
    }

    public void loadLoadoutsForCharacter(String characterId) {
        Flowable<List<Loadout>> flowable =
                loadoutsListOperation.getLoadoutsForCharacterFlowable(characterId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());
        disposables.add(flowable.subscribeWith(new LoadoutListSubscriber()));

    }

    public void equipLoadout(Loadout loadout) {

        Scheduler subscribeOnScheduler = Schedulers.io();
        // We chain a lot of flowables when equipping loadouts, so we pass the scheduler down to
        // ensure everything is executed on the correct thread. There's probably a better way to do
        // this
        List<Flowable<EquipOperationResult>> flowables = equipLoadoutOperation.equipLoadout(loadout, subscribeOnScheduler);
        EquipLoadoutsFinishedListener equipLoadoutsFinishedListener =
                new EquipLoadoutsFinishedListener(loadout, flowables.size());

        for (Flowable<EquipOperationResult> flowable : flowables) {
            flowable.subscribeOn(subscribeOnScheduler)
                    .observeOn(AndroidSchedulers.mainThread());
            disposables.add(flowable.subscribeWith(new EquipLoadoutsSubscriber(equipLoadoutsFinishedListener)));
        }
    }

    public void createLoadout(String characterId) {
        Loadout newLoadout = new Loadout();
        newLoadout.setCharacterId(characterId);
        showLoadoutDetails(newLoadout);
    }

    public void editLoadout(Loadout loadout) {
        showLoadoutDetails(loadout);
    }

    public void dispose() {
        if (!disposables.isDisposed()) {
            disposables.dispose();
        }
    }

    private void loadoutsFinishedLoading(List<Loadout> loadouts) {
        loadoutsListViewController.displayLoadouts(loadouts);
    }

    private void showLoadoutDetails(Loadout loadout) {
        loadoutEditingCache.setLoadout(loadout);
        loadoutsListViewController.showLoadoutDetails(loadout);
    }

    private void equipLoadoutFinished(Loadout loadout,
                                      List<Throwable> errors) {
        loadoutsListViewController.loadoutEquipped(loadout, errors);
    }

    private final class LoadoutListSubscriber extends DisposableSubscriber<List<Loadout>> {

        @Override
        public void onNext(List<Loadout> loadouts) {
            loadoutsFinishedLoading(loadouts);
        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onComplete() {

        }
    }

    private final class EquipLoadoutsFinishedListener {

        private final Loadout loadout;
        private final CountDownLatch countDownLatch;
        private final List<Throwable> errors;

        EquipLoadoutsFinishedListener(Loadout loadout,
                                      int itemsCount) {
            this.loadout = loadout;
            countDownLatch = new CountDownLatch(itemsCount);
            errors = new ArrayList<>();

        }

        private void subscriberFinished(@Nullable Throwable error) {
            if (error != null) {
                errors.add(error);
            }
            countDownLatch.countDown();
            if (countDownLatch.getCount() == 0) {
                equipLoadoutFinished(loadout, errors);
            }
        }

    }

    private static final class EquipLoadoutsSubscriber extends DisposableSubscriber<EquipOperationResult> {

        private final EquipLoadoutsFinishedListener equipLoadoutsFinishedListener;

        EquipLoadoutsSubscriber(EquipLoadoutsFinishedListener equipLoadoutsFinishedListener) {
            this.equipLoadoutsFinishedListener = equipLoadoutsFinishedListener;
        }

        @Override
        public void onNext(EquipOperationResult equipOperationResult) {
        }

        @Override
        public void onError(Throwable e) {
            equipLoadoutsFinishedListener.subscriberFinished(e);
        }

        @Override
        public void onComplete() {
            equipLoadoutsFinishedListener.subscriberFinished(null);
        }

    }
}
