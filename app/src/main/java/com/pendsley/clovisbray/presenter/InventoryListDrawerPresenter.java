package com.pendsley.clovisbray.presenter;

import android.content.Context;

import com.google.common.collect.Lists;
import com.pendsley.clovisbray.R;
import com.pendsley.clovisbray.inject.AccountModule;
import com.pendsley.clovisbray.model.Character;
import com.pendsley.clovisbray.model.CharacterLoadoutsDrawerItem;
import com.pendsley.clovisbray.model.DrawerItem;
import com.pendsley.clovisbray.model.ItemClassType;
import com.pendsley.clovisbray.viewcontroller.InventoryDrawerViewController;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Presenter for the navigation drawer.
 *
 * @author Phil Endsley
 */
public class InventoryListDrawerPresenter {

    private final List<Character> characters;

    private InventoryDrawerViewController inventoryDrawerViewController;

    @Inject
    InventoryListDrawerPresenter(@Named(AccountModule.CHARACTERS) List<Character> characters) {
        this.characters = characters;
    }

    public void setInventoryDrawerViewController(InventoryDrawerViewController inventoryDrawerViewController) {
        this.inventoryDrawerViewController = inventoryDrawerViewController;
    }

    public void syncDrawerState() {
        inventoryDrawerViewController.syncDrawerState();
    }

    public void initializeInventoryDrawer() {

        Context context = inventoryDrawerViewController.getContext();

        List<DrawerItem> drawerItems = Lists.newArrayList();
        drawerItems.add(new DrawerItem(context.getString(R.string.category_all)));
        drawerItems.add(new DrawerItem(context.getString(R.string.category_weapons)));
        drawerItems.add(new DrawerItem(context.getString(R.string.category_armor)));
        drawerItems.add(new DrawerItem(context.getString(R.string.postmaster)));
        drawerItems.add(new DrawerItem("Factions")); // TODO
        for (Character character : characters) {
            drawerItems.add(new CharacterLoadoutsDrawerItem(character));
        }

        drawerItems.add(new DrawerItem(context.getString(R.string.change_platform)));

        inventoryDrawerViewController.initializeDrawerItems(drawerItems);
    }

    public void drawerItemSelected(DrawerItem selectedDrawerItem) {
        inventoryDrawerViewController.closeDrawer();

        if (selectedDrawerItem instanceof CharacterLoadoutsDrawerItem) {
            inventoryDrawerViewController.navigateToLoadoutsList(((CharacterLoadoutsDrawerItem) selectedDrawerItem).getCharacter().getCharacterId());
        } else if ("Postmaster".equals(selectedDrawerItem.getScreenName())) {
            inventoryDrawerViewController.navigateToPostmaster();
        } else if ("Factions".equals(selectedDrawerItem.getScreenName())) { // TODO
            inventoryDrawerViewController.navigateToFactions();
        } else if ("Change Platform".equals(selectedDrawerItem.getScreenName())) {
            inventoryDrawerViewController.navigateToPlatformSelection();
        } else {
            inventoryDrawerViewController.navigateToInventoryList(ItemClassType.fromString(selectedDrawerItem.getScreenName()));
        }
    }

}
