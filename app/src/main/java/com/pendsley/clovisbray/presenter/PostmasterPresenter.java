package com.pendsley.clovisbray.presenter;

import android.util.Log;

import com.pendsley.clovisbray.R;
import com.pendsley.clovisbray.model.ManifestInventoryItem;
import com.pendsley.clovisbray.model.NetworkException;
import com.pendsley.clovisbray.operation.PostmasterOperation;
import com.pendsley.clovisbray.viewcontroller.PostmasterViewController;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

/**
 * Manages interactions between the postmaster view and model.
 *
 * @author Phil Endsley
 */
public class PostmasterPresenter {

    private static final String TAG = "PostMasterPresenter";

    private final PostmasterOperation postmasterOperation;
    private final CompositeDisposable disposables;

    private PostmasterViewController postmasterViewController;

    @Inject
    PostmasterPresenter(PostmasterOperation postmasterOperation) {
        this.postmasterOperation = postmasterOperation;
        disposables = new CompositeDisposable();
    }

    public void attachView(PostmasterViewController postmasterViewController) {
        this.postmasterViewController = postmasterViewController;
    }

    public void loadPostMasterItems(String characterId) {

        postmasterViewController.showLoadingIndicator();

        Flowable<List<ManifestInventoryItem>> flowable = postmasterOperation.getItems(characterId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        disposables.add(flowable.subscribeWith(new PostMasterItemsSubscriber()));
    }

    public void transferItemsFromPostmaster(List<ManifestInventoryItem> items,
                                            String characterId) {

        postmasterViewController.showLoadingIndicator();

        Completable completable = postmasterOperation.transferFromPostMaster(items, characterId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        disposables.add(completable.subscribeWith(new PostMasterTransferSubscriber(characterId)));
    }

    private void postMasterItemsFinishedLoading(List<ManifestInventoryItem> postMasterItems) {

        postmasterViewController.hideLoadingIndicator();
        postmasterViewController.displayItems(postMasterItems);
    }

    private void onError(Throwable t) {
        Log.e(TAG, t.getMessage(), t);
        if (t instanceof NetworkException) {
            postmasterViewController.displayError(((NetworkException) t).getErrorMessageResource());
        } else {
            postmasterViewController.displayError(R.string.postmaster_lookup_error);
        }
    }

    private class PostMasterItemsSubscriber extends DisposableSubscriber<List<ManifestInventoryItem>> {

        @Override
        public void onNext(List<ManifestInventoryItem> manifestInventoryItems) {
            postMasterItemsFinishedLoading(manifestInventoryItems);
        }

        @Override
        public void onError(Throwable t) {
            PostmasterPresenter.this.onError(t);
        }

        @Override
        public void onComplete() {

        }
    }

    private class PostMasterTransferSubscriber extends DisposableCompletableObserver {

        private final String characterId;

        PostMasterTransferSubscriber(String characterId) {
            this.characterId = characterId;
        }

        @Override
        public void onComplete() {
            loadPostMasterItems(characterId);
        }

        @Override
        public void onError(Throwable t) {
            PostmasterPresenter.this.onError(t);
            loadPostMasterItems(characterId);
        }
    }
    
}
