package com.pendsley.clovisbray.presenter;

import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.pendsley.clovisbray.model.UserInfo;
import com.pendsley.clovisbray.operation.CharacterOperation;
import com.pendsley.clovisbray.viewcontroller.AuthenticationViewController;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

/**
 * TODO
 */
public class CharacterPresenter {

    private static final String TAG = "CharacterPresenter";

    private final CharacterOperation characterOperation;
    private final CompositeDisposable disposables;

    private AuthenticationViewController authenticationViewController;

    @Inject
    CharacterPresenter(CharacterOperation characterOperation) {
        this.characterOperation = characterOperation;
        disposables = new CompositeDisposable();
    }

    public void setAuthenticationViewController(AuthenticationViewController authenticationViewController) {
        this.authenticationViewController = authenticationViewController;
    }

    public void retrieveAndSaveCharacters(UserInfo userInfo) {

        Flowable<Void> flowable = characterOperation.retrieveAndSaveCharacters(userInfo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        disposables.add(flowable.subscribeWith(new RetrieveAndSaveCharactersSubscriber()));
    }

    private class RetrieveAndSaveCharactersSubscriber extends DisposableSubscriber<Void> {

        @Override
        public void onNext(Void aVoid) {

        }

        @Override
        public void onError(Throwable t) {
            Log.e(TAG, t.getMessage(), t);

            Crashlytics.logException(t);
        }

        @Override
        public void onComplete() {
            authenticationViewController.launchManifestActivity();
        }
    }
}
