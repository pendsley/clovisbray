package com.pendsley.clovisbray.presenter;

import android.util.Log;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.pendsley.clovisbray.R;
import com.pendsley.clovisbray.inject.AccountModule;
import com.pendsley.clovisbray.model.Character;
import com.pendsley.clovisbray.model.EquipOperationResult;
import com.pendsley.clovisbray.model.LocalInventoryItem;
import com.pendsley.clovisbray.model.ManifestSocket;
import com.pendsley.clovisbray.model.Stat;
import com.pendsley.clovisbray.model.VaultDestination;
import com.pendsley.clovisbray.operation.EquipItemOperation;
import com.pendsley.clovisbray.operation.TransferItemOperation;
import com.pendsley.clovisbray.persistence.datastore.InventoryCache;
import com.pendsley.clovisbray.viewcontroller.InventoryItemDetailsViewController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

/**
 * Presenter for an inventory item's details.
 *
 * @author Phil Endsley
 */
public class InventoryItemDetailsPresenter {

    private final TransferItemOperation transferItemOperation;
    private final EquipItemOperation equipItemOperation;
    private final InventoryCache inventoryCache;
    private final List<Character> characters;
    private final CompositeDisposable disposables;

    private InventoryItemDetailsViewController inventoryItemDetailsViewController;
    private LocalInventoryItem item;

    @Inject
    InventoryItemDetailsPresenter(TransferItemOperation transferItemOperation,
                                  EquipItemOperation equipItemOperation,
                                  InventoryCache inventoryCache,
                                  @Named(AccountModule.CHARACTERS) List<Character> characters) {
        this.transferItemOperation = transferItemOperation;
        this.equipItemOperation = equipItemOperation;
        this.inventoryCache = inventoryCache;
        this.characters = characters;
        disposables = new CompositeDisposable();
    }

    public void setInventoryItemDetailsViewController(InventoryItemDetailsViewController inventoryItemDetailsViewController) {
        this.inventoryItemDetailsViewController = inventoryItemDetailsViewController;
    }

    public void setInventoryItem(LocalInventoryItem item) {
        this.item = item;
    }

    public void loadItemDetails() {
        List<ManifestSocket> sockets = item.getSockets();
        if (sockets == null) {
            sockets = new ArrayList<>();
        }
        inventoryItemDetailsViewController.displaySockets(sockets);

        List<Stat> stats = item.getStats();
        if (stats == null) {
            stats = new ArrayList<>();
        }
        inventoryItemDetailsViewController.displayItemStats(stats);
    }

    public void loadTransferEquipCharacterOptions() {
        List<Character> sortedCharacters = Lists.newArrayList(characters);
        Collections.sort(sortedCharacters, (character1, character2) ->
                character1.getCharacterId().compareTo(character2.getCharacterId()));

        sortedCharacters.add(new VaultDestination());

        inventoryItemDetailsViewController.setTransferEquipCharacterOptions(sortedCharacters,
                item.getCharacterId());
    }

    public void transferItem(Character destinationCharacter) {
        if (Strings.isNullOrEmpty(item.getCharacterId()) && destinationCharacter instanceof VaultDestination) {
            displayErrorMessage(inventoryItemDetailsViewController.getContext()
                    .getString(R.string.item_already_in_vault));
            return;
        }

        Flowable<LocalInventoryItem> flowable =
                transferItemOperation.transferItem(destinationCharacter, item)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());
        disposables.add(flowable.subscribeWith(new TransferItemSubscriber()));

        inventoryItemDetailsViewController.hideOptionsSheet();
    }

    public void equipItem(Character destinationCharacter) {

        String destinationCharacterId = destinationCharacter.getCharacterId();

        if (Strings.isNullOrEmpty(item.getCharacterId()) && destinationCharacter instanceof VaultDestination) {
            displayErrorMessage(inventoryItemDetailsViewController.getContext()
                    .getString(R.string.item_already_in_vault, item.getName()));
            return;
        } else if (item.isEquipped()) {
            if (destinationCharacter.getCharacterId().equals(item.getCharacterId())) {
                displayErrorMessage(inventoryItemDetailsViewController.getContext()
                        .getString(R.string.item_already_equipped, item.getName()));
            } else {
                displayErrorMessage(inventoryItemDetailsViewController.getContext()
                        .getString(R.string.item_currently_equipped, item.getName()));
            }
        }

        Flowable<EquipOperationResult> flowable =
                equipItemOperation.equipItem(item, destinationCharacterId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        disposables.add(flowable.subscribeWith(new EquipItemSubscriber()));

        inventoryItemDetailsViewController.hideOptionsSheet();
    }

    public void dispose() {
        if (!disposables.isDisposed()) {
            disposables.dispose();
        }
    }

    private void displayInformationMessage(String message) {
        inventoryItemDetailsViewController.showInformationMessage(message);
    }

    private void displayErrorMessage(String errorMessage) {
        inventoryItemDetailsViewController.showErrorMessage(errorMessage);
    }

    private void transferFinished(LocalInventoryItem updatedItem) {
        inventoryCache.updateItem(updatedItem);

        displayInformationMessage(inventoryItemDetailsViewController.getContext()
                .getString(R.string.item_transferred, item.getName()));

        inventoryItemDetailsViewController.leaveDetailsView();
    }

    private void equipFinished(EquipOperationResult equipOperationResult) {
        LocalInventoryItem newItem = equipOperationResult.getNewItem();
        inventoryCache.updateItem(equipOperationResult.getPreviousItem());
        inventoryCache.updateItem(newItem);

        displayInformationMessage(inventoryItemDetailsViewController.getContext()
                .getString(R.string.item_equipped, newItem.getName()));

        inventoryItemDetailsViewController.leaveDetailsView();
    }

    private class TransferItemSubscriber extends DisposableSubscriber<LocalInventoryItem> {

        @Override
        public void onNext(LocalInventoryItem item) {
            transferFinished(item);
        }

        @Override
        public void onError(Throwable e) {
            Log.e("TransferItemSubscriber", e.getMessage(), e);
            displayErrorMessage(e.getLocalizedMessage());
        }

        @Override
        public void onComplete() {

        }
    }

    private class EquipItemSubscriber extends DisposableSubscriber<EquipOperationResult> {

        @Override
        public void onNext(EquipOperationResult equipOperationResult) {
            equipFinished(equipOperationResult);
        }

        @Override
        public void onError(Throwable e) {
            displayErrorMessage(e.getLocalizedMessage());
        }

        @Override
        public void onComplete() {

        }
    }
}
