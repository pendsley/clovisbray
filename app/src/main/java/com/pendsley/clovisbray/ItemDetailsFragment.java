package com.pendsley.clovisbray;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.common.base.Strings;
import com.gordonwong.materialsheetfab.MaterialSheetFab;
import com.pendsley.clovisbray.authentication.ReinjectFieldsEvent;
import com.pendsley.clovisbray.model.Character;
import com.pendsley.clovisbray.model.LocalInventoryItem;
import com.pendsley.clovisbray.model.ManifestSocket;
import com.pendsley.clovisbray.model.Stat;
import com.pendsley.clovisbray.model.StatName;
import com.pendsley.clovisbray.model.VaultDestination;
import com.pendsley.clovisbray.network.BungieDestinyService;
import com.pendsley.clovisbray.presenter.InventoryItemDetailsPresenter;
import com.pendsley.clovisbray.ui.AnimationUtil;
import com.pendsley.clovisbray.ui.DividerItemDecorator;
import com.pendsley.clovisbray.util.BusProvider;
import com.pendsley.clovisbray.viewcontroller.InventoryItemDetailsViewController;
import com.pendsley.clovisbray.widget.SheetFloatingActionButton;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import static android.support.design.widget.Snackbar.LENGTH_LONG;
import static android.support.design.widget.Snackbar.LENGTH_SHORT;

/**
 * Fragment for a single item's details.
 *
 * @author Phil Endsley
 */
public class ItemDetailsFragment extends Fragment implements InventoryItemDetailsViewController {

    public static final String TAG = "ItemDetailsFragment";

    private static final String STATE_ITEM_DETAILS = "itemDetails";

    @Inject
    InventoryItemDetailsPresenter inventoryItemDetailsPresenter;

    @Inject
    BusProvider busProvider;

    private LocalInventoryItem localInventoryItem;

    private View fragmentContainer;
    private RecyclerView statsRecyclerView;
    private RecyclerView talentNodeStepRecyclerView;

    private SheetFloatingActionButton button;
    private MaterialSheetFab materialSheetFab;

    private RecyclerView characterRecyclerView;

    public static ItemDetailsFragment create(LocalInventoryItem localInventoryItem) {
        Bundle bundle = new Bundle(1);
        bundle.putParcelable(STATE_ITEM_DETAILS, localInventoryItem);

        ItemDetailsFragment fragment = new ItemDetailsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectMembers();

        localInventoryItem = getArguments().getParcelable(STATE_ITEM_DETAILS);

        inventoryItemDetailsPresenter.setInventoryItem(localInventoryItem);
    }

    @Override
    public void onResume() {
        super.onResume();
        busProvider.getBus().register(this);

        getActivity().setTitle(localInventoryItem.getName());
    }

    @Override
    public void onPause() {
        super.onPause();
        busProvider.getBus().unregister(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        inventoryItemDetailsPresenter.dispose();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_talent_nodes, container, false);

        Context context = getContext();

        fragmentContainer = root;

        statsRecyclerView = (RecyclerView) root.findViewById(R.id.stats_list);
        RecyclerView.LayoutManager statsLayoutManager = new LinearLayoutManager(context);
        statsRecyclerView.setLayoutManager(statsLayoutManager);

        talentNodeStepRecyclerView = (RecyclerView) root.findViewById(R.id.talent_nodes_list);


        button = (SheetFloatingActionButton) root.findViewById(R.id.transfer_button);
        final View sheet = root.findViewById(R.id.sheet_container);
        sheet.setTranslationY(-sheet.getHeight());

        View overlay = root.findViewById(R.id.overlay);

        @SuppressWarnings("deprecation") // Must use due to api restrictions
        final int buttonColor = context.getResources().getColor(R.color.accent);
        @SuppressWarnings("deprecation") // Must use due to api restrictions
        final int sheetColor = context.getResources().getColor(R.color.primary_dark);

        materialSheetFab = new MaterialSheetFab<>(button, sheet, overlay, sheetColor, buttonColor);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        talentNodeStepRecyclerView.setLayoutManager(layoutManager);
        talentNodeStepRecyclerView.addItemDecoration(new DividerItemDecorator(context));
        talentNodeStepRecyclerView.setLayoutAnimation(AnimationUtil.fastFadeInLayoutAnimationController(context));

        // This is a hack to get around a known bug
        // https://code.google.com/p/android/issues/detail?id=221387
        button.post(() -> {
            button.requestLayout();
            button.setVisibility(View.VISIBLE);
        });

        characterRecyclerView = (RecyclerView) root.findViewById(R.id.sheet_list);

        RecyclerView.LayoutManager characterLayoutManager = new LinearLayoutManager(context);
        characterRecyclerView.setLayoutManager(characterLayoutManager);

        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        inventoryItemDetailsPresenter.setInventoryItemDetailsViewController(this);

        inventoryItemDetailsPresenter.loadItemDetails();
        inventoryItemDetailsPresenter.loadTransferEquipCharacterOptions();
    }

    @Override
    public void displaySockets(@NonNull List<ManifestSocket> manifestSockets) {
        SocketAdapter adapter = new SocketAdapter(manifestSockets);
        talentNodeStepRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        talentNodeStepRecyclerView.startLayoutAnimation();
    }

    @Override
    public void displayItemStats(List<Stat> stats) {
        StatAdapter adapter = new StatAdapter(stats);
        statsRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void setTransferEquipCharacterOptions(List<Character> characters,
                                                 String itemCharacterId) {
        CharacterAdapter characterAdapter = new CharacterAdapter(itemCharacterId,
                characters);
        characterRecyclerView.setAdapter(characterAdapter);
        characterAdapter.notifyDataSetChanged();
    }

    @Override
    public void hideOptionsSheet() {
        if (materialSheetFab.isSheetVisible()) {
            materialSheetFab.hideSheet();
        }
    }

    @Override
    public void showInformationMessage(String message) {
        showSnackbar(message, LENGTH_SHORT);
    }

    @Override
    public void showErrorMessage(String errorMessage) {
        showSnackbar(errorMessage, LENGTH_LONG);
    }

    @Override
    public void leaveDetailsView() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    private void showSnackbar(String message,
                              int duration) {
        @SuppressWarnings("deprecation") // Must use due to api restrictions
        int textColor = getContext().getResources().getColor(R.color.secondary_text);

        final Snackbar snackbar = Snackbar.make(fragmentContainer, message, duration)
                .setActionTextColor(textColor);
        snackbar.setAction(R.string.dismiss, v -> snackbar.dismiss());
        snackbar.show();
    }

    @Subscribe
    public void onRefreshToken(ReinjectFieldsEvent event) {
        injectMembers();
    }

    private void injectMembers() {
        ((ClovisBrayApplication) getActivity().getApplication()).getApiComponent().inject(this);
    }

    private static class SocketAdapter extends RecyclerView.Adapter<SocketViewHolder> {

        private final List<ManifestSocket> manifestSockets;

        SocketAdapter(List<ManifestSocket> manifestSockets) {
            this.manifestSockets = manifestSockets;
        }

        @Override
        public SocketViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_talent_node_step, parent, false);

            return new SocketViewHolder(view);
        }

        @Override
        public void onBindViewHolder(SocketViewHolder holder, int position) {
            holder.bindData(manifestSockets.get(position));
        }

        @Override
        public int getItemCount() {
            return manifestSockets.size();
        }
    }

    private static final class SocketViewHolder extends RecyclerView.ViewHolder {

        private final TextView talentNodeNameView;
        private final TextView talentNodeDescriptionView;
        private final ImageView perkIcon;

        SocketViewHolder(View itemView) {
            super(itemView);

            talentNodeNameView = (TextView) itemView.findViewById(R.id.talent_node_name);
            talentNodeDescriptionView = (TextView) itemView.findViewById(R.id.talent_node_description);
            perkIcon = (ImageView) itemView.findViewById(R.id.perk_icon);
        }

        void bindData(ManifestSocket socket) {
            talentNodeNameView.setText(socket.getDisplayProperties().getName());
            talentNodeDescriptionView.setText(socket.getDisplayProperties().getDescription());

            String iconPath = BungieDestinyService.BASE_URL + socket.getDisplayProperties().getIcon();
            Picasso.with(perkIcon.getContext()).load(iconPath).into(perkIcon);
        }

    }

    private static final class StatAdapter extends RecyclerView.Adapter<StatViewHolder> {

        private final List<Stat> stats;

        StatAdapter(List<Stat> stats) {
            this.stats = stats;
        }

        @Override
        public StatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_stat_bar,
                    parent,
                    false);

            return new StatViewHolder(view);
        }

        @Override
        public void onBindViewHolder(StatViewHolder holder, int position) {
            holder.bindData(stats.get(position));
        }

        @Override
        public int getItemCount() {
            return stats.size();
        }
    }

    private static final class StatViewHolder extends RecyclerView.ViewHolder {

        private final TextView statNameView;
        private final ImageView statBarView;
        private final TextView alternateStatBarView;
        private final int statBarColor;

        StatViewHolder(View itemView) {
            super(itemView);

            statNameView = (TextView) itemView.findViewById(R.id.stat_name);
            statBarView = (ImageView) itemView.findViewById(R.id.stat_bar);
            alternateStatBarView = (TextView) itemView.findViewById(R.id.alternate_stat_bar);

            // Must use due to api restrictions
            //noinspection deprecation
            statBarColor = itemView.getContext().getResources().getColor(R.color.destiny_yellow);
        }

        void bindData(Stat stat) {

            String statName = stat.getName();

            statNameView.setText(statName);
            statBarView.setImageDrawable(new ColorDrawable(statBarColor));

            // It seems a little inappropriate to set layout params while binding data, but we
            // calculate the weights based on the stat value, which we don't have until we call
            // bindData.
            final int STAT_BAR_HEIGHT = 50;
            TableRow.LayoutParams statBarLayoutParams;
            TableRow.LayoutParams alternateStatBarLayoutParams;

            if (StatName.forName(statName).isTextDisplay()) {

                statBarView.setVisibility(View.GONE);

                statBarLayoutParams = new TableRow.LayoutParams(0, STAT_BAR_HEIGHT, 0);
                alternateStatBarLayoutParams = new TableRow.LayoutParams(0, STAT_BAR_HEIGHT, 1);

                alternateStatBarView.setText(String.valueOf(stat.getValue()));

            } else {

                statBarView.setVisibility(View.VISIBLE);

                float scaledPercentage = stat.getValue().floatValue() / stat.getMaximumValue();
                statBarLayoutParams = new TableRow.LayoutParams(0, STAT_BAR_HEIGHT, scaledPercentage);
                alternateStatBarLayoutParams = new TableRow.LayoutParams(0, STAT_BAR_HEIGHT, 1 - scaledPercentage);
                alternateStatBarView.setText("");
            }

            statBarView.setLayoutParams(statBarLayoutParams);
            alternateStatBarView.setLayoutParams(alternateStatBarLayoutParams);

        }
    }

    private class CharacterAdapter extends RecyclerView.Adapter<CharacterViewHolder> {

        private final String itemCharacterId;
        private final List<Character> characters;

        CharacterAdapter(String itemCharacterId,
                         List<Character> characters) {
            this.itemCharacterId = itemCharacterId;
            this.characters = characters;
        }

        @Override
        public CharacterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_character, parent, false);

            return new CharacterViewHolder(view);
        }

        @Override
        public void onBindViewHolder(CharacterViewHolder holder, int position) {
            Character character = characters.get(position);
            holder.bindData(character, !Strings.isNullOrEmpty(itemCharacterId) &&
                    itemCharacterId.equals(character.getCharacterId()));
        }

        @Override
        public int getItemCount() {
            return characters.size();
        }
    }

    private final class CharacterViewHolder extends RecyclerView.ViewHolder {

        private final View root;
        private final ImageView backgroundView;
        private final TextView actionView;

        CharacterViewHolder(View itemView) {
            super(itemView);
            root = itemView;

            backgroundView = (ImageView) itemView.findViewById(R.id.character_background);
            actionView = (TextView) itemView.findViewById(R.id.move_action);
        }

        void bindData(final Character character,
                      final boolean equippable) {

            if (equippable && !(character instanceof VaultDestination)) {
                actionView.setText(R.string.equip);
            } else {
                actionView.setText(R.string.transfer);
            }

            if (character instanceof VaultDestination) {
                Picasso.with(actionView.getContext()).load(R.drawable.ic_vault_background).into(backgroundView);
            } else {
                String url = BungieDestinyService.BASE_URL + character.getBackgroundPath();
                Picasso.with(actionView.getContext()).load(url).into(backgroundView);
            }

            root.setOnClickListener(v -> {
                if (equippable) {
                    inventoryItemDetailsPresenter.equipItem(character);
                } else {
                    inventoryItemDetailsPresenter.transferItem(character);
                }
            });
        }

    }
}