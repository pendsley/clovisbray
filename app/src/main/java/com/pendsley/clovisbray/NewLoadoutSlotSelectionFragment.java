package com.pendsley.clovisbray;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;

import com.pendsley.clovisbray.model.ItemSlot;
import com.pendsley.clovisbray.presenter.NewLoadoutSlotSelectionPresenter;
import com.pendsley.clovisbray.ui.AnimationUtil;
import com.pendsley.clovisbray.viewcontroller.NewLoadoutSlotSelectionViewController;

import java.util.List;

import javax.inject.Inject;

/**
 * Fragment for displaying the available item slots for a loadout.
 *
 * @author Phil Endsley
 */
public class NewLoadoutSlotSelectionFragment extends Fragment implements NewLoadoutSlotSelectionViewController {

    public static final String TAG = "NewLoadoutSlotSelectionFrag";

    @Inject
    NewLoadoutSlotSelectionPresenter newLoadoutSlotSelectionPresenter;

    private RecyclerView loadoutSlotsRecyclerView;

    private LoadoutDetailsFragment.LoadoutSlotSelectedListener loadoutSlotSelectedListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        injectMembers();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity activity = getActivity();
        if (activity instanceof LoadoutDetailsFragment.LoadoutSlotSelectedListener) {
            loadoutSlotSelectedListener = (LoadoutDetailsFragment.LoadoutSlotSelectedListener) activity;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        newLoadoutSlotSelectionPresenter.dispose();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_new_loadout_slot_selection, container, false);

        loadoutSlotsRecyclerView = (RecyclerView) root.findViewById(R.id.loadout_slot_list);

        RecyclerView.LayoutManager loadoutSlotLayoutManager = new LinearLayoutManager(root.getContext());
        loadoutSlotsRecyclerView.setLayoutManager(loadoutSlotLayoutManager);

        LayoutAnimationController animationController = AnimationUtil.fastFadeInLayoutAnimationController(getContext());
        loadoutSlotsRecyclerView.setLayoutAnimation(animationController);

        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        newLoadoutSlotSelectionPresenter.setNewLoadoutSlotSelectionViewController(this);
        newLoadoutSlotSelectionPresenter.loadAvailableSlots();
    }

    @Override
    public void displayAvailableSlots(List<ItemSlot> availableSlots) {
        loadoutSlotsRecyclerView.setAdapter(new ItemSlotAdapter(availableSlots));
    }

    private void injectMembers() {
        ((ClovisBrayApplication) getActivity().getApplication()).getApiComponent().inject(this);
    }

    private final class ItemSlotAdapter extends RecyclerView.Adapter<ItemSlotHolder> {

        private final List<ItemSlot> itemSlots;

        ItemSlotAdapter(List<ItemSlot> itemSlots) {
            this.itemSlots = itemSlots;
        }

        @Override
        public ItemSlotHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_slot, parent, false);
            return new ItemSlotHolder(root);
        }

        @Override
        public void onBindViewHolder(ItemSlotHolder holder, int position) {
            holder.bindData(itemSlots.get(position));
        }

        @Override
        public int getItemCount() {
            return itemSlots.size();
        }
    }

    private final class ItemSlotHolder extends RecyclerView.ViewHolder {

        private final View root;
        private final TextView itemSlotView;

        ItemSlotHolder(View itemView) {
            super(itemView);

            root = itemView;
            itemSlotView = (TextView) itemView.findViewById(R.id.item_slot);
        }

        void bindData(final ItemSlot itemSlot) {
            itemSlotView.setText(itemSlot.getDescription());

            root.setOnClickListener(view -> loadoutSlotSelectedListener.onSlotSelected(itemSlot.getDescription()));
        }
    }
}
