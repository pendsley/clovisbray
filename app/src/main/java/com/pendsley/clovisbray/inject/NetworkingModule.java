package com.pendsley.clovisbray.inject;

import com.pendsley.clovisbray.network.BungieApiHeaderInterceptor;
import com.pendsley.clovisbray.network.BungieDestiny2Service;
import com.pendsley.clovisbray.network.BungieDestinyService;
import com.pendsley.clovisbray.network.BungieUserService;
import com.pendsley.clovisbray.authentication.BungieAuthenticationService;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * TODO
 */
@Module
public class NetworkingModule {

    private static final String APP_RETROFIT = "appRetrofit";
    private static final String USER_RETROFIT = "userRetrofit";
    private static final String DESTINY_RETROFIT = "destinyRetrofit";
    private static final String DESTINY_2_RETROFIT = "destiny2Retrofit";

    @Provides
    OkHttpClient provideHttpClient(BungieApiHeaderInterceptor bungieApiHeaderInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(bungieApiHeaderInterceptor)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    GsonConverterFactory provideGsonConverterFactory() {
        return GsonConverterFactory.create();
    }

    @Provides
    @Named(APP_RETROFIT)
    Retrofit provideAppRetrofit(OkHttpClient okHttpClient,
                                GsonConverterFactory gsonConverterFactory) {

        return new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("https://www.bungie.net/Platform/App/")
                .addConverterFactory(gsonConverterFactory)
                .build();
    }

    @Provides
    @Named(USER_RETROFIT)
    Retrofit provideUserServiceRetrofit(OkHttpClient okHttpClient,
                                        GsonConverterFactory gsonConverterFactory) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("https:/www.bungie.net/Platform/User/")
                .addConverterFactory(gsonConverterFactory)
                .build();
    }

    @Provides
    @Named(DESTINY_RETROFIT)
    Retrofit provideDestinyServiceRetrofit(OkHttpClient okHttpClient,
                                           GsonConverterFactory gsonConverterFactory) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("https:/www.bungie.net/d1/Platform/Destiny/")
                .addConverterFactory(gsonConverterFactory)
                .build();
    }

    @Provides
    @Named(DESTINY_2_RETROFIT)
    Retrofit provideDestiny2ServiceRetrofit(OkHttpClient okHttpClient,
                                            GsonConverterFactory gsonConverterFactory) {

        return new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("https://www.bungie.net/Platform/Destiny2/")
                .addConverterFactory(gsonConverterFactory)
                .build();
    }

    @Provides
    BungieAuthenticationService provideBungieAuthenticationService(@Named(APP_RETROFIT) Retrofit retrofit) {

        return retrofit.create(BungieAuthenticationService.class);
    }

    @Provides
    BungieUserService provideBungieUserService(@Named(USER_RETROFIT) Retrofit retrofit) {
        return retrofit.create(BungieUserService.class);
    }

    @Provides
    BungieDestinyService provideBungieDestinyService(@Named(DESTINY_RETROFIT) Retrofit retrofit) {
        return retrofit.create(BungieDestinyService.class);
    }

    @Provides
    BungieDestiny2Service provideBungieDestiny2Service(@Named(DESTINY_2_RETROFIT) Retrofit retrofit) {
        return retrofit.create(BungieDestiny2Service.class);
    }

}
