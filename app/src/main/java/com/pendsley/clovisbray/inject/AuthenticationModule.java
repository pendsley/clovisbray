package com.pendsley.clovisbray.inject;

import com.pendsley.clovisbray.authentication.model.TokenType;
import com.pendsley.clovisbray.network.BungieApiHeaderInterceptor;
import com.pendsley.clovisbray.authentication.persistence.operation.AuthenticationTokenOperation;

import java.util.Date;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Module for providing OAuth token information.
 *
 * @author Phil Endsley
 */
@Module
public class AuthenticationModule {

    public static final String ACCESS_TOKEN = "accessToken";
    public static final String ACCESS_TOKEN_EXPIRATION_TIME = "accessTokenExpirationTime";

    public static final String REFRESH_TOKEN = "refreshToken";
    public static final String REFRESH_TOKEN_EXPIRATION_TIME = "refreshTokenExpirationTime";

    public static final String AUTHENTICATED = "authenticated";

    @Provides
    @Named(ACCESS_TOKEN)
    String provideAccessToken(AuthenticationTokenOperation authenticationTokenOperation) {
        return authenticationTokenOperation.getTokenByType(TokenType.ACCESS);
    }

    @Provides
    @Named(ACCESS_TOKEN_EXPIRATION_TIME)
    Long provideAccessTokenExpirationTime(AuthenticationTokenOperation authenticationTokenOperation) {
        return authenticationTokenOperation.getTokenExpirationTimeByType(TokenType.ACCESS);
    }

    @Provides
    @Named(REFRESH_TOKEN)
    String provideRefreshToken(AuthenticationTokenOperation authenticationTokenOperation) {
        return authenticationTokenOperation.getTokenByType(TokenType.REFRESH);
    }

    @Provides
    @Named(REFRESH_TOKEN_EXPIRATION_TIME)
    Long provideRefreshTokenExpirationTime(AuthenticationTokenOperation authenticationTokenOperation) {
        return authenticationTokenOperation.getTokenExpirationTimeByType(TokenType.REFRESH);
    }

    @Provides
    BungieApiHeaderInterceptor provideApiHeaderInterceptor(@Named(ACCESS_TOKEN) String accesstoken,
                                                           @Named(ACCESS_TOKEN_EXPIRATION_TIME) Long accessTokenExpiration) {
        return new BungieApiHeaderInterceptor(accesstoken, accessTokenExpiration);
    }

    @Provides
    @Named(AUTHENTICATED)
    Boolean provideAuthenticated(@Named(ACCESS_TOKEN) String accessToken,
                                 AuthenticationTokenOperation authenticationTokenOperation) {
        long expirationTime = authenticationTokenOperation.getTokenExpirationTimeByType(TokenType.ACCESS);
        return new Date().getTime() < expirationTime &&
                accessToken != null && !"".equals(accessToken);
    }
}
