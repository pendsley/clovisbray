package com.pendsley.clovisbray.inject;

import com.pendsley.clovisbray.EditLoadoutActivity;
import com.pendsley.clovisbray.FactionProgressionFragment;
import com.pendsley.clovisbray.FactionsActivity;
import com.pendsley.clovisbray.InitialSyncActivity;
import com.pendsley.clovisbray.InventoryListActivity;
import com.pendsley.clovisbray.InventoryListFragment;
import com.pendsley.clovisbray.LoadoutDetailsFragment;
import com.pendsley.clovisbray.LoadoutsListFragment;
import com.pendsley.clovisbray.ItemDetailsFragment;
import com.pendsley.clovisbray.NewLoadoutSlotSelectionFragment;
import com.pendsley.clovisbray.PlatformSelectionActivity;
import com.pendsley.clovisbray.PostmasterActivity;
import com.pendsley.clovisbray.PostmasterFragment;
import com.pendsley.clovisbray.authentication.BungieAuthenticationActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * TOOD
 */
@Singleton
@Component(modules = { NetworkingModule.class,
        AuthenticationModule.class,
        DatabaseModule.class,
        AccountModule.class,
        AppModule.class})
public interface BungieApiComponent {

    void inject(BungieAuthenticationActivity bungieAuthenticationActivity);

    void inject(InitialSyncActivity initialSyncActivity);

    void inject(InventoryListActivity inventoryActivity);

    void inject(InventoryListFragment inventoryFragment);

    void inject(ItemDetailsFragment itemDetailsFragment);

    void inject(EditLoadoutActivity loadoutActivity);

    void inject(LoadoutsListFragment loadoutsListFragment);

    void inject(LoadoutDetailsFragment loadoutDetailsFragment);

    void inject(NewLoadoutSlotSelectionFragment newLoadoutSlotSelectionFragment);

    void inject(PostmasterActivity postmasterActivity);

    void inject(PostmasterFragment postmasterFragment);

    void inject(FactionsActivity factionsActivity);

    void inject(FactionProgressionFragment factionProgressionFragment);

    void inject(PlatformSelectionActivity platformSelectionActivity);
}
