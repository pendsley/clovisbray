package com.pendsley.clovisbray.inject;

import com.pendsley.clovisbray.ClovisBrayApplication;
import com.pendsley.clovisbray.authentication.BungieAuthenticationActivity;
import com.pendsley.clovisbray.authentication.services.TokenRefreshReceiver;
import com.pendsley.clovisbray.login.LoginActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * TODO
 */
@Singleton
@Component(modules = {DatabaseModule.class,
        AccountModule.class,
        AuthenticationModule.class,
        NetworkingModule.class,
        AppModule.class})
public interface AuthenticationComponent {

    void inject(ClovisBrayApplication clovisBrayApplication);

    void inject(LoginActivity loginActivity);

    void inject(BungieAuthenticationActivity bungieAuthenticationActivity);

    void inject(TokenRefreshReceiver tokenRefreshReceiver);
}
