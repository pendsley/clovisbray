package com.pendsley.clovisbray.inject;

import android.database.sqlite.SQLiteDatabase;

import com.pendsley.clovisbray.authentication.persistence.ClovisBrayDbHelper;
import com.pendsley.clovisbray.persistence.dao.ManifestHelper;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * TODO
 */
@Module
public class DatabaseModule {

    public static final String READABLE_DATABASE = "readableDatabase";
    public static final String WRITABLE_DATABASE = "writableDatabase";
    public static final String MANIFEST_DATABASE = "manifestDatabase";

    private final ClovisBrayDbHelper destingPerksDbeHelper;
    private final ManifestHelper manifestHelper;

    public DatabaseModule(ClovisBrayDbHelper destingPerksDbeHelper,
                          ManifestHelper manifestHelper) {
        this.destingPerksDbeHelper = destingPerksDbeHelper;
        this.manifestHelper = manifestHelper;
    }

    @Provides
    @Named(READABLE_DATABASE)
    SQLiteDatabase provideReadableDatabase() {
        return destingPerksDbeHelper.getReadableDatabase();
    }

    @Provides
    @Named(WRITABLE_DATABASE)
    SQLiteDatabase provideWritableDatabase() {
        return destingPerksDbeHelper.getWritableDatabase();
    }

    @Provides
    @Named(MANIFEST_DATABASE)
    SQLiteDatabase provideManifestDatabase() {
        return manifestHelper.getReadableDatabase();
    }
}

// TODO
//SELECT * FROM DestinySandboxPerkDefinition WHERE id + 4294967296 = 2151130854 OR id = 2151130854
