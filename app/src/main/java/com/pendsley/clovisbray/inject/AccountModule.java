package com.pendsley.clovisbray.inject;

import com.pendsley.clovisbray.model.Character;
import com.pendsley.clovisbray.persistence.dao.AccountOperation;

import java.util.List;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * TODO
 */
@Module
public class AccountModule {

    public static final String MEMBERSHIP_ID = "membershipId";
    public static final String MEMBERSHIP_TYPE = "membershipType";
    public static final String CHARACTER_IDS = "characterIds";
    public static final String CHARACTERS = "characters";

    @Provides
    @Named(MEMBERSHIP_ID)
    String provideMembershipId(AccountOperation accountOperation) {
        return accountOperation.getMembershipId();
    }

    @Provides
    @Named(MEMBERSHIP_TYPE)
    Double provideMembershipType(AccountOperation accountOperation,
                                 @Named(MEMBERSHIP_ID) String memberShipId) {
        return accountOperation.getMembershipType(memberShipId);
    }

    @Provides
    @Named(CHARACTER_IDS)
    List<String> provideCharacterIds(AccountOperation accountOperation,
                                    @Named(MEMBERSHIP_ID) String membershipId) {
        return accountOperation.getCharacterIds(membershipId);
    }

    @Provides
    @Named(CHARACTERS)
    List<Character> provideCharacter(AccountOperation accountOperation) {
        return accountOperation.getCharacters();
    }
}
