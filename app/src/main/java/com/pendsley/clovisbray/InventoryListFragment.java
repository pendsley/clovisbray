package com.pendsley.clovisbray;

import android.animation.LayoutTransition;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.common.base.Strings;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.pendsley.clovisbray.authentication.ReinjectFieldsEvent;
import com.pendsley.clovisbray.model.InventoryFilterOption;
import com.pendsley.clovisbray.model.ItemClassType;
import com.pendsley.clovisbray.model.LocalInventoryItem;
import com.pendsley.clovisbray.model.ManifestSocket;
import com.pendsley.clovisbray.network.BungieDestiny2Service;
import com.pendsley.clovisbray.network.BungieDestinyService;
import com.pendsley.clovisbray.presenter.InventoryListPresenter;
import com.pendsley.clovisbray.ui.AnimationUtil;
import com.pendsley.clovisbray.ui.DividerItemDecorator;
import com.pendsley.clovisbray.util.BusProvider;
import com.pendsley.clovisbray.viewcontroller.InventoryListViewController;
import com.pendsley.clovisbray.widget.CornerImageView;
import com.pendsley.clovisbray.widget.DownRevealMaterialSheetFab;
import com.pendsley.clovisbray.widget.SheetFloatingActionButton;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

/**
 * Fragment for displaying an item list.
 *
 * @author Phil Endsley
 */
public class InventoryListFragment extends Fragment implements InventoryListViewController,
                                                               SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = "InventoryListFragment";

    private static final String STATE_ITEM_CATEGORY = "itemCategory";
    private static final String STATE_ITEM_CLASS_TYPE = "itemClassType";
    private static final String STATE_FILTER_TEXT = "filterText";
    private static final String STATE_CHARACTER_FILTER_INDEX = "characterFilterIndex";

    @Inject
    InventoryListPresenter inventoryListPresenter;

    @Inject
    BusProvider busProvider;

    private SearchView searchView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView weaponsRecyclerView;
    private WeaponsAdapter weaponsAdapter;
    private DownRevealMaterialSheetFab materialSheetFab;

    private ItemSelectedListener itemSelectedListener;

    private String itemCategory = "";
    private ItemClassType itemClassType = null;

    private String filterText = "";

    public static InventoryListFragment create(String itemCategory,
                                               ItemClassType itemClassType) {
        Bundle arguments = new Bundle(2);
        arguments.putString(STATE_ITEM_CATEGORY, itemCategory);
        if (itemClassType != null) {
            arguments.putInt(STATE_ITEM_CLASS_TYPE, itemClassType.ordinal());
        }

        InventoryListFragment inventoryFragment = new InventoryListFragment();
        inventoryFragment.setArguments(arguments);
        return inventoryFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity activity = getActivity();
        if (activity instanceof ItemSelectedListener) {
            itemSelectedListener = (ItemSelectedListener) activity;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectMembers();

        setHasOptionsMenu(true);

        Bundle arguments = getArguments();
        if (arguments != null) {
            itemCategory = arguments.getString(STATE_ITEM_CATEGORY, "");
            int itemClassTypeIndex = arguments.getInt(STATE_ITEM_CLASS_TYPE, -1);
            if (itemClassTypeIndex >= 0) {
                itemClassType = ItemClassType.values()[itemClassTypeIndex];
            } else {
                itemClassType = ItemClassType.WEAPONS;
            }
        } else {
            itemClassType = ItemClassType.WEAPONS;
        }

        if (savedInstanceState != null) {
            int classTypeIndex = savedInstanceState.getInt(STATE_ITEM_CLASS_TYPE);
            itemClassType = classTypeIndex < 0 ? null : ItemClassType.values()[classTypeIndex];

            itemCategory = savedInstanceState.getString(STATE_ITEM_CATEGORY);
            filterText = savedInstanceState.getString(STATE_FILTER_TEXT);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        inventoryListPresenter.dispose();
    }

    @Override
    public void onResume() {
        super.onResume();
        busProvider.getBus().register(this);

        getActivity().setTitle(R.string.app_name);
    }

    @Override
    public void onPause() {
        super.onPause();
        busProvider.getBus().unregister(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_inventory_weapons, container, false);

        inventoryListPresenter.setInventoryListViewController(this);

        bindSwipeRefreshLayout(root);
        bindItemList(root);
        bindFilterView(root);

        return root;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        int classTypeIndex = itemClassType == null ? -1 : itemClassType.ordinal();
        outState.putInt(STATE_ITEM_CLASS_TYPE, classTypeIndex);
        outState.putString(STATE_ITEM_CATEGORY, itemCategory);

        if (searchView != null) {
            CharSequence query = searchView.getQuery();
            outState.putString(STATE_FILTER_TEXT, query == null ? "" : query.toString());
        } else {
            outState.putString(STATE_FILTER_TEXT, "");
        }
        if (weaponsAdapter != null) {
            outState.putString(STATE_CHARACTER_FILTER_INDEX, weaponsAdapter.getCharacterFilter());
        } else {
            outState.putString(STATE_CHARACTER_FILTER_INDEX, getString(R.string.all));
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState != null) {
            String characterFilterIndex = savedInstanceState.getString(STATE_CHARACTER_FILTER_INDEX);
            weaponsAdapter.setCharacterFilter(characterFilterIndex);
        }

        loadInventoryItems();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_item_list, menu);

        MenuItem filterItem = menu.findItem(R.id.filter_view);
        searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.filter_view));
        searchView.setQueryHint(getString(R.string.filter));
        searchView.setOnQueryTextListener(weaponsAdapter);

        LinearLayout searchBar = searchView.findViewById(R.id.search_bar);
        searchBar.setLayoutTransition(new LayoutTransition());

        MenuItemCompat.setOnActionExpandListener(filterItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {

                return true;
            }
        });

        if (!Strings.isNullOrEmpty(filterText)) {
            filterItem.expandActionView();
            searchView.setQuery(filterText, false);
        }
    }

    @Override
    public void onRefresh() {
        syncInventoryItems();
    }

    @Override
    public void displayInventoryList(List<LocalInventoryItem> inventoryItems) {

        Collections.sort(inventoryItems, (localInventoryItem1, localInventoryItem2) ->
                localInventoryItem1.getName().compareTo(localInventoryItem2.getName()));

        Collections.sort(inventoryItems, (localInventoryItem1, localInventoryItem2) -> {
            if (localInventoryItem1.getLight() == null) {
                return 1;
            } else if (localInventoryItem2.getLight() == null) {
                return -1;
            }
            return localInventoryItem2.getLight().compareTo(localInventoryItem1.getLight());
        });

        weaponsAdapter.setWeapons(inventoryItems);
        weaponsAdapter.notifyDataSetChanged();
        weaponsRecyclerView.startLayoutAnimation();

    }

    @Override
    public void showLoadingIndicator() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoadingIndicator() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public Context context() {
        return getContext();
    }

    @Subscribe
    public void onRefreshToken(ReinjectFieldsEvent event) {
        injectMembers();
    }

    private void injectMembers() {
        ((ClovisBrayApplication) getActivity().getApplication()).getApiComponent().inject(this);
    }

    private void bindSwipeRefreshLayout(View root) {
        swipeRefreshLayout = root.findViewById(R.id.refresh_container);
        swipeRefreshLayout.setProgressBackgroundColorSchemeResource(R.color.accent);
        swipeRefreshLayout.setColorSchemeResources(R.color.icons);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    private void bindItemList(View root) {
        weaponsRecyclerView = root.findViewById(R.id.weapons_list);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(root.getContext());
        weaponsRecyclerView.setLayoutManager(layoutManager);
        weaponsRecyclerView.addItemDecoration(new DividerItemDecorator(root.getContext()));

        LayoutAnimationController animationController = AnimationUtil.fastFadeInLayoutAnimationController(root.getContext());
        weaponsRecyclerView.setLayoutAnimation(animationController);

        weaponsAdapter = new WeaponsAdapter(itemSelectedListener,
                                            inventoryListPresenter.loadCharacterEmblemPaths(),
                                            context());
        weaponsRecyclerView.setAdapter(weaponsAdapter);
    }

    private void bindFilterView(View root) {
        RecyclerView filterOptionsRecyclerView = root.findViewById(R.id.sheet_list);
        RecyclerView.LayoutManager filterOptionsLayoutManager = new LinearLayoutManager(root.getContext());
        filterOptionsRecyclerView.setLayoutManager(filterOptionsLayoutManager);

        FilterOptionsAdapter filterOptionsAdapter = new FilterOptionsAdapter(inventoryListPresenter.loadFilterOptions());
        filterOptionsRecyclerView.setAdapter(filterOptionsAdapter);
        filterOptionsAdapter.notifyDataSetChanged();

        final SheetFloatingActionButton button = root.findViewById(R.id.filter_button);
        View sheet = root.findViewById(R.id.sheet_container);
        View overlay = root.findViewById(R.id.overlay);

        @SuppressWarnings("deprecation") // Can't use because of minimum sdk
                int buttonColor = root.getResources().getColor(R.color.accent);
        @SuppressWarnings("deprecation") // Can't use because of minimum sdk
                int sheetColor = root.getResources().getColor(R.color.primary_dark);

        materialSheetFab = new DownRevealMaterialSheetFab(button, sheet, overlay, sheetColor, buttonColor);

        // This is a hack to get around a known bug
        // https://code.google.com/p/android/issues/detail?id=221387
        button.post(() -> {
            button.requestLayout();
            button.setVisibility(View.VISIBLE);
        });
    }

    private void loadInventoryItems() {
        if (!Strings.isNullOrEmpty(itemCategory)) {
            inventoryListPresenter.loadInventoryListByCategory(itemCategory);
        } else {
            inventoryListPresenter.loadInventoryListByClassType(itemClassType);
        }
    }

    private void syncInventoryItems() {
        if (!Strings.isNullOrEmpty(itemCategory)) {
            inventoryListPresenter.syncInventoryItemsAndLoadByCategory(itemCategory);
        } else {
            inventoryListPresenter.syncInventoryItemsAndLoadByClassType(itemClassType);
        }
    }

    private void characterFilterSelected(String characterId) {
        materialSheetFab.hideSheet();
        weaponsAdapter.setCharacterFilter(characterId);
    }

    interface ItemSelectedListener {
        void onItemSelected(LocalInventoryItem selectedItem);
    }

    private static class WeaponsAdapter extends RecyclerView.Adapter<ItemViewHolder> implements SearchView.OnQueryTextListener {

        private final String allFilter;

        private final ItemSelectedListener itemSelectedListener;
        private final Map<String, String> emblemPathMap;

        private List<LocalInventoryItem> originalWeapons;
        private List<LocalInventoryItem> displayWeapons;

        private String filterText = "";
        private String characterFilter;

        WeaponsAdapter(ItemSelectedListener itemSelectedListener,
                       Map<String, String> emblemPathMap,
                       Context context) {

            this.itemSelectedListener = itemSelectedListener;
            this.emblemPathMap = emblemPathMap;

            displayWeapons = originalWeapons;

            allFilter = context.getString(R.string.all);
            characterFilter = allFilter;
        }


        @Override
        public ItemViewHolder onCreateViewHolder(ViewGroup parent,
                                                 int viewType) {

            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_weapon, parent, false);

            return new ItemViewHolder(view, itemSelectedListener);
        }

        @Override
        public void onBindViewHolder(ItemViewHolder holder,
                                     int position) {
            LocalInventoryItem item = displayWeapons.get(position);
            String characterId = item.getCharacterId();

            holder.bindData(item, Strings.isNullOrEmpty(characterId) ? null : emblemPathMap.get(characterId));
        }

        @Override
        public int getItemCount() {
            return displayWeapons == null ? 0 : displayWeapons.size();
        }

        @Override
        public boolean onQueryTextSubmit(final String query) {
            filterText = query.toLowerCase();
            filterItems();

            return true;
        }

        @Override
        public boolean onQueryTextChange(final String newText) {
            filterText = newText.toLowerCase();
            filterItems();

            return true;
        }

        void setWeapons(List<LocalInventoryItem> items) {
            this.originalWeapons = items;
            filterItems();
        }

        void setCharacterFilter(String characterId) {
            this.characterFilter = characterId;

            filterItems();
        }

        String getCharacterFilter() {
            return characterFilter;
        }

        private void filterItems() {
            if (originalWeapons == null) {
                return;
            }

            displayWeapons = Lists.newArrayList(Collections2.filter(originalWeapons, input -> {

                if (input == null) {
                    return false;
                }

                if (!Strings.isNullOrEmpty(filterText)) {

                    // Each word in the search view must at least partially match the result
                    String[] allFilterCriteria = filterText.split(" ");

                    for (String filterCriteria : allFilterCriteria) {

                        String typeName = input.getItemTypeName();
                        boolean passes = (input.getName().toLowerCase().contains(filterCriteria) ||
                                input.getItemType().toLowerCase().contains(filterCriteria) ||
                                (!Strings.isNullOrEmpty(typeName) && input.getItemTypeName().toLowerCase().contains(filterCriteria)) ||
                                input.getDamageType().toLowerCase().contains(filterCriteria));

                        String inputCharacterId = input.getCharacterId();

                        if (!allFilter.equals(characterFilter)) {
                            passes = passes && characterFilter.equals(inputCharacterId);
                        }

                        for (ManifestSocket socket : input.getSockets()) {
                            if (socket.getDisplayProperties().getName().toLowerCase().contains(filterCriteria)) {
                                passes = passes || allFilter.equals(characterFilter) ||
                                        characterFilter.equals(inputCharacterId);
                            }
                        }

                        if (!passes) {
                            return false;
                        }

                    }

                    return true;

                } else
                    return allFilter.equals(characterFilter) ||
                            (input.getCharacterId() != null && input.getCharacterId().equals(characterFilter));
            }));

            notifyDataSetChanged();

        }
    }

    private static final class ItemViewHolder extends RecyclerView.ViewHolder {

        private final View view;
        private final ItemSelectedListener itemSelectedListener;

        private final ImageView masterWorkBorderView;
        private final ImageView masterWorkBackgroundView;
        private final CornerImageView emblemView;
        private final TextView weaponNameView;
        private final TextView weaponTypeView;
        private final ImageView modIconView;
        private final TextView lightView;

        ItemViewHolder(View itemView,
                       ItemSelectedListener itemSelectedListener) {
            super(itemView);

            this.view = itemView;
            this.itemSelectedListener = itemSelectedListener;

            masterWorkBorderView = itemView.findViewById(R.id.masterwork_border);
            masterWorkBackgroundView = itemView.findViewById(R.id.masterwork_background);
            emblemView = itemView.findViewById(R.id.character_emblem);
            weaponNameView = itemView.findViewById(R.id.weapon_name_view);
            weaponTypeView = itemView.findViewById(R.id.weapon_type_view);
            modIconView = itemView.findViewById(R.id.mod_icon);
            lightView = itemView.findViewById(R.id.weapon_light);

            view.setTag(this);
        }

        void bindData(final LocalInventoryItem item,
                      String emblemPath) {

            weaponNameView.setText(item.getName());
            weaponTypeView.setText(item.getItemTypeName());
            lightView.setText(String.valueOf(item.getLight()));

            if (item.isEquipped()) {
                emblemView.setShowCorner(true);
            } else {
                emblemView.setShowCorner(false);
            }

            Context context = view.getContext();

            if (!Strings.isNullOrEmpty(emblemPath)) {
                String url = BungieDestinyService.BASE_URL + emblemPath;
                Picasso.with(context).load(url).into(emblemView);
            } else {

                Picasso.with(view.getContext()).load(R.drawable.ic_vault_emblem).into(emblemView);
            }

            configureSockets(item.getSockets(), context);

            configureDamageTypeView(item, context);

            if (item.isMasterWorks()) {
                // Get the drawable here so we can manually adjust the alpha value. Can't be done
                // in xml
                Drawable drawable = context.getDrawable(R.drawable.ic_masterwork);
                if (drawable != null) {
                    drawable.setAlpha(20);
                }
                masterWorkBackgroundView.setImageDrawable(drawable);
                masterWorkBackgroundView.setVisibility(View.VISIBLE);
                masterWorkBorderView.setVisibility(View.VISIBLE);
            } else {
                view.setBackgroundColor(Color.TRANSPARENT);
                masterWorkBorderView.setVisibility(View.INVISIBLE);
                masterWorkBackgroundView.setVisibility(View.INVISIBLE);
            }

            view.setOnClickListener(v -> itemSelectedListener.onItemSelected(item));
        }

        private void configureSockets(List<ManifestSocket> sockets,
                                      Context context) {
            if (sockets != null) {
                for (ManifestSocket socket : sockets) {
                    if (socket.getInventory().getBucketTypeHash().equals(3313201758L)) {

                        modIconView.setVisibility(View.VISIBLE);

                        String url = BungieDestiny2Service.BASE_URL + socket.getDisplayProperties().getIcon();
                        Picasso.with(context).load(url).into(modIconView);
                        return;
                    }
                }
            }

            modIconView.setVisibility(View.INVISIBLE);
        }

        private void configureDamageTypeView(LocalInventoryItem item,
                                             Context context) {

            String damageTypeIcon = item.getDamageIcon();
            if (Strings.isNullOrEmpty(damageTypeIcon)) {
                weaponNameView.setTextColor(Color.WHITE);
            } else {

                String damageType = item.getDamageType();

                switch (damageType) {
                    case "Solar":
                        weaponNameView.setTextColor(context.getResources().getColor(R.color.solar_element));
                        break;
                    case "Arc":
                        weaponNameView.setTextColor(context.getResources().getColor(R.color.arc_element));
                        break;
                    case "Void":
                        weaponNameView.setTextColor(context.getResources().getColor(R.color.void_element));
                        break;
                }
            }

        }

    }

    private final class FilterOptionsAdapter extends RecyclerView.Adapter<FilterOptionsHolder> {

        private List<InventoryFilterOption> filterOptions;

        FilterOptionsAdapter(List<InventoryFilterOption> filterOptions) {
            this.filterOptions = filterOptions;
        }

        @Override
        public FilterOptionsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_inventory_filter_option, parent, false);

            return new FilterOptionsHolder(view);
        }

        @Override
        public void onBindViewHolder(FilterOptionsHolder holder, int position) {
            holder.bindData(filterOptions.get(position));
        }

        @Override
        public int getItemCount() {
            return filterOptions.size();
        }

    }

    private final class FilterOptionsHolder extends RecyclerView.ViewHolder {

        private final ImageView filterBackgroundView;
        private final TextView descriptionView;

        private String characterId;

        FilterOptionsHolder(View itemView) {
            super(itemView);

            filterBackgroundView = itemView.findViewById(R.id.filter_background);
            descriptionView = itemView.findViewById(R.id.filter_description);

            itemView.setOnClickListener(view -> characterFilterSelected(characterId));
        }

        void bindData(InventoryFilterOption inventoryFilterOption) {

            this.characterId = inventoryFilterOption.getCharacterId();

            if ("".equals(characterId)) {
                // For the vault, we set the character id to a constant
                Picasso.with(filterBackgroundView.getContext()).load(R.drawable.ic_vault_background).into(filterBackgroundView);
            } else {
                String url = BungieDestinyService.BASE_URL + inventoryFilterOption.getEmblemBackgroundPath();
                Picasso.with(filterBackgroundView.getContext()).load(url).into(filterBackgroundView);
            }

            descriptionView.setText(inventoryFilterOption.getDescription());
        }
    }
}
