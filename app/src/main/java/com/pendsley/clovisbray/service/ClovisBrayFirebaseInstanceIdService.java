package com.pendsley.clovisbray.service;

import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * See https://firebase.google.com/docs/cloud-messaging/android/client?authuser=0
 */
public class ClovisBrayFirebaseInstanceIdService extends FirebaseInstanceIdService {
}
