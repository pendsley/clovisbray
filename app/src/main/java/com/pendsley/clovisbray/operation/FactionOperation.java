package com.pendsley.clovisbray.operation;

import android.util.Log;

import com.google.common.collect.Lists;
import com.pendsley.clovisbray.R;
import com.pendsley.clovisbray.inject.AccountModule;
import com.pendsley.clovisbray.model.BungieErrorCode;
import com.pendsley.clovisbray.model.BungieResponse;
import com.pendsley.clovisbray.model.CharacterProfileResponse;
import com.pendsley.clovisbray.model.DestinyComponents;
import com.pendsley.clovisbray.model.DisplayProperties;
import com.pendsley.clovisbray.model.Faction;
import com.pendsley.clovisbray.model.FactionProgression;
import com.pendsley.clovisbray.model.NetworkException;
import com.pendsley.clovisbray.network.BungieDestiny2Service;
import com.pendsley.clovisbray.persistence.dao.FactionDao;

import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import retrofit2.Response;

/**
 * Retrieves faction information.
 *
 * @author Phil Endsley
 */
public class FactionOperation {

    private static final String TAG = "FactionOperation";

    private final BungieDestiny2Service destiny2Service;
    private final int membershipType;
    private final String membershipId;

    private final FactionDao factionDao;

    @Inject
    FactionOperation(BungieDestiny2Service destiny2Service,
                     @Named(AccountModule.MEMBERSHIP_TYPE) Double membershipType,
                     @Named(AccountModule.MEMBERSHIP_ID) String membershipId,
                     FactionDao factionDao) {
        this.destiny2Service = destiny2Service;
        this.membershipType = membershipType.intValue();
        this.membershipId = membershipId;
        this.factionDao = factionDao;
    }

    public Flowable<List<FactionProgression>> retrieveFactions(String characterId) {

        return Flowable.create(emitter -> {

            Response<BungieResponse<CharacterProfileResponse>> responseWrapper = destiny2Service.getCharacterProfile(membershipType,
                                                                                                                     membershipId,
                                                                                                                     characterId,
                                                                                                                     DestinyComponents.toParameter(DestinyComponents.CHARACTER_PROGRESSIONS)).execute();

            BungieResponse<CharacterProfileResponse> bungieResponse = responseWrapper.body();
            BungieErrorCode errorCode = BungieErrorCode.forCode(bungieResponse.getErrorCode());
            if (BungieErrorCode.SUCCESS != errorCode) {
                Log.e(TAG, "Failed to look up factions. MembershipType: %d, MembershipId: %s, CharacterId: %s");


                emitter.onError(new NetworkException(R.string.faction_lookup_error));
                emitter.onComplete();
                return;
            }
            List<FactionProgression> factionProgressions = Lists.newArrayList(responseWrapper.body().getResponse().getProgressions().getData().getFactions().values());

            Iterator<FactionProgression> factionProgressionIterator = factionProgressions.iterator();
            while (factionProgressionIterator.hasNext()) {
                FactionProgression factionProgression = factionProgressionIterator.next();

                Faction faction = factionDao.getFaction(factionProgression.getFactionHash());

                if (faction.getRedacted()) {
                    factionProgressionIterator.remove();
                    continue;
                }

                DisplayProperties displayProperties = faction.getDisplayProperties();

                factionProgression.setDescription(displayProperties.getDescription());
                factionProgression.setIcon(displayProperties.getIcon());
            }

            emitter.onNext(factionProgressions);

            emitter.onComplete();
        }, BackpressureStrategy.BUFFER);
    }
}
