package com.pendsley.clovisbray.operation;

import com.google.common.base.Strings;
import com.pendsley.clovisbray.R;
import com.pendsley.clovisbray.model.Loadout;
import com.pendsley.clovisbray.model.LocalInventoryItem;
import com.pendsley.clovisbray.model.ValidationException;
import com.pendsley.clovisbray.persistence.dao.LoadoutDao;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.annotations.NonNull;

/**
 * Retrieves the items and details of a loadout.
 *
 * @author Phil Endsley
 */
public class LoadoutDetailsOperation {

    private final InventoryLookupOperation inventoryLookupOperation;
    private final LoadoutDao loadoutDao;

    @Inject
    LoadoutDetailsOperation(InventoryLookupOperation inventoryLookupOperation,
                            LoadoutDao loadoutDao) {
        this.inventoryLookupOperation = inventoryLookupOperation;
        this.loadoutDao = loadoutDao;
    }

    public Flowable<List<LocalInventoryItem>> getLoadoutItems(final Loadout loadout) {

        return Flowable.create(new FlowableOnSubscribe<List<LocalInventoryItem>>() {
            @Override
            public void subscribe(@NonNull FlowableEmitter<List<LocalInventoryItem>> emitter) throws Exception {

                List<LocalInventoryItem> loadoutItems = new ArrayList<>();

                String itemInstanceId = loadout.getPrimaryItemInstanceId();
                if (!Strings.isNullOrEmpty(itemInstanceId)) {
                    loadoutItems.add(inventoryLookupOperation.getItemByInstanceId(itemInstanceId));
                }

                itemInstanceId = loadout.getSpecialItemInstanceId();
                if (!Strings.isNullOrEmpty(itemInstanceId)) {
                    loadoutItems.add(inventoryLookupOperation.getItemByInstanceId(itemInstanceId));
                }

                itemInstanceId = loadout.getHeavyItemInstanceId();
                if (!Strings.isNullOrEmpty(itemInstanceId)) {
                    loadoutItems.add(inventoryLookupOperation.getItemByInstanceId(itemInstanceId));
                }

                itemInstanceId = loadout.getGhostItemInstanceId();
                if (!Strings.isNullOrEmpty(itemInstanceId)) {
                    loadoutItems.add(inventoryLookupOperation.getItemByInstanceId(itemInstanceId));
                }

                itemInstanceId = loadout.getHelmetItemInstanceId();
                if (!Strings.isNullOrEmpty(itemInstanceId)) {
                    loadoutItems.add(inventoryLookupOperation.getItemByInstanceId(itemInstanceId));
                }

                itemInstanceId = loadout.getGauntletsItemInstanceId();
                if (!Strings.isNullOrEmpty(itemInstanceId)) {
                    loadoutItems.add(inventoryLookupOperation.getItemByInstanceId(itemInstanceId));
                }

                itemInstanceId = loadout.getChestItemInstanceId();
                if (!Strings.isNullOrEmpty(itemInstanceId)) {
                    loadoutItems.add(inventoryLookupOperation.getItemByInstanceId(itemInstanceId));
                }

                itemInstanceId = loadout.getLegsItemInstanceId();
                if (!Strings.isNullOrEmpty(itemInstanceId)) {
                    loadoutItems.add(inventoryLookupOperation.getItemByInstanceId(itemInstanceId));
                }

                itemInstanceId = loadout.getClassItemItemInstanceId();
                if (!Strings.isNullOrEmpty(itemInstanceId)) {
                    loadoutItems.add(inventoryLookupOperation.getItemByInstanceId(itemInstanceId));
                }

                itemInstanceId = loadout.getArtifactItemInstanceId();
                if (!Strings.isNullOrEmpty(itemInstanceId)) {
                    loadoutItems.add(inventoryLookupOperation.getItemByInstanceId(itemInstanceId));
                }


                emitter.onNext(loadoutItems);
                emitter.onComplete();
            }
        }, BackpressureStrategy.BUFFER);
    }

    public Completable saveLoadout(Loadout loadout) throws ValidationException {
        if (Strings.isNullOrEmpty(loadout.getName())) {
            throw new ValidationException(R.string.loadout_validation_name);
        } else if (!anyItemsSet(loadout)) {
            throw new ValidationException(R.string.loadout_validation_item);
        }

        if (loadout.getLoadoutId() < 0) {
            return loadoutDao.getSaveNewLoadoutCompletable(loadout);
        } else {
            return loadoutDao.getUpdateLoadoutCompletable(loadout);
        }
    }

    public void deleteLoadout(int loadoutId) {
        loadoutDao.deleteLoadout(loadoutId);
    }

    private boolean anyItemsSet(Loadout loadout) {
        return !Strings.isNullOrEmpty(loadout.getPrimaryItemInstanceId()) ||
                !Strings.isNullOrEmpty(loadout.getSpecialItemInstanceId()) ||
                !Strings.isNullOrEmpty(loadout.getHeavyItemInstanceId()) ||
                !Strings.isNullOrEmpty(loadout.getGhostItemInstanceId()) ||
                !Strings.isNullOrEmpty(loadout.getHelmetItemInstanceId()) ||
                !Strings.isNullOrEmpty(loadout.getGauntletsItemInstanceId()) ||
                !Strings.isNullOrEmpty(loadout.getChestItemInstanceId()) ||
                !Strings.isNullOrEmpty(loadout.getLegsItemInstanceId()) ||
                !Strings.isNullOrEmpty(loadout.getClassItemItemInstanceId()) ||
                !Strings.isNullOrEmpty(loadout.getArtifactItemInstanceId());
    }
}
