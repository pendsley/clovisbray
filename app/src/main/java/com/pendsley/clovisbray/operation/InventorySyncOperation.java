package com.pendsley.clovisbray.operation;

import android.support.annotation.Nullable;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.pendsley.clovisbray.model.BungieResponse;
import com.pendsley.clovisbray.model.DamageType;
import com.pendsley.clovisbray.model.DestinyComponents;
import com.pendsley.clovisbray.model.DestinyInventoryComponent;
import com.pendsley.clovisbray.model.DestinyItemInstance;
import com.pendsley.clovisbray.model.DestinyOnlineItem;
import com.pendsley.clovisbray.model.InventoryDetails;
import com.pendsley.clovisbray.model.ItemClassType;
import com.pendsley.clovisbray.model.ItemResponse;
import com.pendsley.clovisbray.model.LocalInventoryItem;
import com.pendsley.clovisbray.model.ManifestInventoryItem;
import com.pendsley.clovisbray.model.ManifestPerk;
import com.pendsley.clovisbray.model.OnlinePerk;
import com.pendsley.clovisbray.model.OnlinePerksInfo;
import com.pendsley.clovisbray.model.OnlineSocketInfo;
import com.pendsley.clovisbray.model.OnlineStatsInfo;
import com.pendsley.clovisbray.model.ProfileResponse;
import com.pendsley.clovisbray.model.Socket;
import com.pendsley.clovisbray.model.Stat;
import com.pendsley.clovisbray.network.BungieDestiny2Service;
import com.pendsley.clovisbray.persistence.dao.AccountOperation;
import com.pendsley.clovisbray.persistence.dao.DamageTypeOperation;
import com.pendsley.clovisbray.persistence.dao.ItemStatsDao;
import com.pendsley.clovisbray.persistence.dao.LocalInventoryDao;
import com.pendsley.clovisbray.persistence.dao.ManifestInventoryItemOperation;
import com.pendsley.clovisbray.persistence.dao.PerkDao;
import com.pendsley.clovisbray.persistence.dao.PerkOperation;
import com.pendsley.clovisbray.persistence.dao.SocketDao;
import com.pendsley.clovisbray.util.HandlerExecutor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.inject.Inject;

import io.reactivex.Completable;
import retrofit2.Response;

/**
 * Syncs inventory with the server.
 *
 * @author Phil Endsley
 */
public class InventorySyncOperation {

    private static final String TAG = "InventorySync";

    private static final List<Long> WEAPON_HASHES = Lists.newArrayList(
            1498876634L, // Primary
            2465295065L, // Special
            953998645L, // Heavy
            4046403665L); // Weapons

    private static final List<Long> ARMOR_HASHES = Lists.newArrayList(
            3003523923L, // Armor
            3448274439L, // Helmet
            3551918588L, // Gauntlets
            14239492L, // Chest
            20886954L, // Legs
            1585787867L, // Class
            434908299L, // Artifact
            4023194814L); // Ghost

    private final BungieDestiny2Service destiny2Service;
    private final ManifestInventoryItemOperation manifestInventoryItemOperation;
    private final LocalInventoryDao localInventoryDao;
    private final PerkDao perkDao;
    private final PerkOperation perkOperation;
    private final SocketDao socketDao;
    private final ItemStatsDao itemStatsDao;
    private final AccountOperation accountOperation;
    private final DamageTypeOperation damageTypeOperation;

    // We're not injecting these, since it's possible they:
    // 1. Don't exist yet
    // 2. Will change if an account sync is done first
    private int membershipType;
    private String membershipId;

    @Inject
    InventorySyncOperation(BungieDestiny2Service destiny2Service,
                           ManifestInventoryItemOperation manifestInventoryItemOperation,
                           LocalInventoryDao localInventoryDao,
                           PerkDao perkDao,
                           PerkOperation perkOperation,
                           SocketDao socketDao,
                           ItemStatsDao itemStatsDao,
                           AccountOperation accountOperation,
                           DamageTypeOperation damageTypeOperation) {
        this.destiny2Service = destiny2Service;
        this.manifestInventoryItemOperation = manifestInventoryItemOperation;
        this.localInventoryDao = localInventoryDao;
        this.perkDao = perkDao;
        this.perkOperation = perkOperation;
        this.socketDao = socketDao;
        this.itemStatsDao = itemStatsDao;
        this.accountOperation = accountOperation;
        this.damageTypeOperation = damageTypeOperation;
    }

    public Completable deleteAllAndSyncCharactersAndInventory(@Nullable SyncOperationCallback syncOperationCallback) {
        return Completable.create(emitter -> {

            localInventoryDao.deleteAllItems();
            perkDao.deleteAllPerks();
            socketDao.deleteAllSockets();
            itemStatsDao.deleteAllStats();

            emitter.onComplete();

        }).andThen(syncCharactersAndInventory(syncOperationCallback));
    }

    public Completable syncCharactersAndInventory(@Nullable SyncOperationCallback syncOperationCallback) {

        return Completable.create(emitter -> {
            membershipId = accountOperation.getMembershipId();
            membershipType = accountOperation.getMembershipType(membershipId).intValue();

            retrieveVaultItemIds(syncOperationCallback);
            emitter.onComplete();
        });

    }

    private void retrieveVaultItemIds(@Nullable SyncOperationCallback syncOperationCallback) {

        try {

            Response<BungieResponse<ProfileResponse>> response = destiny2Service.getProfile(membershipType,
                                                                                            membershipId,
                                                                                            DestinyComponents.toParameter(DestinyComponents.CHARACTER_EQUIPMENT,
                                                                                                                          DestinyComponents.CHARACTER_INVENTORIES,
                                                                                                                          DestinyComponents.PROFILE_INVENTORIES)).execute();
            ProfileResponse profileResponse = response.body().getResponse();

            if (profileResponse == null) {
                String message = "Error: " + response.code() + " - " + response.message();
                Log.e(TAG, message);
                Crashlytics.logException(new RuntimeException(message));
                return;
            }

            List<DestinyOnlineItem> onlineItems = Lists.newArrayList();

            for (DestinyInventoryComponent itemCollections : profileResponse.getCharacterInventories().getData().values()) {
                for (DestinyOnlineItem item : itemCollections.getItems()) {
                    if (Strings.isNullOrEmpty(item.getItemInstanceId())) {
                        continue;
                    }
                    Long bucketHash = item.getBucketHash();
                    if (WEAPON_HASHES.contains(bucketHash)) {
                        item.setItemClassType(ItemClassType.WEAPONS);
                        onlineItems.add(item);
                    } else if (ARMOR_HASHES.contains(bucketHash)) {
                        item.setItemClassType(ItemClassType.ARMOR);
                        onlineItems.add(item);
                    }
                }
            }

            // TODO

            for (DestinyInventoryComponent itemCollections : profileResponse.getCharacterEquipment().getData().values()) {
                for (DestinyOnlineItem item : itemCollections.getItems()) {
                    if (Strings.isNullOrEmpty(item.getItemInstanceId())) {
                        continue;
                    }
                    Long bucketHash = item.getBucketHash();
                    if (WEAPON_HASHES.contains(bucketHash)) {
                        item.setItemClassType(ItemClassType.WEAPONS);
                        onlineItems.add(item);
                    } else if (ARMOR_HASHES.contains(bucketHash)) {
                        item.setItemClassType(ItemClassType.ARMOR);
                        onlineItems.add(item);
                    }
                }
            }

            for (DestinyOnlineItem item : profileResponse.getProfileInventory().getData().getItems()) {
                if (Strings.isNullOrEmpty(item.getItemInstanceId())) {
                    continue;
                }
                Long bucketHash = item.getBucketHash();
                if (WEAPON_HASHES.contains(bucketHash)) {
                    item.setItemClassType(ItemClassType.WEAPONS);
                    onlineItems.add(item);
                } else if (ARMOR_HASHES.contains(bucketHash)) {
                    item.setItemClassType(ItemClassType.ARMOR);
                    onlineItems.add(item);
                } else if (bucketHash.equals(138197802L)) {
                    item.setItemClassType(ItemClassType.WEAPONS);
                    onlineItems.add(item);

                }
            }

            retrieveItemDetails(onlineItems, syncOperationCallback);

        } catch (Throwable e) {
            Log.e(TAG, "Error: ", e);
            Crashlytics.logException(e);
        }
    }


    private void retrieveItemDetails(List<DestinyOnlineItem> onlineItems,
                                     @Nullable final SyncOperationCallback syncOperationCallback) {

        if (onlineItems.isEmpty()) {
            return;
        }

        final int totalItems = onlineItems.size();

        final CountDownLatch countDownLatch = new CountDownLatch(onlineItems.size());
        final CountDownLatch syncingCountdownLatch = new CountDownLatch(onlineItems.size());

        ExecutorService executorService = Executors.newCachedThreadPool();

        // Collect all the details in memory, then batch insert/delete them. This is for performance
        final Set<ManifestInventoryItem> itemSet = Collections.newSetFromMap(new ConcurrentHashMap<>());
        final ConcurrentHashMap<String, List<ManifestPerk>> perkMap = new ConcurrentHashMap<>();
        final ConcurrentHashMap<String, List<Socket>> socketsMap = new ConcurrentHashMap<>();
        final ConcurrentHashMap<String, List<Stat>> statsMap = new ConcurrentHashMap<>();
        final Set<String> itemsToDelete = Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>());
        final Map<String, LocalInventoryItem> allCurrentLocalItems = localInventoryDao.getAllLocalItemsMap();

        Log.d(TAG, "Retrieving details for " + onlineItems.size() + " items");
        for (final DestinyOnlineItem onlineItem : onlineItems) {
            executorService.submit(() -> {

                try {

                    Response<BungieResponse<ItemResponse>> response =
                            destiny2Service.getItem(membershipType,
                                                    membershipId,
                                                    onlineItem.getItemInstanceId(),
                                                    DestinyComponents.toParameter(DestinyComponents.ITEM_COMMON_DATA,
                                                                                  DestinyComponents.ITEM_INSTANCES,
                                                                                  DestinyComponents.ITEM_PERKS,
                                                                                  DestinyComponents.ITEM_PLUG_STATES,
                                                                                  DestinyComponents.ITEM_SOCKETS,
                                                                                  DestinyComponents.ITEM_STATS)).execute();

                    ItemResponse itemResponse = response.body().getResponse();
                    if (itemResponse == null) {
                        return;
                    }

                    ManifestInventoryItem manifestItem =
                            manifestInventoryItemOperation.getManifestInventoryItem(onlineItem.getItemHash());

                    manifestItem.setItemInstanceId(onlineItem.getItemInstanceId());
                    manifestItem.setCharacterId(Strings.nullToEmpty(itemResponse.getCharacterId()));
                    manifestItem.setItemClassType(onlineItem.getItemClassType());
                    manifestItem.setMasterWorks(itemResponse.getItem().getData().getState() >= 4);

                    InventoryDetails inventoryDetails = manifestItem.getInventory();
                    if (inventoryDetails != null) {
                        manifestItem.setItemCategory(localInventoryDao.getItemCategoryByBucketHash(inventoryDetails.getBucketTypeHash()));
                    } else {
                        manifestItem.setItemCategory("");
                    }

                    Long bucketHash = manifestItem.getInventory().getBucketTypeHash();
                    if (WEAPON_HASHES.contains(bucketHash)) {
                        manifestItem.setItemClassType(ItemClassType.WEAPONS);
                    } else if (ARMOR_HASHES.contains(bucketHash)) {
                        manifestItem.setItemClassType(ItemClassType.ARMOR);
                    } else {
                        return;
                    }

                    DestinyItemInstance itemInstance = itemResponse.getInstance().getData();

                    manifestItem.setEquipped(itemInstance.isEquipped());

                    Long damageTypeHash = itemInstance.getDamageTypeHash();
                    if (damageTypeHash != null) {

                        DamageType damageType = damageTypeOperation.getDamageType(damageTypeHash);

                        if (damageType != null && damageType.showIcon()) {

                            manifestItem.setDamageType(damageType.getDisplayProperties().getName());
                            manifestItem.setDamageTypeIcon(damageType.getTransparentIconPath());
                        } else {

                            manifestItem.setDamageType("");
                            manifestItem.setDamageTypeIcon("");
                        }
                    } else {

                        manifestItem.setDamageType("");
                        manifestItem.setDamageTypeIcon("");
                    }

                    Stat primaryStat = itemInstance.getPrimaryStat();
                    if (primaryStat != null) {
                        manifestItem.setLightLevel(primaryStat.getValue());
                    }

                    LocalInventoryItem oldLocalItem = allCurrentLocalItems.get(manifestItem.getItemInstanceId());

                    if (oldLocalItem != null) {

                        allCurrentLocalItems.remove(oldLocalItem.getInstanceId());

                        boolean same = oldLocalItem.getCharacterId().equals(manifestItem.getCharacterId()) &&
                                oldLocalItem.getLight().equals(manifestItem.getLightLevel()) &&
                                oldLocalItem.isEquipped().equals(manifestItem.isEquipped());

                        // If there's no difference, saving details will create a duplicate entry
                        if (same) {
                            return;
                        }

                        // If there is a difference, we need to delete the existing details
                        // before we insert the updated ones
                        itemsToDelete.add(manifestItem.getItemInstanceId());

                    }

                    itemSet.add(manifestItem);

                    List<ManifestPerk> perks = new ArrayList<>();

                    OnlinePerksInfo perksContainer = itemResponse.getPerks().getData();
                    if (perksContainer != null) {
                        perks = getPerks(perksContainer.getPerks());
                    }

                    if (!perks.isEmpty()) {
                        perkMap.put(onlineItem.getItemInstanceId(), perks);
                    }

                    OnlineSocketInfo socketContainer = itemResponse.getSockets().getData();
                    if (socketContainer != null) {
                        List<Socket> sockets = socketContainer.getSockets();
                        socketsMap.put(manifestItem.getItemInstanceId(), sockets);
                    }

                    OnlineStatsInfo statsContainer = itemResponse.getStats().getData();

                    if (statsContainer != null) {

                        List<Stat> itemStats = Lists.newArrayList(statsContainer.getStats().values());
                        statsMap.put(manifestItem.getItemInstanceId(), itemStats);
                    }

                } catch (Exception e) {
                    Log.e(TAG, "Error getting item details", e);
                    Crashlytics.logException(e);

                } finally {
                    countDownLatch.countDown();

                    final long currentItem = countDownLatch.getCount();

                    if (syncOperationCallback != null) {
                        new HandlerExecutor().execute(() -> syncOperationCallback.onProgressChanged((int) (((totalItems - currentItem) / (float) totalItems) * 100)));
                    }

                    if (currentItem == 0) {
                        handleItemDetailsPersistence(allCurrentLocalItems,
                                                     itemSet,
                                                     perkMap,
                                                     socketsMap,
                                                     statsMap,
                                                     itemsToDelete);
                        syncingCountdownLatch.countDown();
                    } else {
                        syncingCountdownLatch.countDown();
                    }
                }
            });

        }

        try {

            // We use two countdown latches. countDownLatch counts down every time we process an item.
            // syncingCountdownLatch counts down after every operation in the finally block is ocmpleted.
            // We wait for syncingCountdownLatch to finish so we can guarantee the database writes/
            // deletes are finished before we return. This is hacky, but the quickest way I could
            // think of to do this.
            syncingCountdownLatch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }


    private List<ManifestPerk> getPerks(List<OnlinePerk> onlinePerks) {
        return perkOperation.getPerks(onlinePerks);
    }

    private void handleItemDetailsPersistence(Map<String, LocalInventoryItem> allCurrentLocalItems,
                                              Set<ManifestInventoryItem> itemSet,
                                              ConcurrentHashMap<String, List<ManifestPerk>> perksMap,
                                              Map<String, List<Socket>> socketsMap,
                                              ConcurrentHashMap<String, List<Stat>> statsMap,
                                              Set<String> itemsToDelete) {

        itemsToDelete.addAll(allCurrentLocalItems.keySet());

        if (!itemsToDelete.isEmpty()) {
            localInventoryDao.deleteItems(itemsToDelete);
            perkDao.deletePerks(itemsToDelete);
            itemStatsDao.deleteStats(itemsToDelete);
            socketDao.deleteSocketsForInstanceIds(itemsToDelete);
        }

        localInventoryDao.batchInsertItems(itemSet);
        perkDao.batchInsertPerks(perksMap);
        socketDao.batchInsertSockets(socketsMap);
        itemStatsDao.batchInsertStats(statsMap);
    }

    public interface SyncOperationCallback {

        void onProgressChanged(int percentComplete);

    }
}