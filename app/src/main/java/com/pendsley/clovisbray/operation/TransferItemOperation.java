package com.pendsley.clovisbray.operation;

import com.google.common.base.Strings;
import com.pendsley.clovisbray.inject.AccountModule;
import com.pendsley.clovisbray.model.BungieErrorCode;
import com.pendsley.clovisbray.model.BungieResponse;
import com.pendsley.clovisbray.model.Character;
import com.pendsley.clovisbray.model.LocalInventoryItem;
import com.pendsley.clovisbray.model.TransferItemArguments;
import com.pendsley.clovisbray.model.VaultDestination;
import com.pendsley.clovisbray.network.BungieDestiny2Service;
import com.pendsley.clovisbray.persistence.dao.LocalInventoryDao;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.annotations.NonNull;
import io.reactivex.annotations.Nullable;
import retrofit2.Response;

/**
 * Operation for transferring items.
 *
 * @author Phil Endsley
 */
public class TransferItemOperation {

    private final Double membershipType;

    private final BungieDestiny2Service destiny2Service;
    private final LocalInventoryDao localInventoryDao;
    private final List<Character> characters;

    @Inject
    TransferItemOperation(BungieDestiny2Service destiny2Service,
                          LocalInventoryDao localInventoryDao,
                          @Named(AccountModule.MEMBERSHIP_TYPE) Double membershipType,
                          @Named(AccountModule.CHARACTERS) List<Character> characters) {
        this.destiny2Service = destiny2Service;
        this.localInventoryDao = localInventoryDao;
        this.membershipType = membershipType;
        this.characters = characters;
    }

    public Flowable<LocalInventoryItem> transferItem(final Character destinationCharacter,
                                                     final LocalInventoryItem item) {


        final Character currentCharacterLocation = getCharacterForId(item.getCharacterId());

        boolean moveToVault = currentCharacterLocation != null;

        Flowable<LocalInventoryItem> transferFlowable;

        if (moveToVault) {

            if (destinationCharacter instanceof VaultDestination) {
                transferFlowable = Flowable.create(emitter -> {
                    emitter.onNext(performTransfer(item,
                                                   destinationCharacter,
                                                   currentCharacterLocation.getCharacterId(),
                                                   true));
                    emitter.onComplete();
                }, BackpressureStrategy.BUFFER);
            } else {

                transferFlowable = Flowable.create(emitter -> {
                    LocalInventoryItem transferredItem = performTransfer(item,
                                                                         currentCharacterLocation,
                                                                         currentCharacterLocation.getCharacterId(),
                                                                         true);
                    emitter.onNext(performTransfer(transferredItem,
                                                   destinationCharacter,
                                                   destinationCharacter.getCharacterId(),
                                                   false));
                    emitter.onComplete();
                }, BackpressureStrategy.BUFFER);
            }

        } else {
            transferFlowable = Flowable.create(emitter -> {

                emitter.onNext(performTransfer(item,
                                               destinationCharacter,
                                               destinationCharacter.getCharacterId(),
                                               false));
                emitter.onComplete();
            }, BackpressureStrategy.BUFFER);
        }

        return transferFlowable;
    }

    private LocalInventoryItem performTransfer(final LocalInventoryItem item,
                                               final Character destinationCharacter,
                                               final String characterId,
                                               final boolean movingToVault) throws IOException {


        final TransferItemArguments transferItemArguments = new TransferItemArguments(membershipType.intValue(),
                                                                                      item.getInstanceId(),
                                                                                      characterId,
                                                                                      new BigDecimal(item.getHashId()),
                                                                                      movingToVault);

        Response<BungieResponse<Object>> transferResponse = destiny2Service.transferItem(transferItemArguments).execute();

        BungieErrorCode errorCode = BungieErrorCode.forCode(transferResponse.body().getErrorCode());


        if (BungieErrorCode.SUCCESS == errorCode) {

            String destinationCharacterId = destinationCharacter.getCharacterId();
            localInventoryDao.updateWeaponCharacterIndex(item.getInstanceId(), destinationCharacterId);

            item.setCharacterId(destinationCharacterId);

            return item;

        } else {
            String errorMessage = errorCode.getErrorMessage();
            // Bungie services return an error code of 1623 - ItemNotFound if the vault is full when
            // trying to transfer to it.
            if (movingToVault && BungieErrorCode.ITEM_NOT_FOUND == errorCode) {
                errorMessage = "Vault is full";
            }
            throw new RuntimeException(errorMessage);
        }
    }

    @Nullable
    private Character getCharacterForId(String itemCharacterId) {

        if (Strings.isNullOrEmpty(itemCharacterId)) {
            return null;
        }

        for (Character character : characters) {
            if (character.getCharacterId().equals(itemCharacterId)) {
                return character;
            }
        }

        return null;
    }

}

