package com.pendsley.clovisbray.operation;

import com.pendsley.clovisbray.persistence.dao.ItemStatsDao;
import com.pendsley.clovisbray.persistence.dao.LocalInventoryDao;
import com.pendsley.clovisbray.persistence.dao.PerkDao;
import com.pendsley.clovisbray.persistence.dao.SocketDao;

import javax.inject.Inject;

public class DeleteInventoryOperation {

    private final LocalInventoryDao localInventoryDao;
    private final PerkDao perkDao;
    private final SocketDao socketDao;
    private final ItemStatsDao itemStatsDao;

    @Inject
    DeleteInventoryOperation(LocalInventoryDao localInventoryDao,
                             PerkDao perkDao,
                             SocketDao socketDao,
                             ItemStatsDao itemStatsDao) {
        this.localInventoryDao = localInventoryDao;
        this.perkDao = perkDao;
        this.socketDao = socketDao;
        this.itemStatsDao = itemStatsDao;
    }

    public void deleteAllItems() {
        localInventoryDao.deleteAllItems();
        perkDao.deleteAllPerks();
        itemStatsDao.deleteAllStats();
        socketDao.deleteAllSockets();
    }
}
