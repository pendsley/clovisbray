package com.pendsley.clovisbray.operation;

import com.crashlytics.android.Crashlytics;
import com.google.common.base.Optional;
import com.pendsley.clovisbray.inject.AccountModule;
import com.pendsley.clovisbray.model.BungieErrorCode;
import com.pendsley.clovisbray.model.BungieResponse;
import com.pendsley.clovisbray.model.EquipItemArguments;
import com.pendsley.clovisbray.model.EquipOperationResult;
import com.pendsley.clovisbray.model.LocalInventoryItem;
import com.pendsley.clovisbray.network.BungieDestiny2Service;
import com.pendsley.clovisbray.persistence.dao.LocalInventoryDao;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import retrofit2.Response;

/**
 * Operation for equipping items.
 *
 * @author Phil Endsley
 */
public class EquipItemOperation {

    private final BungieDestiny2Service destiny2Service;
    private final InventoryLookupOperation inventoryLookupOperation;
    private final LocalInventoryDao localInventoryDao;
    private final int membershipType;

    @Inject
    EquipItemOperation(BungieDestiny2Service destiny2Service,
                       InventoryLookupOperation inventoryLookupOperation,
                       LocalInventoryDao localInventoryDao,
                       @Named(AccountModule.MEMBERSHIP_TYPE) Double membershipType) {
        this.destiny2Service = destiny2Service;
        this.inventoryLookupOperation = inventoryLookupOperation;
        this.localInventoryDao = localInventoryDao;
        this.membershipType = membershipType.intValue();
    }

    public Flowable<EquipOperationResult> equipItem(final LocalInventoryItem item,
                                                    final String characterId) {
        return Flowable.create(emitter -> {

            final EquipItemArguments equipItemArguments = new EquipItemArguments(membershipType, item.getInstanceId(),
                    characterId);

            final String itemType = item.getItemType();
            final String characterId1 = item.getCharacterId();

            try {

                Response<BungieResponse<Object>> responseWrapper = destiny2Service.equipItem(equipItemArguments).execute();

                BungieResponse<Object> response = responseWrapper.body();

                BungieErrorCode errorCode = BungieErrorCode.forCode(response.getErrorCode());

                if (BungieErrorCode.SUCCESS == errorCode) {
                    Optional<LocalInventoryItem> previousItemOptional =
                            inventoryLookupOperation.getCurrentlyEquippedItem(itemType, characterId1);

                    LocalInventoryItem previousItem = null;
                    String previousInstanceId = null;

                    if (previousItemOptional.isPresent()) {

                        previousItem = previousItemOptional.get();
                        previousInstanceId = previousItem.getInstanceId();
                        previousItem.setEquipped(false);

                    } else {
                        Crashlytics.logException(new Exception(String.format("Could not find previously equipped item on character [%s]", characterId1)));
                    }

                    localInventoryDao.updateWeaponEquippedStatus(item.getInstanceId(),
                            previousInstanceId);

                    item.setEquipped(true);

                    emitter.onNext(new EquipOperationResult(previousItem, item));
                    emitter.onComplete();

                } else {
                    throw new RuntimeException(errorCode.getErrorMessage());
                }

            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }, BackpressureStrategy.BUFFER);
    }

}
