package com.pendsley.clovisbray.operation;

import com.crashlytics.android.Crashlytics;
import com.google.common.collect.Lists;
import com.pendsley.clovisbray.authentication.ReinjectFieldsEvent;
import com.pendsley.clovisbray.model.BungieErrorCode;
import com.pendsley.clovisbray.model.BungieResponse;
import com.pendsley.clovisbray.model.Character;
import com.pendsley.clovisbray.model.DestinyComponents;
import com.pendsley.clovisbray.model.NetworkException;
import com.pendsley.clovisbray.model.ProfileResponse;
import com.pendsley.clovisbray.model.UserInfo;
import com.pendsley.clovisbray.network.BungieDestiny2Service;
import com.pendsley.clovisbray.persistence.dao.AccountOperation;
import com.pendsley.clovisbray.util.BusProvider;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import retrofit2.Response;

/**
 * Retrieves characters for the account
 */
public class CharacterOperation {

    private final BungieDestiny2Service destiny2Service;

    private final AccountOperation accountOperation;

    private final BusProvider busProvider;

    @Inject
    CharacterOperation(BungieDestiny2Service destiny2Service,
                       AccountOperation accountOperation,
                       BusProvider busProvider) {
        this.destiny2Service = destiny2Service;
        this.accountOperation = accountOperation;
        this.busProvider = busProvider;
    }

    public Flowable<Void> retrieveAndSaveCharacters(UserInfo userInfo) {

        return Flowable.create(emitter -> {

            Response<BungieResponse<ProfileResponse>> responseWrapper =
                    destiny2Service.getProfile(userInfo.getMembershipType(),
                                               userInfo.getMembershipId(),
                                               DestinyComponents.toParameter(DestinyComponents.CHARACTERS))
                            .execute();

            BungieResponse<ProfileResponse> response = responseWrapper.body();

            BungieErrorCode bungieErrorCode = BungieErrorCode.forCode(response.getErrorCode());

            if (BungieErrorCode.SUCCESS != bungieErrorCode) {
                NetworkException exception = new NetworkException(bungieErrorCode.getErrorMessageResource());
                emitter.onError(exception);
                return;
            }

            handleProfileResponse(response, userInfo);

            busProvider.getBus().post(new ReinjectFieldsEvent());

            emitter.onComplete();

        }, BackpressureStrategy.BUFFER);
    }

    public void updateCharacterClasses() {

        List<Character> characters = accountOperation.getCharacters();
        for (Character character : characters) {
            String characterClass = accountOperation.getClass(character.getClassHash());
            accountOperation.updateCharacterClass(character.getCharacterId(), characterClass);
        }
    }

    private void handleProfileResponse(BungieResponse<ProfileResponse> response,
                                       UserInfo userInfo) {

        Map<String, Character> charactersMap = response.getResponse().getCharacters().getData();
        List<Character> characters = Lists.newArrayList(charactersMap.values());
        String membershipId = characters.get(0).getMembershipId();
        int membershipType = characters.get(0).getMembershipType();

        accountOperation.saveCharacterInfo(membershipId, characters);
        accountOperation.updateUserMembershipInfo(userInfo.getDisplayName(),
                                                  membershipType,
                                                  membershipId);
    }

}
