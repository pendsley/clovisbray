package com.pendsley.clovisbray.operation;

import android.support.annotation.Nullable;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.google.common.primitives.Ints;
import com.pendsley.clovisbray.model.ItemClassType;
import com.pendsley.clovisbray.model.LocalInventoryItem;
import com.pendsley.clovisbray.model.ManifestSocket;
import com.pendsley.clovisbray.model.Stat;
import com.pendsley.clovisbray.model.StatName;
import com.pendsley.clovisbray.persistence.dao.ItemStatsDao;
import com.pendsley.clovisbray.persistence.dao.LocalInventoryDao;
import com.pendsley.clovisbray.persistence.dao.SocketDao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;

/**
 * Retrieves inventory items based on various search parameters.
 *
 * @author Phil Endsley
 */
public class InventoryLookupOperation {

    private final LocalInventoryDao localInventoryDao;
    private final ItemStatsDao itemStatsDao;
    private final SocketDao socketDao;

    @Inject
    InventoryLookupOperation(LocalInventoryDao localInventoryDao,
                             ItemStatsDao itemStatsDao,
                             SocketDao socketDao) {
        this.localInventoryDao = localInventoryDao;
        this.itemStatsDao = itemStatsDao;
        this.socketDao = socketDao;
    }

    public Flowable<List<LocalInventoryItem>> getLookupByClassTypeFlowable(ItemClassType itemClassType) {
        return localInventoryDao.getLocalItemsByClassTypeFlowable(itemClassType)
                .map(inventoryItems -> new ArrayList<>(Lists.transform(inventoryItems, item -> {
                    item.setStats(getStats(item.getInstanceId()));
                    return item;
                }))).map(inventoryItems -> new ArrayList<>(Lists.transform(inventoryItems, item -> {
                    item.setSockets(getSockets(item.getInstanceId()));
                    return item;
                })));
    }

    public Flowable<List<LocalInventoryItem>> getLookupByCategoryFlowable(String itemCategory) {
        return localInventoryDao.getLocalItemsByCategoryFlowable(itemCategory)
                .map(inventoryItems -> new ArrayList<>(Lists.transform(inventoryItems, item -> {
                    item.setStats(getStats(item.getInstanceId()));
                    return item;
                }))).map(inventoryItems -> new ArrayList<>(Lists.transform(inventoryItems, item -> {
                    item.setSockets(getSockets(item.getInstanceId()));
                    return item;
                })));
    }

    Optional<LocalInventoryItem> getCurrentlyEquippedItem(String itemCategory,
                                                          String characterId) {
        Optional<LocalInventoryItem> itemOptional = localInventoryDao.getCurrentlyEquippedItem(itemCategory, characterId);
        if (itemOptional.isPresent()) {
            String itemInstanceId = itemOptional.get().getInstanceId();
            itemOptional.get().setStats(getStats(itemInstanceId));
        }

        return itemOptional;
    }

    @Nullable
    LocalInventoryItem getItemByInstanceId(String itemInstanceId) {
        LocalInventoryItem item = localInventoryDao.getLocalItemByInstanceId(itemInstanceId);
        if (item == null) {
            return null;
        }
        item.setStats(getStats(itemInstanceId));
        return item;
    }

    List<Stat> getStats(String itemInstanceId) {

        List<Stat> stats = itemStatsDao.getStatsForInstanceId(itemInstanceId);
        Collections.sort(stats, new Comparator<Stat>() {
            @Override
            public int compare(Stat stat1, Stat stat2) {
                Integer value1 = StatName.forName(stat1.getName()).getSortOrder();
                Integer value2 = StatName.forName(stat2.getName()).getSortOrder();

                return value1.compareTo(value2);
            }
        });

        return stats;
    }

    private List<ManifestSocket> getSockets(String itemInstanceId) {

        List<ManifestSocket> sockets = socketDao.getSocketsForItemInstanceId(itemInstanceId);
        Collections.sort(sockets, (socket1, socket2) -> {
            int result = Ints.compare(socket1.getColumnIndex() , socket2.getColumnIndex());
            if (result == 0) {
                result = Ints.compare(socket1.getRowIndex(), socket1.getColumnIndex());
            }

            return result;
        });

        return sockets;
    }


}
