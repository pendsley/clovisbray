package com.pendsley.clovisbray.operation;

import android.util.Log;

import com.pendsley.clovisbray.model.BungieResponse;
import com.pendsley.clovisbray.model.UserInfo;
import com.pendsley.clovisbray.model.UserMemberships;
import com.pendsley.clovisbray.network.BungieUserService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import retrofit2.Response;

/**
 * Different network operations for platform selection.
 *
 * @author Phil Endsley
 */
public class PlatformSelectionOperation {

    private static final String TAG = "PlatformSelectionOprtn";

    private final BungieUserService userService;

    @Inject
    PlatformSelectionOperation(BungieUserService userService) {
        this.userService = userService;
    }

    public Flowable<List<UserInfo>> loadUserInfos() {

        return Flowable.create(emitter -> {

                    Response<BungieResponse<UserMemberships>> response = userService.getMembershipData().execute();

                    BungieResponse<UserMemberships> membershipsResponse = response.body();
                    if (membershipsResponse == null) {
                        Log.e(TAG, "Failed to get membership data");
                        emitter.onError(new RuntimeException("Failed to get membership data"));
                        return;
                    }

                    List<UserInfo> allPlatformsInfo = membershipsResponse.getResponse().getDestinyMemberships();
                    emitter.onNext(allPlatformsInfo);
                    emitter.onComplete();
                },
                BackpressureStrategy.BUFFER);
    }
}
