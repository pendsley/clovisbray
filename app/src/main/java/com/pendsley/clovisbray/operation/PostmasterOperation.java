package com.pendsley.clovisbray.operation;

import android.util.Log;

import com.google.common.base.Strings;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.pendsley.clovisbray.R;
import com.pendsley.clovisbray.inject.AccountModule;
import com.pendsley.clovisbray.model.BungieErrorCode;
import com.pendsley.clovisbray.model.BungieResponse;
import com.pendsley.clovisbray.model.DestinyComponents;
import com.pendsley.clovisbray.model.DestinyInventoryComponent;
import com.pendsley.clovisbray.model.DestinyOnlineItem;
import com.pendsley.clovisbray.model.ItemResponse;
import com.pendsley.clovisbray.model.ManifestInventoryItem;
import com.pendsley.clovisbray.model.NetworkException;
import com.pendsley.clovisbray.model.PostmasterTransferRequest;
import com.pendsley.clovisbray.model.ProfileResponse;
import com.pendsley.clovisbray.model.Stat;
import com.pendsley.clovisbray.network.BungieDestiny2Service;
import com.pendsley.clovisbray.persistence.dao.ManifestInventoryItemOperation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import retrofit2.Response;

/**
 * Retrieves items that are at the postmaster.
 *
 * @author Phil Endsley
 */
public class PostmasterOperation {

    private static final String TAG = "PostmasterOperation";

    private final BungieDestiny2Service destiny2Service;
    private final ManifestInventoryItemOperation manifestInventoryItemOperation;
    private final int membershipType;
    private final String membershipId;

    @Inject
    PostmasterOperation(BungieDestiny2Service destiny2Service,
                        ManifestInventoryItemOperation manifestInventoryItemOperation,
                        @Named(AccountModule.MEMBERSHIP_TYPE) Double membershipType,
                        @Named(AccountModule.MEMBERSHIP_ID) String membershipId) {
        this.destiny2Service = destiny2Service;
        this.manifestInventoryItemOperation = manifestInventoryItemOperation;
        this.membershipType = membershipType.intValue();
        this.membershipId = membershipId;
    }

    public Flowable<List<ManifestInventoryItem>> getItems(String characterId) {

        return Flowable.create(emitter -> {

            Response<BungieResponse<ProfileResponse>> responseWrapper = destiny2Service.getProfile(membershipType,
                    membershipId,
                    DestinyComponents.toParameter(DestinyComponents.CHARACTER_INVENTORIES)).execute();

            BungieResponse<ProfileResponse> bungieResponse = responseWrapper.body();
            BungieErrorCode errorCode = BungieErrorCode.forCode(bungieResponse.getErrorCode());
            if (BungieErrorCode.SUCCESS != errorCode) {
                Log.e(TAG, "Failed to get character profile. " + errorCode.getErrorMessage());

                emitter.onError(new NetworkException(R.string.postmaster_lookup_error));
                emitter.onComplete();
                return;
            }


            List<DestinyOnlineItem> onlineItems = new ArrayList<>();

            Map<String, DestinyInventoryComponent> map = bungieResponse.getResponse().getCharacterInventories().getData();
            onlineItems.addAll(map.get(characterId).getItems());
            List<ManifestInventoryItem> manifestInventoryItems = getPostmasterItems(onlineItems);


            emitter.onNext(manifestInventoryItems);
            emitter.onComplete();

        }, BackpressureStrategy.BUFFER);
    }

    public Completable transferFromPostMaster(List<ManifestInventoryItem> items,
                                              String characterId) {

        return Completable.create(emitter -> {

            for (ManifestInventoryItem item : items) {

                PostmasterTransferRequest postmasterTransferRequest = new PostmasterTransferRequest();
                postmasterTransferRequest.setMembershipType(membershipType);
                postmasterTransferRequest.setCharacterId(characterId);
                String instanceId = item.getItemInstanceId();
                if (Strings.isNullOrEmpty(instanceId)) {
                    postmasterTransferRequest.setItemId("0");
                } else {
                    postmasterTransferRequest.setItemId(item.getItemInstanceId());
                }
                postmasterTransferRequest.setItemReferenceHash(item.getHash());
                postmasterTransferRequest.setStackSize(item.getQuantity());

                Response<BungieResponse<Object>> response = destiny2Service.transferFromPostmaster(postmasterTransferRequest).execute();
                BungieResponse<Object> bungieResponse = response.body();
                BungieErrorCode errorCode = BungieErrorCode.forCode(bungieResponse.getErrorCode());
                if (BungieErrorCode.SUCCESS != errorCode) {
                    Log.e(TAG, "Failed to get transfer item from postmaster. " + errorCode.getErrorMessage());

                    emitter.onError(new NetworkException(errorCode.getErrorMessageResource()));
                    emitter.onComplete();
                    return;
                }

            }

            emitter.onComplete();
        });
    }

    private List<ManifestInventoryItem> getPostmasterItems(List<DestinyOnlineItem> allInventoryItems) {
        List<DestinyOnlineItem> list =  Lists.newArrayList(Collections2.filter(allInventoryItems,
                item ->  item != null && (215593132L == item.getBucketHash() ||
                        (4294967296L + item.getBucketHash()) == 215593132L)
        ));

        return toManifestItems(list);
    }

    private List<ManifestInventoryItem> toManifestItems(List<DestinyOnlineItem> onlineItems) {
        List<ManifestInventoryItem> manifestItems = new ArrayList<>();

        for (DestinyOnlineItem onlineItem : onlineItems) {

            ManifestInventoryItem manifestInventoryItem =
                    manifestInventoryItemOperation.getManifestInventoryItem(onlineItem.getItemHash());

            String instanceId = onlineItem.getItemInstanceId();
            manifestInventoryItem.setItemInstanceId(instanceId);
            manifestInventoryItem.setQuantity(onlineItem.getQuantity());


            if (!Strings.isNullOrEmpty(instanceId)) {
                try {
                    Response<BungieResponse<ItemResponse>> response =
                            destiny2Service.getItem(membershipType,
                                    membershipId,
                                    instanceId,
                                    DestinyComponents.toParameter(DestinyComponents.ITEM_COMMON_DATA,
                                            DestinyComponents.ITEM_INSTANCES,
                                            DestinyComponents.ITEM_PERKS,
                                            DestinyComponents.ITEM_PLUG_STATES,
                                            DestinyComponents.ITEM_SOCKETS,
                                            DestinyComponents.ITEM_STATS)).execute();
                    BungieResponse<ItemResponse> bungieResponse = response.body();
                    BungieErrorCode errorCode = BungieErrorCode.forCode(bungieResponse.getErrorCode());
                    if (BungieErrorCode.SUCCESS == errorCode) {
                        ItemResponse itemResponse = bungieResponse.getResponse();
                        manifestInventoryItem.setMasterWorks(itemResponse.getItem().getData().getState() >= 4);

                        Stat primaryStat = itemResponse.getInstance().getData().getPrimaryStat();
                        if (primaryStat != null) {
                            manifestInventoryItem.setLightLevel(primaryStat.getValue());
                        }
                    }
                } catch (IOException e) {
                    Log.e(TAG, e.getMessage(), e);
                }
            }
            manifestItems.add(manifestInventoryItem);
        }

        return manifestItems;
    }

}
