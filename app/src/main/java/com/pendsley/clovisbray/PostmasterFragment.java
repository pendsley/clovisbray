package com.pendsley.clovisbray;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.common.base.Strings;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.pendsley.clovisbray.authentication.ReinjectFieldsEvent;
import com.pendsley.clovisbray.model.Character;
import com.pendsley.clovisbray.model.ManifestInventoryItem;
import com.pendsley.clovisbray.network.BungieDestinyService;
import com.pendsley.clovisbray.presenter.PostmasterPresenter;
import com.pendsley.clovisbray.ui.AnimationUtil;
import com.pendsley.clovisbray.ui.DividerItemDecorator;
import com.pendsley.clovisbray.viewcontroller.PostmasterViewController;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

/**
 * Fragment for viewing and managing postmaster inventory.
 *
 * @author Phil Endsley
 */
public class PostmasterFragment extends Fragment implements PostmasterViewController,
        SwipeRefreshLayout.OnRefreshListener {

    static final String TAG = "PostmasterFragment";

    @Inject
    PostmasterPresenter postmasterPresenter;

    private View root;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView postmasterItemsRecyclerView;
    private FloatingActionButton transferItemsButton;

    // This is in the activity's toolbar
    private Spinner characterToolbarSpinner;
    private PostmasterItemsAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        injectFields();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        characterToolbarSpinner = (Spinner) getActivity().findViewById(R.id.character_spinner);
        characterToolbarSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                loadPostmasterItemsForSelectedCharacter();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        root = inflater.inflate(R.layout.fragment_postmaster_list, container, false);

        swipeRefreshLayout = (SwipeRefreshLayout) root.findViewById(R.id.refresh_container);
        swipeRefreshLayout.setProgressBackgroundColorSchemeResource(R.color.accent);
        swipeRefreshLayout.setColorSchemeResources(R.color.icons);
        swipeRefreshLayout.setOnRefreshListener(this);

        postmasterItemsRecyclerView = (RecyclerView) root.findViewById(R.id.postmaster_items_list);


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(postmasterItemsRecyclerView.getContext());
        postmasterItemsRecyclerView.setLayoutManager(layoutManager);
        postmasterItemsRecyclerView.addItemDecoration(new DividerItemDecorator(postmasterItemsRecyclerView.getContext()));

        LayoutAnimationController animationController = AnimationUtil.fastFadeInLayoutAnimationController(postmasterItemsRecyclerView.getContext());
        postmasterItemsRecyclerView.setLayoutAnimation(animationController);

        transferItemsButton = (FloatingActionButton) root.findViewById(R.id.button_transfer);
        transferItemsButton.setOnClickListener(view -> {

            List<ManifestInventoryItem> selectedItems = adapter.getSelectedItems();
            postmasterPresenter.transferItemsFromPostmaster(selectedItems, getSelectedCharacterId());

        });
        transferItemsButton.post(() -> {
            transferItemsButton.requestLayout();
            transferItemsButton.setVisibility(View.VISIBLE);
        });

        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_postmaster, menu);

        MenuItem item = menu.findItem(R.id.clear_all);
        item.setOnMenuItemClickListener(menuItem -> {
            adapter.clearAllSelectedItems();
            return true;
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        postmasterPresenter.attachView(this);
        loadPostmasterItemsForSelectedCharacter();
    }

    @Override
    public void showLoadingIndicator() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoadingIndicator() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void displayItems(List<ManifestInventoryItem> postMasterItems) {

        adapter = new PostmasterItemsAdapter(postMasterItems);
        postmasterItemsRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void displayError(int errorMessageResource) {

        int textColor = root.getContext().getResources().getColor(R.color.secondary_text);
        final Snackbar snackbar = Snackbar.make(root, errorMessageResource, Snackbar.LENGTH_LONG)
                .setActionTextColor(textColor);
        snackbar.setAction(R.string.dismiss, view -> snackbar.dismiss());
        snackbar.show();
    }

    @Override
    public void onRefresh() {
        loadPostmasterItemsForSelectedCharacter();
    }

    @Subscribe
    public void onRefreshToken(ReinjectFieldsEvent event) {
        injectFields();
    }

    private void loadPostmasterItemsForSelectedCharacter() {
        postmasterPresenter.loadPostMasterItems(getSelectedCharacterId());
    }

    private String getSelectedCharacterId() {
        return ((Character) characterToolbarSpinner.getSelectedItem()).getCharacterId();
    }

    private void injectFields() {
        ((ClovisBrayApplication) getActivity().getApplication()).getApiComponent().inject(this);
    }

    private static class PostmasterItemsAdapter extends RecyclerView.Adapter<ItemViewHolder> {

        private List<ManifestInventoryItem> postmasterItems;
        private Map<Long, Boolean> selectedItemsMap;

        PostmasterItemsAdapter(List<ManifestInventoryItem> postmasterItems) {
            this.postmasterItems = postmasterItems;
            selectedItemsMap = new HashMap<>();
            for (ManifestInventoryItem item : postmasterItems){
                selectedItemsMap.put(item.getHash(), true);
            }
        }

        @Override
        public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_postmaster_item, parent, false);
            return new ItemViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ItemViewHolder holder, int position) {

            ManifestInventoryItem item = postmasterItems.get(position);

            holder.bindData(item, selectedItemsMap);
        }

        @Override
        public int getItemCount() {
            return postmasterItems.size();
        }

        void clearAllSelectedItems() {
            for (Long hash : selectedItemsMap.keySet()) {
                selectedItemsMap.put(hash, false);
            }
            notifyDataSetChanged();
        }

        List<ManifestInventoryItem> getSelectedItems() {
            return Lists.newArrayList(Collections2.filter(postmasterItems,
                    item -> selectedItemsMap.get(item.getHash())));
        }
    }

    private static final class ItemViewHolder extends RecyclerView.ViewHolder {

        private final View view;
        private final ImageView iconView;
        private final ImageView masterWorkBorderView;
        private final ImageView masterWorkBackgroundView;
        private final TextView weaponNameView;
        private final TextView weaponTypeView;
        private final TextView lightView;
        private final CheckBox selectedView;

        ItemViewHolder(View itemView) {
            super(itemView);

            view = itemView;
            iconView = (ImageView) itemView.findViewById(R.id.item_icon);
            masterWorkBorderView = (ImageView) itemView.findViewById(R.id.masterwork_border);
            masterWorkBackgroundView = (ImageView) itemView.findViewById(R.id.masterwork_background);
            weaponNameView = (TextView) itemView.findViewById(R.id.weapon_name_view);
            weaponTypeView = (TextView) itemView.findViewById(R.id.weapon_type_view);
            lightView = (TextView) itemView.findViewById(R.id.weapon_light);
            selectedView = (CheckBox) itemView.findViewById(R.id.selected);
        }

        void bindData(ManifestInventoryItem inventoryItem,
                      Map<Long, Boolean> selectedItemsMap) {
            weaponNameView.setText(inventoryItem.getDisplayProperties().getName());

            weaponTypeView.setText(inventoryItem.getItemTypeAndTierDisplayName());

            Integer lightLevel = inventoryItem.getLightLevel();
            if (lightLevel == null || lightLevel == 0) {
                lightView.setVisibility(View.INVISIBLE);
            } else {
                lightView.setText(String.valueOf(lightLevel));
                lightView.setVisibility(View.VISIBLE);
            }

            if (!Strings.isNullOrEmpty(inventoryItem.getDisplayProperties().getIcon())) {
                String url = BungieDestinyService.BASE_URL + inventoryItem.getDisplayProperties().getIcon();
                Picasso.with(iconView.getContext()).load(url).into(iconView);
                iconView.setVisibility(View.VISIBLE);
            } else {
                iconView.setVisibility(View.INVISIBLE);
            }

            if (inventoryItem.isMasterWorks()) {
                // Get the drawable here so we can manually adjust the alpha value. Can't be done
                // in xml
                Context context = masterWorkBackgroundView.getContext();
                Drawable drawable = context.getDrawable(R.drawable.ic_masterwork);
                if (drawable != null) {
                    drawable.setAlpha(20);
                }
                masterWorkBackgroundView.setImageDrawable(drawable);
                masterWorkBackgroundView.setVisibility(View.VISIBLE);
                masterWorkBorderView.setVisibility(View.VISIBLE);
            } else {
                view.setBackgroundColor(Color.TRANSPARENT);
                masterWorkBorderView.setVisibility(View.INVISIBLE);
                masterWorkBackgroundView.setVisibility(View.INVISIBLE);
            }


            selectedView.setOnCheckedChangeListener(null);
            selectedView.setChecked(selectedItemsMap.get(inventoryItem.getHash()));

            selectedView.setOnCheckedChangeListener((compoundButton, checked) ->
                    selectedItemsMap.put(inventoryItem.getHash(), checked));

            itemView.setOnClickListener(view -> selectedView.setChecked(!selectedView.isChecked()));
        }

    }

}
