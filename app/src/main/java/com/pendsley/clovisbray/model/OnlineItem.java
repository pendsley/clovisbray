package com.pendsley.clovisbray.model;

/**
 * Created by Phillip on 1/29/2017.
 */
public class OnlineItem {

    private Long itemHash;
    private String itemId;
    private Long bucketHash;
    private Integer characterIndex;
    private ItemClassType itemClassType;

    public Long getItemHash() {
        return itemHash;
    }

    public void setItemHash(Long itemHash) {
        this.itemHash = itemHash;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public Long getBucketHash() {
        return bucketHash;
    }

    public void setBucketHash(Long bucketHash) {
        this.bucketHash = bucketHash;
    }

    public Integer getCharacterIndex() {
        return characterIndex;
    }

    public void setCharacterIndex(Integer characterIndex) {
        this.characterIndex = characterIndex;
    }

    public ItemClassType getItemClassType() {
        return itemClassType;
    }

    public void setItemClassType(ItemClassType itemClassType) {
        this.itemClassType = itemClassType;
    }
}
