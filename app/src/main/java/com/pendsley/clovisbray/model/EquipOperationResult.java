package com.pendsley.clovisbray.model;

/**
 * Wrapper for the updated items after equipping.
 *
 * @author Phil Endsley
 */
public class EquipOperationResult {

    private final LocalInventoryItem previousItem;
    private final LocalInventoryItem newItem;

    public EquipOperationResult(LocalInventoryItem previousItem,
                                LocalInventoryItem newItem) {
        this.previousItem = previousItem;
        this.newItem = newItem;
    }

    public LocalInventoryItem getPreviousItem() {
        return previousItem;
    }

    public LocalInventoryItem getNewItem() {
        return newItem;
    }
}
