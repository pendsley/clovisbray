package com.pendsley.clovisbray.model;

/**
 * Membership information for a Destiny profile.
 *
 * @author Phil Endsley
 */
public class DestinyMembership {

    private String displayName;
    private int membershipType;
    private String membershipId;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public int getMembershipType() {
        return membershipType;
    }

    public void setMembershipType(int membershipType) {
        this.membershipType = membershipType;
    }

    public String getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(String membershipId) {
        this.membershipId = membershipId;
    }
}
