package com.pendsley.clovisbray.model;

/**
 * Plug information for a socket.
 *
 * @author Phil Endsley
 */
public class ManifestPlug {

    private String uiPlugLabel;

    public String getUiPlugLabel() {
        return uiPlugLabel;
    }

    public void setUiPlugLabel(String uiPlugLabel) {
        this.uiPlugLabel = uiPlugLabel;
    }
}
