package com.pendsley.clovisbray.model;

/**
 * Exception that is thrown if validation fails.
 *
 * @author Phil Endsley
 */
public class ValidationException extends Exception {

    private final int messageResource;

    public ValidationException(int messageResource) {
        this.messageResource= messageResource;
    }

    public int getMessageResource() {
        return messageResource;
    }
}
