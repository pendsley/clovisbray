package com.pendsley.clovisbray.model;

import com.google.gson.annotations.SerializedName;

/**
 * A Destiny character.
 *
 * @author Phil Endsley
 */
public class Character {

    private String membershipId;
    private int membershipType;
    private String characterId;
    private String emblemPath;
    private Long classHash;
    private ClassInformation characterClass;
    private Integer characterIndex;

    @SerializedName("emblemBackgroundPath")
    private String backgroundPath;

    @SerializedName("light")
    private Integer lightLevel;

    public String getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(String membershipId) {
        this.membershipId = membershipId;
    }

    public int getMembershipType() {
        return membershipType;
    }

    public void setMembershipType(int membershipType) {
        this.membershipType = membershipType;
    }

    public String getCharacterId() {
        return characterId;
    }

    public void setCharacterId(String characterId) {
        this.characterId = characterId;
    }

    public String getEmblemPath() {
        return emblemPath;
    }

    public void setEmblemPath(String emblemPath) {
        this.emblemPath = emblemPath;
    }

    public String getBackgroundPath() {
        return backgroundPath;
    }

    public void setBackgroundPath(String backgroundPath) {
        this.backgroundPath = backgroundPath;
    }

    public Long getClassHash() {
        return classHash;
    }

    public void setClassHash(Long classHash) {
        this.classHash = classHash;
    }

    public ClassInformation getCharacterClass() {
        return characterClass;
    }

    public void setCharacterClass(ClassInformation characterClass) {
        this.characterClass = characterClass;
    }

    public Integer getLightLevel() {
        return lightLevel;
    }

    public void setLightLevel(Integer lightLevel) {
        this.lightLevel = lightLevel;
    }

    public Integer getCharacterIndex() {
        return characterIndex;
    }

    public void setCharacterIndex(Integer characterIndex) {
        this.characterIndex = characterIndex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Character character = (Character) o;

        return characterId != null ? characterId.equals(character.characterId) : character.characterId == null;

    }

    @Override
    public int hashCode() {
        return characterId != null ? characterId.hashCode() : 0;
    }
}
