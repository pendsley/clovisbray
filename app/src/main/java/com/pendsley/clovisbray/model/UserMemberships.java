package com.pendsley.clovisbray.model;

import java.util.List;

/**
 * Membership data for a Bungie user.
 *
 * @author Phil Endsley
 */
public class UserMemberships {

    private List<UserInfo> destinyMemberships;

    public List<UserInfo> getDestinyMemberships() {
        return destinyMemberships;
    }

    public void setDestinyMemberships(List<UserInfo> destinyMemberships) {
        this.destinyMemberships = destinyMemberships;
    }
}
