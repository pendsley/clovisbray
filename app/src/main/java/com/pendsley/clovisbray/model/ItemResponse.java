package com.pendsley.clovisbray.model;

/**
 * TODO
 */
public class ItemResponse {

    private String characterId;
    private DestinyComponent<DestinyItem> item;
    private DestinyComponent<DestinyItemInstance> instance;
    private DestinyComponent<OnlinePerksInfo> perks;
    private DestinyComponent<OnlineSocketInfo> sockets;
    private DestinyComponent<OnlineStatsInfo> stats;

    public String getCharacterId() {
        return characterId;
    }

    public void setCharacterId(String characterId) {
        this.characterId = characterId;
    }

    public DestinyComponent<DestinyItem> getItem() {
        return item;
    }

    public void setItem(DestinyComponent<DestinyItem> item) {
        this.item = item;
    }

    public DestinyComponent<DestinyItemInstance> getInstance() {
        return instance;
    }

    public void setInstance(DestinyComponent<DestinyItemInstance> instance) {
        this.instance = instance;
    }

    public DestinyComponent<OnlinePerksInfo> getPerks() {
        return perks;
    }

    public void setPerks(DestinyComponent<OnlinePerksInfo> perks) {
        this.perks = perks;
    }

    public DestinyComponent<OnlineSocketInfo> getSockets() {
        return sockets;
    }

    public void setSockets(DestinyComponent<OnlineSocketInfo> sockets) {
        this.sockets = sockets;
    }

    public DestinyComponent<OnlineStatsInfo> getStats() {
        return stats;
    }

    public void setStats(DestinyComponent<OnlineStatsInfo> stats) {
        this.stats = stats;
    }
}
