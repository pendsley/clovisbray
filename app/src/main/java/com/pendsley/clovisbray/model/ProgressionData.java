package com.pendsley.clovisbray.model;

import java.util.Map;

/**
 * TODO
 */
public class ProgressionData {

    private Map<Long, FactionProgression> factions;

    public Map<Long, FactionProgression> getFactions() {
        return factions;
    }

    public void setFactions(Map<Long, FactionProgression> factions) {
        this.factions = factions;
    }
}
