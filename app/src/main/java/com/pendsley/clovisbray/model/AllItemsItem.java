package com.pendsley.clovisbray.model;

/**
 * TODO
 */
public class AllItemsItem {

    private Long itemHash;
    private String itemId;
    private Long bucketHash;

    public Long getItemHash() {
        return itemHash;
    }

    public void setItemHash(Long itemHash) {
        this.itemHash = itemHash;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public Long getBucketHash() {
        return bucketHash;
    }

    public void setBucketHash(Long bucketHash) {
        this.bucketHash = bucketHash;
    }
}
