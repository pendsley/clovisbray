package com.pendsley.clovisbray.model;

/**
 * Asset locations from Destiny.
 *
 * @author Phil Endsley
 */
public class DestinyAssets {

    public static final String ALL_FILTER_OPTION_BACKGROUND = "/common/destiny_content/icons/23c693a4a13a463d8e186522ccc419cb.jpg";
}
