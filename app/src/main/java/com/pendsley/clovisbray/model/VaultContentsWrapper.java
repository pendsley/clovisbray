package com.pendsley.clovisbray.model;

/**
 * Created by Phillip on 1/29/2017.
 */
public class VaultContentsWrapper {

    private VaultContents data;

    public VaultContents getData() {
        return data;
    }

    public void setData(VaultContents data) {
        this.data = data;
    }
}
