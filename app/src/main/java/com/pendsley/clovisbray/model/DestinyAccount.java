package com.pendsley.clovisbray.model;

import java.util.List;

/**
 * TODO
 */
public class DestinyAccount {

    private UserInfo userInfo;
    private List<Character> characters;

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public List<Character> getCharacters() {
        return characters;
    }

    public void setCharacters(List<Character> characters) {
        this.characters = characters;
    }
}
