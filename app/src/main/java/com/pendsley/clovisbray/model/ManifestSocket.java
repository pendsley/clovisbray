package com.pendsley.clovisbray.model;

import java.util.List;

/**
 * TODO
 */
public class ManifestSocket {

    private DisplayProperties displayProperties;
    private InventoryDetails inventory;
    private String itemTypeDisplayName;
    private Long hash;
    private ManifestPlug plug;
    private List<SocketPerk> perks;
    private int columnIndex;
    private int rowIndex;

    /**
     * Convenience Fields
     */
    private Boolean showSocketPlug = true;

    public DisplayProperties getDisplayProperties() {
        return displayProperties;
    }

    public void setDisplayProperties(DisplayProperties displayProperties) {
        this.displayProperties = displayProperties;
    }

    public InventoryDetails getInventory() {
        return inventory;
    }

    public void setInventory(InventoryDetails inventory) {
        this.inventory = inventory;
    }

    public String getItemTypeDisplayName() {
        return itemTypeDisplayName;
    }

    public void setItemTypeDisplayName(String itemTypeDisplayName) {
        this.itemTypeDisplayName = itemTypeDisplayName;
    }

    public Long getHash() {
        return hash;
    }

    public void setHash(Long hash) {
        this.hash = hash;
    }

    public ManifestPlug getPlug() {
        return plug;
    }

    public void setPlug(ManifestPlug plug) {
        this.plug = plug;
    }

    public List<SocketPerk> getPerks() {
        return perks;
    }

    public void setPerks(List<SocketPerk> perks) {
        this.perks = perks;
    }

    public int getColumnIndex() {
        return columnIndex;
    }

    public void setColumnIndex(int columnIndex) {
        this.columnIndex = columnIndex;
    }

    public int getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    public Boolean getShowSocketPlug() {
        return showSocketPlug;
    }

    public void setShowSocketPlug(Boolean showSocketPlug) {
        this.showSocketPlug = showSocketPlug;
    }
}
