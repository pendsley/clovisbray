package com.pendsley.clovisbray.model;

/**
 * TODO
 * @param <T>
 */
public class ResponseData<T> {

    private T item;

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }
}
