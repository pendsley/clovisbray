package com.pendsley.clovisbray.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * A character loadout.
 *
 * @author Phil Endsley
 */
public class Loadout implements Parcelable {

    private int loadoutId = -1;
    private String name;
    private String characterId;
    private String primaryItemInstanceId;
    private String specialItemInstanceId;
    private String heavyItemInstanceId;
    private String ghostItemInstanceId;
    private String helmetItemInstanceId;
    private String gauntletsItemInstanceId;
    private String chestItemInstanceId;
    private String legsItemInstanceId;
    private String classItemItemInstanceId;
    private String artifactItemInstanceId;

    public int getLoadoutId() {
        return loadoutId;
    }

    public void setLoadoutId(int loadoutId) {
        this.loadoutId = loadoutId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCharacterId() {
        return characterId;
    }

    public void setCharacterId(String characterId) {
        this.characterId = characterId;
    }

    public String getPrimaryItemInstanceId() {
        return primaryItemInstanceId;
    }

    public void setPrimaryItemInstanceId(String primaryItemInstanceId) {
        this.primaryItemInstanceId = primaryItemInstanceId;
    }

    public String getSpecialItemInstanceId() {
        return specialItemInstanceId;
    }

    public void setSpecialItemInstanceId(String specialItemInstanceId) {
        this.specialItemInstanceId = specialItemInstanceId;
    }

    public String getHeavyItemInstanceId() {
        return heavyItemInstanceId;
    }

    public void setHeavyItemInstanceId(String heavyItemInstanceId) {
        this.heavyItemInstanceId = heavyItemInstanceId;
    }

    public String getGhostItemInstanceId() {
        return ghostItemInstanceId;
    }

    public void setGhostItemInstanceId(String ghostItemInstanceId) {
        this.ghostItemInstanceId = ghostItemInstanceId;
    }

    public String getHelmetItemInstanceId() {
        return helmetItemInstanceId;
    }

    public void setHelmetItemInstanceId(String helmetItemInstanceId) {
        this.helmetItemInstanceId = helmetItemInstanceId;
    }

    public String getGauntletsItemInstanceId() {
        return gauntletsItemInstanceId;
    }

    public void setGauntletsItemInstanceId(String gauntletsItemInstanceId) {
        this.gauntletsItemInstanceId = gauntletsItemInstanceId;
    }

    public String getChestItemInstanceId() {
        return chestItemInstanceId;
    }

    public void setChestItemInstanceId(String chestItemInstanceId) {
        this.chestItemInstanceId = chestItemInstanceId;
    }

    public String getLegsItemInstanceId() {
        return legsItemInstanceId;
    }

    public void setLegsItemInstanceId(String legsItemInstanceId) {
        this.legsItemInstanceId = legsItemInstanceId;
    }

    public String getClassItemItemInstanceId() {
        return classItemItemInstanceId;
    }

    public void setClassItemItemInstanceId(String classItemItemInstanceId) {
        this.classItemItemInstanceId = classItemItemInstanceId;
    }

    public String getArtifactItemInstanceId() {
        return artifactItemInstanceId;
    }

    public void setArtifactItemInstanceId(String artifactItemInstanceId) {
        this.artifactItemInstanceId = artifactItemInstanceId;
    }

    public Loadout() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.loadoutId);
        dest.writeString(this.name);
        dest.writeString(this.characterId);
        dest.writeString(this.primaryItemInstanceId);
        dest.writeString(this.specialItemInstanceId);
        dest.writeString(this.heavyItemInstanceId);
        dest.writeString(this.ghostItemInstanceId);
        dest.writeString(this.helmetItemInstanceId);
        dest.writeString(this.gauntletsItemInstanceId);
        dest.writeString(this.chestItemInstanceId);
        dest.writeString(this.legsItemInstanceId);
        dest.writeString(this.classItemItemInstanceId);
        dest.writeString(this.artifactItemInstanceId);
    }

    protected Loadout(Parcel in) {
        this.loadoutId = in.readInt();
        this.name = in.readString();
        this.characterId = in.readString();
        this.primaryItemInstanceId = in.readString();
        this.specialItemInstanceId = in.readString();
        this.heavyItemInstanceId = in.readString();
        this.ghostItemInstanceId = in.readString();
        this.helmetItemInstanceId = in.readString();
        this.gauntletsItemInstanceId = in.readString();
        this.chestItemInstanceId = in.readString();
        this.legsItemInstanceId = in.readString();
        this.classItemItemInstanceId = in.readString();
        this.artifactItemInstanceId = in.readString();
    }

    public static final Creator<Loadout> CREATOR = new Creator<Loadout>() {
        @Override
        public Loadout createFromParcel(Parcel source) {
            return new Loadout(source);
        }

        @Override
        public Loadout[] newArray(int size) {
            return new Loadout[size];
        }
    };
}
