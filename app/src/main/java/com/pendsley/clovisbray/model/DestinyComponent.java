package com.pendsley.clovisbray.model;

/**
 * TODO
 * @param <T>
 */
public class DestinyComponent<T> {

    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
