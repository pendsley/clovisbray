package com.pendsley.clovisbray.model;

import com.google.gson.annotations.SerializedName;

/**
 * TODO
 */
public class OnlinePerk {

    private Long perkHash;
    private String iconPath;
    @SerializedName("isActive")
    private Boolean active;
    private boolean visible;

    public Long getPerkHash() {
        return perkHash;
    }

    public void setPerkHash(Long perkHash) {
        this.perkHash = perkHash;
    }

    public String getIconPath() {
        return iconPath;
    }

    public void setIconPath(String iconPath) {
        this.iconPath = iconPath;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}
