package com.pendsley.clovisbray.model;

import java.util.List;

/**
 * TODO
 */
public class OnlineSocketInfo {

    private List<Socket> sockets;

    public List<Socket> getSockets() {
        return sockets;
    }

    public void setSockets(List<Socket> sockets) {
        this.sockets = sockets;
    }
}
