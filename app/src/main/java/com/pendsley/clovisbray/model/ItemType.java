package com.pendsley.clovisbray.model;

import java.util.List;

/**
 * TODO
 */
public enum ItemType {
    // Primaries
    PRIMARY(2, "Primary"),
    SPECIAL(3, "Special"),
    HEAVY(4, "Heavy"),
    GHOST(39, "Ghost"),
    HELMET(45, "Helmet"),
    GAUNTLET(46, "Gauntlets"),
    CHEST(47, "Chest"),
    LEG(48, "Legs"),
    CLASS(49, "Class Item"),
    ARTIFACT(38, "Artifact"),
    OTHER(0, "Other");

    private final int key;
    private final String description;

    ItemType(int key,
             String description) {
        this.key = key;
        this.description = description;
    }

    public int getKey() {
        return key;
    }

    public String getDescription() {
        return description;
    }

    public static String descriptionForKey(int key) {
        for (ItemType itemType : values()) {
            if (key == itemType.getKey()) {
                return itemType.getDescription();
            }
        }

        return "Other";
    }

    public static String descriptionForKeys(List<Integer> keys) {
        for (ItemType itemType : values()) {
            if (keys.contains(itemType.getKey())) {
                return itemType.getDescription();
            }
        }

        return "Other";
    }
}
