package com.pendsley.clovisbray.model;

/**
 * TODO
 */
public class Faction {

    private DisplayProperties displayProperties;
    private Boolean redacted;

    public DisplayProperties getDisplayProperties() {
        return displayProperties;
    }

    public void setDisplayProperties(DisplayProperties displayProperties) {
        this.displayProperties = displayProperties;
    }

    public Boolean getRedacted() {
        return redacted;
    }

    public void setRedacted(Boolean redacted) {
        this.redacted = redacted;
    }
}
