package com.pendsley.clovisbray.model;

/**
 * TODO
 */
public class FactionProgression {

    private Long factionHash;
    private Long progressionHash;
    private Integer level;
    private Integer levelCap;
    private Integer progressToNextLevel;
    private Integer nextLevelAt;
    private String description;
    private String icon;

    public Long getFactionHash() {
        return factionHash;
    }

    public void setFactionHash(Long factionHash) {
        this.factionHash = factionHash;
    }

    public Long getProgressionHash() {
        return progressionHash;
    }

    public void setProgressionHash(Long progressionHash) {
        this.progressionHash = progressionHash;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getLevelCap() {
        return levelCap;
    }

    public void setLevelCap(Integer levelCap) {
        this.levelCap = levelCap;
    }

    public Integer getProgressToNextLevel() {
        return progressToNextLevel;
    }

    public void setProgressToNextLevel(Integer progressToNextLevel) {
        this.progressToNextLevel = progressToNextLevel;
    }

    public Integer getNextLevelAt() {
        return nextLevelAt;
    }

    public void setNextLevelAt(Integer nextLevelAt) {
        this.nextLevelAt = nextLevelAt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
