package com.pendsley.clovisbray.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Phillip on 9/23/2017.
 */

public class Socket {

    private Long plugHash;
    @SerializedName("isEnabled")
    private Boolean enabled;
    private List<Long> reusablePlugHashes;

    private int columnIndex;
    private int rowIndex;

    public Long getPlugHash() {
        return plugHash;
    }

    public void setPlugHash(Long plugHash) {
        this.plugHash = plugHash;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public List<Long> getReusablePlugHashes() {
        return reusablePlugHashes;
    }

    public void setReusablePlugHashes(List<Long> reusablePlugHashes) {
        this.reusablePlugHashes = reusablePlugHashes;
    }

    public int getColumnIndex() {
        return columnIndex;
    }

    public void setColumnIndex(int columnIndex) {
        this.columnIndex = columnIndex;
    }

    public int getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }
}
