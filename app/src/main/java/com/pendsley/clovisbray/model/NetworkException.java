package com.pendsley.clovisbray.model;

/**
 * TODO
 */
public class NetworkException extends Exception {

    private final int errorMessageResource;

    public NetworkException(int errorMessageResource) {
        this.errorMessageResource = errorMessageResource;
    }

    public int getErrorMessageResource() {
        return errorMessageResource;
    }
}
