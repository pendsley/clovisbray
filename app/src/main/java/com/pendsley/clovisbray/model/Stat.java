package com.pendsley.clovisbray.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Stats for an item.
 *
 * @author Phil endsley
 */
public class Stat implements Parcelable {

    private Long statHash;
    private String name;
    private Integer value;
    private Integer minimum;
    private Integer maximumValue;

    public Long getStatHash() {
        return statHash;
    }

    public void setStatHash(Long statHash) {
        this.statHash = statHash;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getMinimum() {
        return minimum;
    }

    public void setMinimum(Integer minimum) {
        this.minimum = minimum;
    }

    public Integer getMaximumValue() {
        return maximumValue;
    }

    public void setMaximumValue(Integer maximumValue) {
        this.maximumValue = maximumValue;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.statHash);
        dest.writeString(this.name);
        dest.writeValue(this.value);
        dest.writeValue(this.minimum);
        dest.writeValue(this.maximumValue);
    }

    public Stat() {
    }

    protected Stat(Parcel in) {
        this.statHash = (Long) in.readValue(Long.class.getClassLoader());
        this.name = in.readString();
        this.value = (Integer) in.readValue(Integer.class.getClassLoader());
        this.minimum = (Integer) in.readValue(Integer.class.getClassLoader());
        this.maximumValue = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<Stat> CREATOR = new Parcelable.Creator<Stat>() {
        @Override
        public Stat createFromParcel(Parcel source) {
            return new Stat(source);
        }

        @Override
        public Stat[] newArray(int size) {
            return new Stat[size];
        }
    };
}
