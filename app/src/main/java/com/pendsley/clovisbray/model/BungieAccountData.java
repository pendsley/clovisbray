package com.pendsley.clovisbray.model;

import java.util.List;

/**
 * TODO
 */
public class BungieAccountData {

    private List<UserInfo> destinyMemberships;

    public List<UserInfo> getDestinyMemberships() {
        return destinyMemberships;
    }

    public void setDestinyMemberships(List<UserInfo> destinyMemberships) {
        this.destinyMemberships = destinyMemberships;
    }
}
