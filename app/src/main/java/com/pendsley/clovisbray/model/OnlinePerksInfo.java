package com.pendsley.clovisbray.model;

import java.util.List;

/**
 * TODO
 */
public class OnlinePerksInfo {

    List<OnlinePerk> perks;

    public List<OnlinePerk> getPerks() {
        return perks;
    }

    public void setPerks(List<OnlinePerk> perks) {
        this.perks = perks;
    }
}
