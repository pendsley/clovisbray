package com.pendsley.clovisbray.model;

/**
 * Request for transferring an item from the postmaster to a character.
 *
 * @author Phil Endsley
 */
public class PostmasterTransferRequest {

    private Long itemReferenceHash;
    private int stackSize = 1;
    private String itemId;
    private String characterId;
    private int membershipType;

    public Long getItemReferenceHash() {
        return itemReferenceHash;
    }

    public void setItemReferenceHash(Long itemReferenceHash) {
        this.itemReferenceHash = itemReferenceHash;
    }

    public int getStackSize() {
        return stackSize;
    }

    public void setStackSize(int stackSize) {
        this.stackSize = stackSize;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getCharacterId() {
        return characterId;
    }

    public void setCharacterId(String characterId) {
        this.characterId = characterId;
    }

    public int getMembershipType() {
        return membershipType;
    }

    public void setMembershipType(int membershipType) {
        this.membershipType = membershipType;
    }
}
