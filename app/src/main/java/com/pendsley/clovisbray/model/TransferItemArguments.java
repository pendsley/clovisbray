package com.pendsley.clovisbray.model;

import java.math.BigDecimal;

/**
 * TODO
 */
public class TransferItemArguments {

    private String characterId;
    private String itemId;
    private int membershipType;
    private int stackSize = 1;
    private BigDecimal itemReferenceHash;
    private boolean transferToVault;

    public TransferItemArguments(int membershipType,
                                 String itemId,
                                 String characterId,
                                 BigDecimal itemReferenceHash,
                                 boolean transferToVault) {
        this.membershipType = membershipType;
        this.itemId = itemId;
        this.characterId = characterId;
        this.itemReferenceHash = itemReferenceHash;
        this.transferToVault = transferToVault;
    }

    public int getMembershipType() {
        return membershipType;
    }

    public void setMembershipType(int membershipType) {
        this.membershipType = membershipType;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getCharacterId() {
        return characterId;
    }

    public void setCharacterId(String characterId) {
        this.characterId = characterId;
    }

    public boolean isTransferToVault() {
        return transferToVault;
    }

    public void setTransferToVault(boolean transferToVault) {
        this.transferToVault = transferToVault;
    }

    public int getStackSize() {
        return stackSize;
    }

    public void setStackSize(int stackSize) {
        this.stackSize = stackSize;
    }

    public BigDecimal getItemReferenceHash() {
        return itemReferenceHash;
    }

    public void setItemReferenceHash(BigDecimal itemReferenceHash) {
        this.itemReferenceHash = itemReferenceHash;
    }
}
