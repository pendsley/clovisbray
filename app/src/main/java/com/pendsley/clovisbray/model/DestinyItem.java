package com.pendsley.clovisbray.model;

/**
 * TODO
 */
public class DestinyItem {

    private Long itemHash;
    private String itemInstanceId;
    private Long bucketHash;
    private Integer state;

    public Long getItemHash() {
        return itemHash;
    }

    public void setItemHash(Long itemHash) {
        this.itemHash = itemHash;
    }

    public String getItemInstanceId() {
        return itemInstanceId;
    }

    public void setItemInstanceId(String itemInstanceId) {
        this.itemInstanceId = itemInstanceId;
    }

    public Long getBucketHash() {
        return bucketHash;
    }

    public void setBucketHash(Long bucketHash) {
        this.bucketHash = bucketHash;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
