package com.pendsley.clovisbray.model;

import java.util.List;

/**
 * TODO
 */
public class CharacterProfileResponse {

    private DestinyComponent<List<DestinyOnlineItem>> inventories;
    private DestinyComponent<ProgressionData> progressions;

    public DestinyComponent<ProgressionData> getProgressions() {
        return progressions;
    }

    public void setProgressions(DestinyComponent<ProgressionData> progressions) {
        this.progressions = progressions;
    }

    public DestinyComponent<List<DestinyOnlineItem>> getCharacterInventories() {
        return inventories;
    }

    public void setCharacterInventories(DestinyComponent<List<DestinyOnlineItem>> inventories) {
        this.inventories = inventories;
    }
}
