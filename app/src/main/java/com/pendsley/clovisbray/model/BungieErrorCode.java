package com.pendsley.clovisbray.model;

import com.google.common.collect.ImmutableMap;
import com.pendsley.clovisbray.R;

import java.util.Map;

/**
 * TODO
 */
public enum BungieErrorCode {

    SUCCESS(1, "", -1),
    DESTINY_ACCOUNT_NOT_FOUND(1601, "Destiny account not found", R.string.destiny_account_not_found),
    ITEM_NOT_FOUND(1623, "Could not find item", R.string.could_not_find_item),
    CANT_EQUIP_IN_COMBAT(1634, "Can't equip in combat", R.string.cannot_equip_in_combat),
    EXOTIC_ALREADY_EQUIPPED(1641, "Exotic already equipped", R.string.exotic_already_equipped),
    NO_ROOM_TO_TRANSFER(1642, "No free slots", R.string.no_room_to_transfer),
    OTHER(-1, "Something unexpected happened", R.string.something_unexpected_happened);

    static {
        ImmutableMap.Builder<Integer, BungieErrorCode> errorCodesMap = ImmutableMap.builder();
        for (BungieErrorCode errorCode : values()) {
            errorCodesMap.put(errorCode.errorCode, errorCode);
        }
        ERROR_CODES_MAP = errorCodesMap.build();
    }

    private static final Map<Integer, BungieErrorCode> ERROR_CODES_MAP;

    private final int errorCode;
    private final String errorMessage;
    private final int errorMessageResource;

    BungieErrorCode(int errorCode,
                    String errorMessage,
                    int errorMessageResource) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.errorMessageResource = errorMessageResource;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public int getErrorMessageResource() {
        return errorMessageResource;
    }

    public static BungieErrorCode forCode(int errorCode) {
        BungieErrorCode bungieErrorCode = ERROR_CODES_MAP.get(errorCode);
        if (bungieErrorCode == null) {
            return OTHER;
        }
        return bungieErrorCode;
    }
}
