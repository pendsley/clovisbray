package com.pendsley.clovisbray.model;

/**
 * Created by Phillip on 9/16/2017.
 */

public class ManifestBucketDefinition {

    DisplayProperties displayProperties;

    public DisplayProperties getDisplayProperties() {
        return displayProperties;
    }

    public void setDisplayProperties(DisplayProperties displayProperties) {
        this.displayProperties = displayProperties;
    }
}
