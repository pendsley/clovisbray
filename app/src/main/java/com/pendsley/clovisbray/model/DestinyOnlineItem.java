package com.pendsley.clovisbray.model;

/**
 * TODO
 */
public class DestinyOnlineItem {

    private Long itemHash;
    private String itemInstanceId;
    private Integer quantity;
    private Integer bindStatus;
    private Long bucketHash;
    private Integer transferStatus;
    private Object location;

    private ItemClassType itemClassType;

    public Long getItemHash() {
        return itemHash;
    }

    public void setItemHash(Long itemHash) {
        this.itemHash = itemHash;
    }

    public String getItemInstanceId() {
        return itemInstanceId;
    }

    public void setItemInstanceId(String itemInstanceId) {
        this.itemInstanceId = itemInstanceId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getBindStatus() {
        return bindStatus;
    }

    public void setBindStatus(Integer bindStatus) {
        this.bindStatus = bindStatus;
    }

    public Long getBucketHash() {
        return bucketHash;
    }

    public void setBucketHash(Long bucketHash) {
        this.bucketHash = bucketHash;
    }

    public Integer getTransferStatus() {
        return transferStatus;
    }

    public void setTransferStatus(Integer transferStatus) {
        this.transferStatus = transferStatus;
    }

    public ItemClassType getItemClassType() {
        return itemClassType;
    }

    public void setItemClassType(ItemClassType itemClassType) {
        this.itemClassType = itemClassType;
    }

    public Object getLocation() {
        return location;
    }

    public void setLocation(Object location) {
        this.location = location;
    }
}
