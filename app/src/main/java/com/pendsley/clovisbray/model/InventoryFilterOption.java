package com.pendsley.clovisbray.model;

/**
 * Data for displaying the filter options.
 *
 * @author Phil Endsley
 */
public class InventoryFilterOption {

    private String description;
    private String emblemBackgroundPath;
    private String characterId;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmblemBackgroundPath() {
        return emblemBackgroundPath;
    }

    public void setEmblemBackgroundPath(String emblemBackgroundPath) {
        this.emblemBackgroundPath = emblemBackgroundPath;
    }

    public String getCharacterId() {
        return characterId;
    }

    public void setCharacterId(String characterId) {
        this.characterId = characterId;
    }
}
