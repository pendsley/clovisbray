package com.pendsley.clovisbray.model;

import java.util.List;
import java.util.Map;

/**
 * TODO
 */
public class ProfileResponse {

    private DestinyComponent<Map<String, Character>> characters;
    private DestinyComponent<DestinyProfileComponent> profile;
    private DestinyComponent<Map<String, DestinyInventoryComponent>> characterInventories;
    private DestinyComponent<Map<String, DestinyInventoryComponent>> characterEquipment;
    private DestinyComponent<DestinyInventoryComponent> profileInventory;

    public DestinyComponent<Map<String, Character>> getCharacters() {
        return characters;
    }

    public void setCharacters(DestinyComponent<Map<String, Character>> characters) {
        this.characters = characters;
    }

    public DestinyComponent<DestinyProfileComponent> getProfile() {
        return profile;
    }

    public void setProfile(DestinyComponent<DestinyProfileComponent> profile) {
        this.profile = profile;
    }

    public DestinyComponent<Map<String, DestinyInventoryComponent>> getCharacterInventories() {
        return characterInventories;
    }

    public void setCharacterInventories(DestinyComponent<Map<String, DestinyInventoryComponent>> characterInventories) {
        this.characterInventories = characterInventories;
    }

    public DestinyComponent<Map<String, DestinyInventoryComponent>> getCharacterEquipment() {
        return characterEquipment;
    }

    public void setCharacterEquipment(DestinyComponent<Map<String, DestinyInventoryComponent>> characterEquipment) {
        this.characterEquipment = characterEquipment;
    }

    public DestinyComponent<DestinyInventoryComponent> getProfileInventory() {
        return profileInventory;
    }

    public void setProfileInventory(DestinyComponent<DestinyInventoryComponent> profileInventory) {
        this.profileInventory = profileInventory;
    }
}
