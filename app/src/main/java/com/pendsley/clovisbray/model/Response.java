package com.pendsley.clovisbray.model;

/**
 * TODO
 * @param <T>
 */
public abstract class Response<T extends ResponseData<?>> {

    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
