package com.pendsley.clovisbray.model;

import com.google.gson.annotations.SerializedName;

/**
 * TODO
 */
public class DestinyItemInstance {

    private Stat primaryStat;
    @SerializedName("isEquipped")
    private Boolean equipped;
    private Long damageTypeHash;

    public Stat getPrimaryStat() {
        return primaryStat;
    }

    public void setPrimaryStat(Stat primaryStat) {
        this.primaryStat = primaryStat;
    }

    public Boolean isEquipped() {
        return equipped;
    }

    public void setEquipped(Boolean equipped) {
        this.equipped = equipped;
    }

    public Long getDamageTypeHash() {
        return damageTypeHash;
    }

    public void setDamageTypeHash(Long damageTypeHash) {
        this.damageTypeHash = damageTypeHash;
    }
}
