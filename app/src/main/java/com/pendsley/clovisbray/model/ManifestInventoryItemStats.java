package com.pendsley.clovisbray.model;

import java.util.Map;

/**
 * TODO
 */
public class ManifestInventoryItemStats {

    private Map<String, Stat> stats;

    public Map<String, Stat> getStats() {
        return stats;
    }

    public void setStats(Map<String, Stat> stats) {
        this.stats = stats;
    }
}
