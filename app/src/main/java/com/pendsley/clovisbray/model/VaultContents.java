package com.pendsley.clovisbray.model;

import java.util.List;

/**
 * Created by Phillip on 1/29/2017.
 */
public class VaultContents {

    private List<OnlineItem> items;

    public List<OnlineItem> getItems() {
        return items;
    }

    public void setItems(List<OnlineItem> items) {
        this.items = items;
    }
}
