package com.pendsley.clovisbray.model;

/**
 * TODO
 */
public class BungieResponse<T> {

    protected T Response;
    private Integer ErrorCode;
    private Integer ThrottleSeconds;
    private String ErrorStatus;
    private String Message;
    private Object MessageData;

    public T getResponse() {
        return Response;
    }

    public void setResponse(T response) {
        this.Response = response;
    }

    public Integer getErrorCode() {
        return ErrorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.ErrorCode = errorCode;
    }

    public Integer getThrottleSeconds() {
        return ThrottleSeconds;
    }

    public void setThrottleSeconds(Integer throttleSeconds) {
        this.ThrottleSeconds = throttleSeconds;
    }

    public String getErrorStatus() {
        return ErrorStatus;
    }

    public void setErrorStatus(String errorStatus) {
        this.ErrorStatus = errorStatus;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        this.Message = message;
    }

    public Object getMessageData() {
        return MessageData;
    }

    public void setMessageData(Object messageData) {
        this.MessageData = messageData;
    }
}
