package com.pendsley.clovisbray.model;

/**
 * TODO
 */
public class DrawerItem {

    private String screenName;

    public DrawerItem(String screenName) {
        this.screenName = screenName;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }
}
