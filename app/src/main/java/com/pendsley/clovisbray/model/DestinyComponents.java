package com.pendsley.clovisbray.model;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * TODO
 */
public enum DestinyComponents {

    CHARACTER_EQUIPMENT("CharacterEquipment"),
    CHARACTER_INVENTORIES("CharacterInventories"),
    CHARACTER_PROGRESSIONS("CharacterProgressions"),
    CHARACTERS("Characters"),
    ITEM_COMMON_DATA("ItemCommonData"),
    ITEM_INSTANCES("ItemInstances"),
    ITEM_PERKS("ItemPerks"),
    ITEM_PLUG_STATES("ItemPlugStates"),
    ITEM_SOCKETS("ItemSockets"),
    ITEM_STATS("ItemStats"),
    ITEM_TALENT_GRIDS("ItemTalentGrids"),
    PROFILE_INVENTORIES("ProfileInventories");


    private final String description;

    DestinyComponents(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public static String toParameter(DestinyComponents ... components) {

        List<String> componentDescriptions = Lists.transform(Lists.newArrayList(components),
                                                             DestinyComponents::getDescription);

        return Joiner.on(",").join(componentDescriptions);
    }
}
