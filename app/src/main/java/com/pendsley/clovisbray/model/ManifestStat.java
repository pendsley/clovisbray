package com.pendsley.clovisbray.model;

/**
 * Stats data from the manifest.
 *
 * @author Phil Endsley
 */
public class ManifestStat {

    private DisplayProperties displayProperties;

    public DisplayProperties getDisplayProperties() {
        return displayProperties;
    }

    public void setDisplayProperties(DisplayProperties displayProperties) {
        this.displayProperties = displayProperties;
    }
}
