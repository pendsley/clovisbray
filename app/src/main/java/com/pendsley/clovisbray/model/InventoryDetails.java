package com.pendsley.clovisbray.model;

/**
 * Created by Phillip on 9/16/2017.
 */

public class InventoryDetails {

    private Long bucketTypeHash;

    public Long getBucketTypeHash() {
        return bucketTypeHash;
    }

    public void setBucketTypeHash(Long bucketTypeHash) {
        this.bucketTypeHash = bucketTypeHash;
    }
}
