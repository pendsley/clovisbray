package com.pendsley.clovisbray.model;

import java.util.List;

/**
 * The most essential summary information about a Profile.
 *
 * @author Phil Endsley
 */
public class DestinyProfileComponent {

    private UserInfo userInfo;
    private List<String> characterIds;

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public List<String> getCharacterIds() {
        return characterIds;
    }

    public void setCharacterIds(List<String> characterIds) {
        this.characterIds = characterIds;
    }
}
