package com.pendsley.clovisbray.model;

/**
 * Created by Phillip on 1/28/2017.
 */
public class AllItemsSummaryWrapper {

    private AllItemsSummary data;

    public AllItemsSummary getData() {
        return data;
    }

    public void setData(AllItemsSummary data) {
        this.data = data;
    }
}
