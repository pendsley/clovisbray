package com.pendsley.clovisbray.model;

/**
 * TODO
 */
public class ItemDetailTalentNode {

    private Integer nodeHash;
    private Integer stepIndex;
    private Boolean hidden;

    public Integer getNodeHash() {
        return nodeHash;
    }

    public void setNodeHash(Integer nodeHash) {
        this.nodeHash = nodeHash;
    }

    public Integer getStepIndex() {
        return stepIndex;
    }

    public void setStepIndex(Integer stepIndex) {
        this.stepIndex = stepIndex;
    }

    public Boolean isHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }
}
