package com.pendsley.clovisbray.model;

/**
 * TODO
 */
public enum ItemClassType {

    WEAPONS,
    ARMOR,
    ALL;

    public static ItemClassType fromString(String itemTypeString) {
        for (ItemClassType itemClassType : values()) {
            if (itemClassType.name().equalsIgnoreCase(itemTypeString.toLowerCase())) {
                return itemClassType;
            }
        }

        return ALL;
    }

}
