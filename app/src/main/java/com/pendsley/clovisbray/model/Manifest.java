package com.pendsley.clovisbray.model;

/**
 * TODO
 */
public class Manifest {

    private String version;

    private MobileWorldContentPaths mobileWorldContentPaths;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public MobileWorldContentPaths getMobileWorldContentPaths() {
        return mobileWorldContentPaths;
    }

    public void setMobileWorldContentPaths(MobileWorldContentPaths mobileWorldContentPaths) {
        this.mobileWorldContentPaths = mobileWorldContentPaths;
    }
}
