package com.pendsley.clovisbray.model;

/**
 * TODO
 */
public class CharacterLoadoutsDrawerItem extends DrawerItem {

    private Character character;

    public CharacterLoadoutsDrawerItem(Character character) {
        super("Loadouts");

        this.character = character;
    }

    public Character getCharacter() {
        return character;
    }

    public void setCharacter(Character character) {
        this.character = character;
    }
}
