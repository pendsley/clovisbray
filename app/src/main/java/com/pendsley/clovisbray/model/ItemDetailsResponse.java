package com.pendsley.clovisbray.model;

import java.util.List;

/**
 * TODO
 */
public class ItemDetailsResponse {

    private ItemDetails item;
    private List<ItemDetailTalentNode> talentNodes;

    public ItemDetails getItem() {
        return item;
    }

    public void setItem(ItemDetails item) {
        this.item = item;
    }

    public List<ItemDetailTalentNode> getTalentNodes() {
        return talentNodes;
    }

    public void setTalentNodes(List<ItemDetailTalentNode> talentNodes) {
        this.talentNodes = talentNodes;
    }
}
