package com.pendsley.clovisbray.model;

import java.util.List;

/**
 * TODO
 */
public class Item {

    private Long itemHash;
    private String itemName;
    private String itemDescription;
    private String itemTypeName;
    private Integer itemType;
    private List<Integer> itemCategoryHashes;
    private Long bucketTypeHash;
    private Long primaryBaseStatHash;
    private List<Long> perkHashes;
    private Long talentGridHash;
    private String itemInstanceId;
    private Integer light;
    private Integer characterIndex;
    private ItemClassType itemClass;
    private Boolean isEquipped;

    public Long getItemHash() {
        return itemHash;
    }

    public void setItemHash(Long itemHash) {
        this.itemHash = itemHash;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getItemTypeName() {
        return itemTypeName;
    }

    public void setItemTypeName(String itemTypeName) {
        this.itemTypeName = itemTypeName;
    }

    public Integer getItemType() {
        return itemType;
    }

    public List<Integer> getItemCategoryHashes() {
        return itemCategoryHashes;
    }

    public void setItemCategoryHashes(List<Integer> itemCategoryHashes) {
        this.itemCategoryHashes = itemCategoryHashes;
    }

    public void setItemType(Integer itemType) {
        this.itemType = itemType;
    }

    public Long getBucketTypeHash() {
        return bucketTypeHash;
    }

    public void setBucketTypeHash(Long bucketTypeHash) {
        this.bucketTypeHash = bucketTypeHash;
    }

    public Long getPrimaryBaseStatHash() {
        return primaryBaseStatHash;
    }

    public void setPrimaryBaseStatHash(Long primaryBaseStatHash) {
        this.primaryBaseStatHash = primaryBaseStatHash;
    }

    public List<Long> getPerkHashes() {
        return perkHashes;
    }

    public void setPerkHashes(List<Long> perkHashes) {
        this.perkHashes = perkHashes;
    }

    public Long getTalentGridHash() {
        return talentGridHash;
    }

    public void setTalentGridHash(Long talentGridHash) {
        this.talentGridHash = talentGridHash;
    }

    public String getItemInstanceId() {
        return itemInstanceId;
    }

    public void setItemInstanceId(String itemInstanceId) {
        this.itemInstanceId = itemInstanceId;
    }

    public Integer getLight() {
        return light;
    }

    public void setLight(Integer light) {
        this.light = light;
    }

    public Integer getCharacterIndex() {
        return characterIndex;
    }

    public void setCharacterIndex(Integer characterIndex) {
        this.characterIndex = characterIndex;
    }

    public ItemClassType getItemClass() {
        return itemClass;
    }

    public void setItemClass(ItemClassType itemClass) {
        this.itemClass = itemClass;
    }

    public Boolean getEquipped() {
        return isEquipped;
    }

    public void setEquipped(Boolean equipped) {
        isEquipped = equipped;
    }
}
