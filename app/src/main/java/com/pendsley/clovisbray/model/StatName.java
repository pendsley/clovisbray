package com.pendsley.clovisbray.model;

/**
 * The different stats for items.
 *
 * @author Phil Endsley
 */
@SuppressWarnings("unused") // Most of these are used in the forName method
public enum StatName {

    IMPACT("Impact", 1, false),
    HANDLING("Handling", 2, false),
    RELOAD_SPEED("Reload Speed", 3, false),
    STABILITY("Stability", 4, false),
    RANGE("Range", 5, false),
    ROUNDS_PER_MINUTE("Rounds Per Minute", 6, true),
    SWING_SPEED("Swing Speed", 7, true),
    CHARGE_TIME("Charge Time", 8, true),
    BLAST_RADIUS("Blast Radius", 9, false),
    VELOCITY("Velocity", 10, false),
    SPEED("Speed", 11, false),
    EFFICIENCY("Efficiency", 12, false),
    DEFENSE("Defense", 13, false),
    ENERGY("Energy", 14, true),
    MAGAZINE("Magazine", 15, true),
    AMMO_CAPACITY("Ammo Capacity", 16, true),
    INTELLECT("Intellect", 17, true),
    DISCIPLINE("Discipline", 18, true),
    STRENGTH("Strength", 19, true),
    UNKNOWN("Unknown", 20, true);
    
    private final String description;
    private final int sortOrder;
    private final boolean textDisplay;
    
    StatName(String description,
             int sortOrder,
             boolean textDisplay) {
        this.description = description;
        this.sortOrder = sortOrder;
        this.textDisplay = textDisplay;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public String getDescription() {
        return description;
    }

    public boolean isTextDisplay() {
        return textDisplay;
    }

    public static StatName forName(String name) {
        for (StatName statName : values()) {
            if (name.equals(statName.getDescription())) {
                return statName;
            }
        }
        return UNKNOWN;
    }
}
