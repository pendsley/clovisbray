package com.pendsley.clovisbray.model;

/**
 * TODO
 */
public class DamageType {

    private DisplayProperties displayProperties;
    private Boolean showIcon;
    private String transparentIconPath;

    public DisplayProperties getDisplayProperties() {
        return displayProperties;
    }

    public void setDisplayProperties(DisplayProperties displayProperties) {
        this.displayProperties = displayProperties;
    }

    public Boolean showIcon() {
        return showIcon;
    }

    public void setShowIcon(Boolean showIcon) {
        this.showIcon = showIcon;
    }

    public String getTransparentIconPath() {
        return transparentIconPath;
    }

    public void setTransparentIconPath(String transparentIconPath) {
        this.transparentIconPath = transparentIconPath;
    }
}
