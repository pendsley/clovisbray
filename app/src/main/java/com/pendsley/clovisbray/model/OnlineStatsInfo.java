package com.pendsley.clovisbray.model;

import java.util.Map;

/**
 * TODO
 */
public class OnlineStatsInfo {

    private Map<Long, Stat> stats;

    public Map<Long, Stat> getStats() {
        return stats;
    }

    public void setStats(Map<Long, Stat> stats) {
        this.stats = stats;
    }
}
