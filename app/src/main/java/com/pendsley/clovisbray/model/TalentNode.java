package com.pendsley.clovisbray.model;

import java.util.List;

/**
 * TODO
 */
public class TalentNode {

    private Integer nodeHash;
    private Integer nodeIndex;
    private Integer row;
    private Integer column;
    private List<TalentNodeStep> steps;
    private List<Integer> prerequisiteNodeIndexes;

    public Integer getNodeHash() {
        return nodeHash;
    }

    public void setNodeHash(Integer nodeHash) {
        this.nodeHash = nodeHash;
    }

    public Integer getNodeIndex() {
        return nodeIndex;
    }

    public void setNodeIndex(Integer nodeIndex) {
        this.nodeIndex = nodeIndex;
    }

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    public Integer getColumn() {
        return column;
    }

    public void setColumn(Integer column) {
        this.column = column;
    }

    public List<TalentNodeStep> getSteps() {
        return steps;
    }

    public void setSteps(List<TalentNodeStep> steps) {
        this.steps = steps;
    }

    public List<Integer> getPrerequisiteNodeIndexes() {
        return prerequisiteNodeIndexes;
    }

    public void setPrerequisiteNodeIndexes(List<Integer> prerequisiteNodeIndexes) {
        this.prerequisiteNodeIndexes = prerequisiteNodeIndexes;
    }
}
