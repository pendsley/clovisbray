package com.pendsley.clovisbray.model;

/**
 * TODO
 */
public class EquipItemArguments {

    private int membershipType;
    private String itemId;
    private String characterId;

    public EquipItemArguments(int membershipType,
                              String itemId,
                              String characterId) {
        this.membershipType = membershipType;
        this.itemId = itemId;
        this.characterId = characterId;
    }

    public int getMembershipType() {
        return membershipType;
    }

    public void setMembershipType(int membershipType) {
        this.membershipType = membershipType;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getCharacterId() {
        return characterId;
    }

    public void setCharacterId(String characterId) {
        this.characterId = characterId;
    }
}
