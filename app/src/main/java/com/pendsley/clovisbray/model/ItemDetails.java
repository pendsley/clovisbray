package com.pendsley.clovisbray.model;

import java.util.List;

/**
 * Item details from the Bungie's service.
 *
 * @author Phil Endsley
 */
public class ItemDetails {

    private String talentGridHash;
    private Stat primaryStat;
    private Boolean isEquipped;
    private List<Stat> stats;

    public String getTalentGridHash() {
        return talentGridHash;
    }

    public void setTalentGridHash(String talentGridHash) {
        this.talentGridHash = talentGridHash;
    }

    public Stat getPrimaryStat() {
        return primaryStat;
    }

    public void setPrimaryStat(Stat primaryStat) {
        this.primaryStat = primaryStat;
    }

    public Boolean getEquipped() {
        return isEquipped;
    }

    public void setEquipped(Boolean equipped) {
        isEquipped = equipped;
    }

    public List<Stat> getStats() {
        return stats;
    }

    public void setStats(List<Stat> stats) {
        this.stats = stats;
    }
}
