package com.pendsley.clovisbray.model;

import android.support.annotation.Nullable;

/**
 * The different item slots.
 *
 * @author Phil Endsley
 */
public enum ItemSlot {

    KINETIC("Kinetic"),
    ENERGY("Energy"),
    POWER("Power"),
    HELMET("Helmet"),
    GAUNTLETS("Gauntlets"),
    CHEST("Chest"),
    LEGS("Legs"),
    CLASS("Class Item"),
    GHOST("Ghost");

    private final String description;

    ItemSlot(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Nullable
    public static ItemSlot fromDescription(String description) {
        for (ItemSlot itemSlot : values()) {
            if (itemSlot.getDescription().equalsIgnoreCase(description)) {
                return itemSlot;
            }
        }

        return null;
    }
}
