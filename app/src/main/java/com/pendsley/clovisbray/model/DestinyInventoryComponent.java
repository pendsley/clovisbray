package com.pendsley.clovisbray.model;

import java.util.List;

/**
 * TODO
 */
public class DestinyInventoryComponent {

    private List<DestinyOnlineItem> items;

    public List<DestinyOnlineItem> getItems() {
        return items;
    }

    public void setItems(List<DestinyOnlineItem> items) {
        this.items = items;
    }
}
