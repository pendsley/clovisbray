package com.pendsley.clovisbray.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Details for a talent node.
 *
 * @author Phil Endsley
 */
public class TalentNodeStep implements Parcelable {

    private String nodeStepName;
    private String nodeStepDescription;
    private String icon;
    private Integer stepIndex;
    private Integer row;
    private Integer column;

    public String getNodeStepName() {
        return nodeStepName;
    }

    public void setNodeStepName(String nodeStepName) {
        this.nodeStepName = nodeStepName;
    }

    public String getNodeStepDescription() {
        return nodeStepDescription;
    }

    public void setNodeStepDescription(String nodeStepDescription) {
        this.nodeStepDescription = nodeStepDescription;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getStepIndex() {
        return stepIndex;
    }

    public void setStepIndex(Integer stepIndex) {
        this.stepIndex = stepIndex;
    }

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    public Integer getColumn() {
        return column;
    }

    public void setColumn(Integer column) {
        this.column = column;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.nodeStepName);
        dest.writeString(this.nodeStepDescription);
        dest.writeString(this.icon);
        dest.writeValue(this.stepIndex);
        dest.writeValue(this.row);
        dest.writeValue(this.column);
    }

    public TalentNodeStep() {
    }

    protected TalentNodeStep(Parcel in) {
        this.nodeStepName = in.readString();
        this.nodeStepDescription = in.readString();
        this.icon = in.readString();
        this.stepIndex = (Integer) in.readValue(Integer.class.getClassLoader());
        this.row = (Integer) in.readValue(Integer.class.getClassLoader());
        this.column = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<TalentNodeStep> CREATOR = new Parcelable.Creator<TalentNodeStep>() {
        @Override
        public TalentNodeStep createFromParcel(Parcel source) {
            return new TalentNodeStep(source);
        }

        @Override
        public TalentNodeStep[] newArray(int size) {
            return new TalentNodeStep[size];
        }
    };
}
