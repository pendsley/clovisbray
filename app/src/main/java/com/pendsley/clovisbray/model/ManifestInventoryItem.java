package com.pendsley.clovisbray.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Phillip on 9/11/2017.
 */

public class ManifestInventoryItem {

    private DisplayProperties displayProperties;
    private String itemTypeDisplayName;
    private String itemTypeAndTierDisplayName;
    @SerializedName("stats")
    private ManifestInventoryItemStats statInfo; // TODO
    private Long hash;
    private List<Integer> damageTypes;
    private InventoryDetails inventory;

    ////////////////////////////////////////////////////
    // Convenience fields - Not actually in the manifest
    ////////////////////////////////////////////////////
    private String itemInstanceId;
    private Integer lightLevel;
    private String characterId;
    private Boolean equipped;
    private String itemCategory;
    private String damageType;
    private String damageTypeIcon;
    private ItemClassType itemClassType;
    private Integer quantity = 1;
    private boolean isMasterWorks;

    public DisplayProperties getDisplayProperties() {
        return displayProperties;
    }

    public void setDisplayProperties(DisplayProperties displayProperties) {
        this.displayProperties = displayProperties;
    }

    public String getItemTypeDisplayName() {
        return itemTypeDisplayName;
    }

    public void setItemTypeDisplayName(String itemTypeDisplayName) {
        this.itemTypeDisplayName = itemTypeDisplayName;
    }

    public String getItemTypeAndTierDisplayName() {
        return itemTypeAndTierDisplayName;
    }

    public void setItemTypeAndTierDisplayName(String itemTypeAndTierDisplayName) {
        this.itemTypeAndTierDisplayName = itemTypeAndTierDisplayName;
    }

    public ManifestInventoryItemStats getStatInfo() {
        return statInfo;
    }

    public void setStatInfo(ManifestInventoryItemStats statInfo) {
        this.statInfo = statInfo;
    }

    public Long getHash() {
        return hash;
    }

    public void setHash(Long hash) {
        this.hash = hash;
    }

    public List<Integer> getDamageTypes() {
        return damageTypes;
    }

    public void setDamageTypes(List<Integer> damageTypes) {
        this.damageTypes = damageTypes;
    }

    public InventoryDetails getInventory() {
        return inventory;
    }

    public void setInventory(InventoryDetails inventory) {
        this.inventory = inventory;
    }

    public String getItemInstanceId() {
        return itemInstanceId;
    }

    public void setItemInstanceId(String itemInstanceId) {
        this.itemInstanceId = itemInstanceId;
    }

    public Integer getLightLevel() {
        return lightLevel;
    }

    public void setLightLevel(Integer lightLevel) {
        this.lightLevel = lightLevel;
    }

    public String getCharacterId() {
        return characterId;
    }

    public void setCharacterId(String characterId) {
        this.characterId = characterId;
    }

    public Boolean isEquipped() {
        return equipped;
    }

    public void setEquipped(Boolean equipped) {
        this.equipped = equipped;
    }

    public String getItemCategory() {
        return itemCategory;
    }

    public void setItemCategory(String itemCategory) {
        this.itemCategory = itemCategory;
    }

    public String getDamageType() {
        return damageType;
    }

    public void setDamageType(String damageType) {
        this.damageType = damageType;
    }

    public String getDamageTypeIcon() {
        return damageTypeIcon;
    }

    public void setDamageTypeIcon(String damageTypeIcon) {
        this.damageTypeIcon = damageTypeIcon;
    }

    public ItemClassType getItemClassType() {
        return itemClassType;
    }

    public void setItemClassType(ItemClassType itemClassType) {
        this.itemClassType = itemClassType;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public boolean isMasterWorks() {
        return isMasterWorks;
    }

    public void setMasterWorks(boolean masterWorks) {
        isMasterWorks = masterWorks;
    }
}
