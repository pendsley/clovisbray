package com.pendsley.clovisbray.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.pendsley.clovisbray.R;

/**
 * TODO
 */
public class CornerImageView extends ImageView {

    private boolean showCorner = false;
    private Paint paint = new Paint();
    private Path path = new Path();

    public CornerImageView(Context context) {
        super(context);
    }

    public CornerImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CornerImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CornerImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (showCorner) {
            paint.setColor(getContext().getResources().getColor(R.color.destiny_yellow));
            paint.setStyle(Paint.Style.FILL);

            path.moveTo(0, 0);
            path.lineTo(0, (getHeight() / 4));
            path.lineTo((getWidth() / 4), 0);
            path.lineTo(0, 0);
            path.close();

            canvas.drawPath(path, paint);
        }
    }

    public void setShowCorner(boolean showCorner) {
        this.showCorner = showCorner;
    }

}
