package com.pendsley.clovisbray.widget;

import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;

import com.gordonwong.materialsheetfab.MaterialSheetFab;

/**
 * TODO
 */
public class DownRevealMaterialSheetFab extends MaterialSheetFab {
    /**
     * Creates a MaterialSheetFab instance and sets up the necessary click listeners.
     *
     * @param view       The FAB view.
     * @param sheet      The sheet view.
     * @param overlay    The overlay view.
     * @param sheetColor The background color of the material sheet.
     * @param fabColor   The background color of the FAB.
     */
    public DownRevealMaterialSheetFab(View view, View sheet, View overlay, int sheetColor, int fabColor) {

        super(view, sheet, overlay, sheetColor, fabColor);

        Interpolator interpolator = AnimationUtils.loadInterpolator(sheet.getContext(),
                com.gordonwong.materialsheetfab.R.interpolator.msf_interpolator);

        sheetAnimation = new DownRevealMaterialSheetAnimation(sheet, sheetColor, fabColor, interpolator);
    }
}
