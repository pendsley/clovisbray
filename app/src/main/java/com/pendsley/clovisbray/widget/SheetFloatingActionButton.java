package com.pendsley.clovisbray.widget;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;

import com.gordonwong.materialsheetfab.AnimatedFab;

/**
 * {@link FloatingActionButton} which, when tapped, will display an options sheet. All interactions
 * are handled by {@link com.gordonwong.materialsheetfab.MaterialSheetFab}
 *
 * @see com.gordonwong.materialsheetfab.MaterialSheetFab
 * @author Phil Endsley
 */
public class SheetFloatingActionButton extends FloatingActionButton implements AnimatedFab {

    public SheetFloatingActionButton(Context context) {
        super(context);
    }

    public SheetFloatingActionButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SheetFloatingActionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void show(float translationX, float translationY) {
        show();
    }

}
