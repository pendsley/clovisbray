package com.pendsley.clovisbray;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.pendsley.clovisbray.authentication.ReinjectFieldsEvent;
import com.pendsley.clovisbray.inject.AccountModule;
import com.pendsley.clovisbray.model.Character;
import com.pendsley.clovisbray.network.BungieDestiny2Service;
import com.pendsley.clovisbray.util.BusProvider;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Activity for viewing and retrieving items from the poastmaster.
 *
 * @author Phil Endsley
 */
public class PostmasterActivity extends AppCompatActivity {

    @Inject
    BusProvider busProvider;

    @Inject
    @Named(AccountModule.CHARACTERS)
    List<Character> characters;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        injectMembers();

        setContentView(R.layout.activity_postmaster);
        setTitle(R.string.postmaster);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
    }

    @Override
    protected void onResume() {
        super.onResume();

        busProvider.getBus().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        busProvider.getBus().unregister(this);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        Spinner characterSpinner = (Spinner) findViewById(R.id.character_spinner);
        CharacterSpinnerAdapter adapter = new CharacterSpinnerAdapter(this, characters);
        characterSpinner.setAdapter(adapter);
        characterSpinner.setSelection(0);
        adapter.notifyDataSetChanged();


        getSupportFragmentManager().beginTransaction()
                .add(R.id.postmaster_container, new PostmasterFragment(), PostmasterFragment.TAG)
                .commit();
    }

    @Subscribe
    public void onRefreshToken(ReinjectFieldsEvent event) {
        injectMembers();
    }

    private void injectMembers() {
        ((ClovisBrayApplication) getApplication()).getApiComponent().inject(this);
    }

    private class CharacterSpinnerAdapter extends ArrayAdapter<Character> {

        private final List<Character> characters;

        CharacterSpinnerAdapter(@NonNull Context context,
                                List<Character> characters) {
            super(context, R.layout.spinner_character, characters);

            this.characters = characters;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            Context context = getContext();
            PostmasterActivity.CharacterViewHolder characterViewHolder;

            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(R.layout.spinner_character, parent, false);
                characterViewHolder = new PostmasterActivity.CharacterViewHolder(convertView);

                convertView.setTag(characterViewHolder);
            } else {
                characterViewHolder = (PostmasterActivity.CharacterViewHolder) convertView.getTag();
            }

            Character character = characters.get(position);
            String url = BungieDestiny2Service.BASE_URL + character.getBackgroundPath();

            Picasso.with(context).load(url).into(characterViewHolder.emblemView);

            return convertView;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            Context context = getContext();
            PostmasterActivity.CharacterViewHolder characterViewHolder;

            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(R.layout.spinner_character, parent, false);
                characterViewHolder = new PostmasterActivity.CharacterViewHolder(convertView);

                convertView.setTag(characterViewHolder);
            } else {
                characterViewHolder = (PostmasterActivity.CharacterViewHolder) convertView.getTag();
            }

            Character character = characters.get(position);
            String url = BungieDestiny2Service.BASE_URL + character.getEmblemPath();

            Picasso.with(context).load(url).into(characterViewHolder.emblemView);

            return convertView;
        }

    }

    private static class CharacterViewHolder {

        ImageView emblemView;

        CharacterViewHolder(View root) {
            emblemView = (ImageView) root.findViewById(R.id.character_emblem);
        }
    }


}
