package com.pendsley.clovisbray;

import android.util.Log;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.pendsley.clovisbray.model.BungieErrorCode;
import com.pendsley.clovisbray.model.BungieResponse;
import com.pendsley.clovisbray.model.UserInfo;
import com.pendsley.clovisbray.model.UserMemberships;
import com.pendsley.clovisbray.network.BungieUserService;
import com.pendsley.clovisbray.persistence.dao.AccountOperation;

import java.io.IOException;
import java.util.concurrent.Executors;

import javax.inject.Inject;

import retrofit2.Response;

/**
 * Controller for managing Bungie account information.
 * TODO - Remove this
 *
 * @author Phil Endsley
 */
public class AccountInfoController {

    private static final String TAG = "AccountInfoController";

    private final BungieUserService userService;
    private final AccountOperation accountOperation;

    private final ListeningExecutorService listeningExecutorService;

    @Inject
    AccountInfoController(BungieUserService userService,
                          AccountOperation accountOperation) {
        this.userService = userService;
        this.accountOperation = accountOperation;

        listeningExecutorService = MoreExecutors.listeningDecorator(Executors.newSingleThreadExecutor());
    }

    public ListenableFuture retrieveCharacterInformation() {

        return listeningExecutorService.submit(
                new Runnable() {
                    @Override
                    public void run() {

                        try {
                            Response<BungieResponse<UserMemberships>> response = userService.getMembershipData().execute();

                            BungieResponse<UserMemberships> bungieResponse = response.body();


                            BungieErrorCode errorCode = BungieErrorCode.forCode(bungieResponse.getErrorCode());
                            if (BungieErrorCode.SUCCESS == errorCode) {

                                handleCharacterInformation(bungieResponse.getResponse());
                            } else {

                                Log.e(TAG, "Error: " + errorCode.name());

                            }

                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }

                    }
                }
        );
    }

    private void handleCharacterInformation(UserMemberships userMemberships) {

//        DestinyAccount destinyAccount = accountData.getDestinyMemberships().get(0);
//        UserInfo userInfo = userMemberships.getDestinyMemberships().get(0);
//
//        String membershipId = userInfo.getMembershipId();
//
//        accountOperation.deleteUserInfo();
//        accountOperation.deleteCharacterInfo();
//
//        accountOperation.saveUserInfo(userInfo);
//        accountOperation.saveCharacterInfo(membershipId, destinyAccount.getCharacters());
    }
}
