package com.pendsley.clovisbray;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.pendsley.clovisbray.authentication.ReinjectFieldsEvent;
import com.pendsley.clovisbray.inject.AccountModule;
import com.pendsley.clovisbray.model.Character;
import com.pendsley.clovisbray.model.FactionProgression;
import com.pendsley.clovisbray.network.BungieDestiny2Service;
import com.pendsley.clovisbray.presenter.FactionProgressionPresenter;
import com.pendsley.clovisbray.ui.AnimationUtil;
import com.pendsley.clovisbray.ui.DividerItemDecorator;
import com.pendsley.clovisbray.util.BusProvider;
import com.pendsley.clovisbray.viewcontroller.FactionProgressionViewController;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Fragment for displaying progression for each vendor.
 *
 * @author Phil Endsley
 */
public class FactionProgressionFragment extends Fragment implements FactionProgressionViewController,
                                                                    SwipeRefreshLayout.OnRefreshListener {

    static final String TAG = "FactionProgressionFragment";

    @Inject
    FactionProgressionPresenter factionProgressionPresenter;

    @Inject
    @Named(AccountModule.CHARACTERS)
    List<Character> characters;

    @Inject
    BusProvider busProvider;

    private SwipeRefreshLayout swipeRefreshLayout;

    private RecyclerView progressionRecyclerView;
    private ProgressionAdapter adapter;

    // This is on the activity's toolbar
    private Spinner characterToolbarSpinner;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectMembers();
    }

    @Override
    public void onResume() {
        super.onResume();
        busProvider.getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        busProvider.getBus().unregister(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        factionProgressionPresenter.dispose();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_faction_progression, container, false);

        factionProgressionPresenter.setFactionProgressionViewController(this);

        bindRecyclerView(root);
        bindSwipeRefreshLayout(root);

        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        factionProgressionPresenter.loadVendors(characters.get(0).getCharacterId());
    }

    @Override
    public void showLoadingIndicator() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoadingIndicator() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void displayFactionProgressions(List<FactionProgression> progressions) {

        adapter.setProgressions(progressions);
    }

    @Override
    public void displayError(int errorMessageResource) {
        displayErrorMessage(getString(errorMessageResource));
    }

    @Override
    public void displayError(String errorMessage) {
        displayErrorMessage(errorMessage);
    }

    @Override
    public void onRefresh() {
        loadDataForSelectedCharacter();
    }

    @Subscribe
    public void onRefreshToken(ReinjectFieldsEvent event) {
        injectMembers();
    }

    private void loadDataForSelectedCharacter() {
        Character selectedCharacter = (Character) characterToolbarSpinner.getSelectedItem();
        factionProgressionPresenter.loadVendors(selectedCharacter.getCharacterId());
    }

    private void displayErrorMessage(String errorMessage) {

        int textColor = getContext().getResources().getColor(R.color.secondary_text);

        Snackbar snackbar = Snackbar.make(progressionRecyclerView, errorMessage, Snackbar.LENGTH_INDEFINITE)
                .setActionTextColor(textColor);
        snackbar.setAction(R.string.dismiss, v -> snackbar.dismiss());
        snackbar.show();
    }

    private void injectMembers() {
        ((ClovisBrayApplication) getActivity().getApplication()).getApiComponent().inject(this);
    }

    private void bindRecyclerView(View root) {
        progressionRecyclerView = (RecyclerView) root.findViewById(R.id.progression_recycler_view);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(root.getContext());
        progressionRecyclerView.setLayoutManager(layoutManager);
        progressionRecyclerView.addItemDecoration(new DividerItemDecorator(root.getContext()));

        LayoutAnimationController animationController = AnimationUtil.fastFadeInLayoutAnimationController(root.getContext());
        progressionRecyclerView.setLayoutAnimation(animationController);

        characterToolbarSpinner = (Spinner) getActivity().findViewById(R.id.character_spinner);
        characterToolbarSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                loadDataForSelectedCharacter();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapter = new ProgressionAdapter();
        progressionRecyclerView.setAdapter(adapter);
    }

    private void bindSwipeRefreshLayout(View root) {
        swipeRefreshLayout = (SwipeRefreshLayout) root.findViewById(R.id.refresh_container);
        swipeRefreshLayout.setProgressBackgroundColorSchemeResource(R.color.accent);
        swipeRefreshLayout.setColorSchemeResources(R.color.icons);
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    private static final class ProgressionAdapter extends RecyclerView.Adapter<ProgressionViewHolder> {

        private List<FactionProgression> progressions;

        ProgressionAdapter() {
            this.progressions = new ArrayList<>();
        }

        void setProgressions(List<FactionProgression> progressions) {
            this.progressions = progressions;
            notifyDataSetChanged();
        }

        @Override
        public ProgressionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_progression,
                                                                         parent,
                                                                         false);
            return new ProgressionViewHolder(root);
        }

        @Override
        public void onBindViewHolder(ProgressionViewHolder holder, int position) {
            holder.bindData(progressions.get(position));
        }

        @Override
        public int getItemCount() {
            return progressions.size();
        }
    }

    private static final class ProgressionViewHolder extends RecyclerView.ViewHolder {

        private ImageView factionIconView;
        private TextView currentLevelView;
        private TextView nextLevelView;
        private ProgressBar progressionBar;

        ProgressionViewHolder(View itemView) {
            super(itemView);

            factionIconView = (ImageView) itemView.findViewById(R.id.faction_icon);
            currentLevelView = (TextView) itemView.findViewById(R.id.current_level);
            nextLevelView = (TextView) itemView.findViewById(R.id.next_level);
            progressionBar = (ProgressBar) itemView.findViewById(R.id.progression_progress);
        }

        void bindData(FactionProgression progression) {

            String url = BungieDestiny2Service.BASE_URL + progression.getIcon();

            Picasso.with(factionIconView.getContext()).load(url).into(factionIconView);

            int currentLevel = progression.getLevel();
            if (currentLevel == progression.getLevelCap()) {
                currentLevel--;
                progressionBar.setProgress(100);
            } else {
                progressionBar.setProgress((int) (100.0d * progression.getProgressToNextLevel() / progression.getNextLevelAt()));
            }

            currentLevelView.setText(String.valueOf(currentLevel));
            nextLevelView.setText(String.valueOf(currentLevel + 1));

            factionIconView.setOnClickListener((clickedView) ->
                                                       Toast.makeText(clickedView.getContext(),
                                                                      progression.getDescription(),
                                                                      Toast.LENGTH_LONG)
                                                               .show());
        }
    }
}
