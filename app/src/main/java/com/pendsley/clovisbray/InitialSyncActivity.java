package com.pendsley.clovisbray;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.pendsley.clovisbray.authentication.ReinjectFieldsEvent;
import com.pendsley.clovisbray.operation.CharacterOperation;
import com.pendsley.clovisbray.operation.InventorySyncOperation;
import com.pendsley.clovisbray.persistence.dao.ManifestHelper;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

public class InitialSyncActivity extends AppCompatActivity implements ManifestDownloadController.ManifestDownloadListener,
        InventorySyncOperation.SyncOperationCallback {

    @Inject
    CharacterOperation characterOperation;

    @Inject
    ManifestDownloadController manifestDownloadController;

    @Inject
    InventorySyncOperation inventorySyncOperation;

    private ManifestDownloadViewController manifestDownloadViewController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectFields();

        setContentView(R.layout.activity_manifest);

        manifestDownloadViewController = new ManifestDownloadViewController(this);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        startDownload();
    }

    @Override
    public void onManifestDownloadFinished(Throwable t) {
        if (t == null) {
            injectFields();

            characterOperation.updateCharacterClasses();
            Completable completable = inventorySyncOperation.deleteAllAndSyncCharactersAndInventory(this)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());

            completable.subscribeWith(new SyncSubscriber());
        } else {
            showErrorMessage();
        }
    }

    @Override
    public void manifestProgressChanged(final int messageId,
                                        final int percentFinished) {
        runOnUiThread(() -> manifestDownloadViewController.updateProgress(messageId,
                ((percentFinished == 0 ? 1 : percentFinished) / 2)));
    }

    @Override
    public void onProgressChanged(final int percentComplete) {
        runOnUiThread(() -> manifestDownloadViewController.updateProgress(R.string.downloading_items,
                ((percentComplete == 0 ? 1 : percentComplete) / 2) + 50));
    }

    private void onFinishedLoading(@Nullable Throwable t) {
        if (t == null) {
            launchInventoryActivity();
        } else {
            showErrorMessage();
        }
    }

    @Subscribe
    public void onReinjectFields(ReinjectFieldsEvent event) {
        injectFields();
    }


    public void startDownload() {
        manifestDownloadViewController.showProgressBar(true);

        manifestDownloadController.retrieveManifest(getDatabasePath(ManifestHelper.DATABASE_NAME), this);
    }

    private void showErrorMessage() {
        manifestDownloadViewController.showProgressBar(false);
    }

    private void injectFields() {
        ((ClovisBrayApplication) getApplication()).getApiComponent().inject(this);
    }

    @SuppressLint("WrongConstant") // The IntentCompat constants aren't recognized as being valid
    private void launchInventoryActivity() {
        Intent intent = new Intent(this, InventoryListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private final class SyncSubscriber extends DisposableCompletableObserver {

        @Override
        public void onError(Throwable e) {
            Log.e("", "Error getting inventory list", e);
            onFinishedLoading(e);
        }

        @Override
        public void onComplete() {
            onFinishedLoading(null);
        }
    }
}


