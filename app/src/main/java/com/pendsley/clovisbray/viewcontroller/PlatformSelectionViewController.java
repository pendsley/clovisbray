package com.pendsley.clovisbray.viewcontroller;

import com.pendsley.clovisbray.model.UserInfo;

import java.util.List;

/**
 * Platform selection view controller.
 *
 * @author Phil Endsley
 */
public interface PlatformSelectionViewController {

    void displayPlatforms(List<UserInfo> platformUserInfos);

    void launchManifestActivity();

    void displayError(int errorMessageResourceId);
}
