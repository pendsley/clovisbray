package com.pendsley.clovisbray.viewcontroller;

import com.pendsley.clovisbray.model.ItemSlot;

import java.util.List;

/**
 * Available slots for a given loadout view.
 *
 * @author Phil Endsley
 */
public interface NewLoadoutSlotSelectionViewController {

    void displayAvailableSlots(List<ItemSlot> availableSlots);
}
