package com.pendsley.clovisbray.viewcontroller;

import com.pendsley.clovisbray.model.ManifestInventoryItem;

import java.util.List;

/**
 * View for Postmaster list view.
 *
 * @author Phil Endsley
 */
public interface PostmasterViewController {

    void showLoadingIndicator();

    void hideLoadingIndicator();

    void displayItems(List<ManifestInventoryItem> postMasterItems);

    void displayError(int errorMessageResource);
}
