package com.pendsley.clovisbray.viewcontroller;

import android.content.Context;
import android.support.annotation.NonNull;

import com.pendsley.clovisbray.model.Character;
import com.pendsley.clovisbray.model.ManifestSocket;
import com.pendsley.clovisbray.model.Stat;

import java.util.List;

/**
 * View for an inventory item's details.
 *
 * @author Phil Endsley
 */
public interface InventoryItemDetailsViewController {

    void displaySockets(@NonNull List<ManifestSocket> sockets);

    void displayItemStats(List<Stat> stats);

    void setTransferEquipCharacterOptions(List<Character> characters,
                                          String itemCharacterId);

    void hideOptionsSheet();

    void showInformationMessage(String message);

    void showErrorMessage(String errorMessage);

    void leaveDetailsView();

    Context getContext();
}
