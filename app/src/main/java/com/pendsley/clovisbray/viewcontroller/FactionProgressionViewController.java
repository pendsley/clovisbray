package com.pendsley.clovisbray.viewcontroller;

import com.pendsley.clovisbray.model.FactionProgression;

import java.util.List;

/**
 * TODO
 */
public interface FactionProgressionViewController {

    void showLoadingIndicator();

    void hideLoadingIndicator();

    void displayFactionProgressions(List<FactionProgression> progressions);

    void displayError(int errorMessageResource);

    void displayError(String errorMessage);
}
