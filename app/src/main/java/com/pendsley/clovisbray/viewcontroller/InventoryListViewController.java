package com.pendsley.clovisbray.viewcontroller;

import android.content.Context;

import com.pendsley.clovisbray.model.LocalInventoryItem;

import java.util.List;

/**
 * View for displaying the list of inventory items.
 *
 * @author Phil Endsley
 */
public interface InventoryListViewController {

    void displayInventoryList(List<LocalInventoryItem> inventoryItems);

    void showLoadingIndicator();

    void hideLoadingIndicator();

    Context context();
}
