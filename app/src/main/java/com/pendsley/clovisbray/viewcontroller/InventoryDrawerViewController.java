package com.pendsley.clovisbray.viewcontroller;

import android.content.Context;

import com.pendsley.clovisbray.model.DrawerItem;
import com.pendsley.clovisbray.model.ItemClassType;

import java.util.List;

/**
 * Inventory navigation drawer view.
 *
 * @author Phil Endsley
 */
public interface InventoryDrawerViewController {

    void syncDrawerState();

    void closeDrawer();

    void initializeDrawerItems(List<DrawerItem> drawerItems);

    void navigateToInventoryList(ItemClassType itemClassType);

    void navigateToLoadoutsList(String characterId);

    void navigateToPostmaster();

    void navigateToFactions();

    void navigateToPlatformSelection();

    Context getContext();
}
