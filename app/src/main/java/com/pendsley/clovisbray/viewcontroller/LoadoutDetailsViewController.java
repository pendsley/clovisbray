package com.pendsley.clovisbray.viewcontroller;

import com.pendsley.clovisbray.model.ItemSlot;
import com.pendsley.clovisbray.model.Loadout;
import com.pendsley.clovisbray.model.LocalInventoryItem;

import java.util.List;

/**
 * View for displaying a loadout's details and items.
 *
 * @author Phil Endsley
 */
public interface LoadoutDetailsViewController {

    void displayLoadout(Loadout loadout,
                        List<LocalInventoryItem> loadoutItems);

    void clearLoadoutItemSlot(ItemSlot itemSlot);

    void showDeleteLoadoutDialog();

    void showErrorMessage(int errorMessageResource);

    void leaveLoadoutDetails();
}
