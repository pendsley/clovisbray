package com.pendsley.clovisbray.persistence.dao;

/**
 * TODO
 * @param <T>
 */
public interface InsertOperation<T> {

    void insert(T objectToInsert);

}
