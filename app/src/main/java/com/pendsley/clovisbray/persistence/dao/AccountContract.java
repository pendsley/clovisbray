package com.pendsley.clovisbray.persistence.dao;

import android.provider.BaseColumns;

/**
 * TODO
 */
public class AccountContract {

    private AccountContract() {
    }

    public static class AccountEntry implements BaseColumns {

        public static final String TABLE_NAME = "user";
        public static final String COLUMN_NAME_MEMBERSHIP_TYPE = "membership_type";
        public static final String COLUMN_NAME_MEMBERSHIP_ID = "membership_id";
        public static final String COLUMN_NAME_DISPLAY_NAME = "display_name";
    }
}
