package com.pendsley.clovisbray.persistence.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.pendsley.clovisbray.inject.DatabaseModule;
import com.pendsley.clovisbray.model.ManifestPlug;
import com.pendsley.clovisbray.model.ManifestSocket;
import com.pendsley.clovisbray.model.Socket;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * SQL operations for managing sockets.
 *
 * @author Phil Endsley
 */
public class SocketDao {

    private final SQLiteDatabase sqLiteDatabase;
    private final SQLiteDatabase manifestDatabase;
    private final Gson gson;

    @Inject
    SocketDao(@Named(DatabaseModule.WRITABLE_DATABASE) SQLiteDatabase sqLiteDatabase,
              @Named(DatabaseModule.MANIFEST_DATABASE) SQLiteDatabase manifestDatabase,
              Gson gson) {
        this.sqLiteDatabase = sqLiteDatabase;
        this.manifestDatabase = manifestDatabase;
        this.gson = gson;
    }

    public void batchInsertSockets(Map<String, List<Socket>> socketsMap) {

        sqLiteDatabase.beginTransaction();
        for (Map.Entry<String, List<Socket>> entry : socketsMap.entrySet()) {

            int columnIndex = 0;
            String instanceId = entry.getKey();

            for (Socket socket : entry.getValue()) {

                int rowIndex = 0;
                List<Long> socketHashes = socket.getReusablePlugHashes();
                if (socketHashes == null || socketHashes.isEmpty()) {
                    socketHashes = Lists.newArrayList(socket.getPlugHash());
                } else if (socket.getPlugHash() != null && !socketHashes.contains(socket.getPlugHash())){
                    socketHashes.add(socket.getPlugHash());
                }

                for (Long socketHash : socketHashes) {

                    boolean enabled = Objects.equals(socketHash, socket.getPlugHash()) && socket.getEnabled();

                    ContentValues contentValues = new ContentValues(4);
                    contentValues.put(SocketContract.SocketEntry.COLUMN_ITEM_INSTANCE_ID, instanceId);
                    contentValues.put(SocketContract.SocketEntry.COLUMN_SOCKET_HASH, socketHash);
                    contentValues.put(SocketContract.SocketEntry.COLUMN_COLUMN_ORDER, columnIndex);
                    contentValues.put(SocketContract.SocketEntry.COLUMN_ROW_ORDER, rowIndex);
                    contentValues.put(SocketContract.SocketEntry.COLUMN_ENABLED, enabled);

                    sqLiteDatabase.insert(SocketContract.SocketEntry.TABLE_NAME, null, contentValues);

                    rowIndex++;
                }

                columnIndex++;
            }
        }

        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();

    }

    public void deleteAllSockets() {
        sqLiteDatabase.delete(SocketContract.SocketEntry.TABLE_NAME, null, null);
    }

    public void deleteSocketsForInstanceIds(Collection<String> itemInstanceIds) {

        String commaSeparatedInstanceIds = Joiner.on(",").join(itemInstanceIds);

        sqLiteDatabase.delete(SocketContract.SocketEntry.TABLE_NAME,
                              SocketContract.SocketEntry.COLUMN_ITEM_INSTANCE_ID + " IN (" + commaSeparatedInstanceIds + ")",
                              null);

    }

    public List<ManifestSocket> getSocketsForItemInstanceId(String itemInstanceId) {

        Map<Long, Socket> socketMap = new HashMap<>();
        try (Cursor cursor = sqLiteDatabase.query(SocketContract.SocketEntry.TABLE_NAME,
                                                  null,
                                                  SocketContract.SocketEntry.COLUMN_ITEM_INSTANCE_ID + " = ?",
                                                  new String[] {itemInstanceId},
                                                  null,
                                                  null,
                                                  null)) {

            while (cursor.moveToNext()) {
                Socket socket = buildSocket(cursor);

                socketMap.put(socket.getPlugHash(), socket);
            }
        }

        List<ManifestSocket> manifestSockets = new ArrayList<>();
        if (!socketMap.keySet().isEmpty()) {
            manifestSockets = getManifestSockets(socketMap.keySet());
        }

        Iterator<ManifestSocket> manifestSocketIterator = manifestSockets.iterator();
        while (manifestSocketIterator.hasNext()) {
            ManifestSocket manifestSocket = manifestSocketIterator.next();

            if (!manifestSocket.getShowSocketPlug()) {
                manifestSocketIterator.remove();
                continue;
            }

            Socket socket = socketMap.get(manifestSocket.getHash());
            manifestSocket.setRowIndex(socket.getRowIndex());
            manifestSocket.setColumnIndex(socket.getColumnIndex());
        }

        return manifestSockets;
    }

    private List<ManifestSocket> getManifestSockets(Collection<Long> socketHashes) {

        String commaSeparatedHashes = Joiner.on(",").join(socketHashes);
        String whereClause = " id + 4294967296 IN (" + commaSeparatedHashes + ") OR id IN (" + commaSeparatedHashes + ")";

        List<ManifestSocket> sockets = new ArrayList<>();

        try (Cursor cursor = manifestDatabase.query(false,
                                                    ManifestTables.DESTINY_INVENTORY_ITEM_DEFINITION_TABLE_NAME,
                                                    new String[] {ManifestTables.COLUMN_NAME_JSON},
                                                    whereClause,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null)) {

            while (cursor.moveToNext()) {
                String json = cursor.getString(cursor.getColumnIndex(ManifestTables.COLUMN_NAME_JSON));

                ManifestSocket manifestSocket = gson.fromJson(json, ManifestSocket.class);

                ManifestPlug manifestPlug = manifestSocket.getPlug();
                if (manifestPlug != null) {
                    String plugLabel = manifestPlug.getUiPlugLabel();
                    if (!Strings.isNullOrEmpty(plugLabel) && "masterwork_interactable".equals(plugLabel)) {
                        manifestSocket.setShowSocketPlug(false);
                    }
                }
                if ("Restore Defaults".equals(manifestSocket.getItemTypeDisplayName()) ||
                        Strings.nullToEmpty(manifestSocket.getDisplayProperties().getName()).equals("Default Ornament")) {
                    manifestSocket.setShowSocketPlug(false);
                }

                sockets.add(manifestSocket);
            }

        }

        return sockets;
    }

    private Socket buildSocket(Cursor cursor) {
        Socket socket = new Socket();
        socket.setPlugHash(cursor.getLong(cursor.getColumnIndexOrThrow(SocketContract.SocketEntry.COLUMN_SOCKET_HASH)));
        socket.setColumnIndex(cursor.getInt(cursor.getColumnIndexOrThrow(SocketContract.SocketEntry.COLUMN_COLUMN_ORDER)));
        socket.setRowIndex(cursor.getInt(cursor.getColumnIndexOrThrow(SocketContract.SocketEntry.COLUMN_ROW_ORDER)));
        socket.setEnabled(1 == cursor.getInt(cursor.getColumnIndexOrThrow(SocketContract.SocketEntry.COLUMN_ENABLED)));
        return socket;
    }

}
