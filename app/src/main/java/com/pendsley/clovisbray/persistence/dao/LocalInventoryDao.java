package com.pendsley.clovisbray.persistence.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.common.base.Optional;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.pendsley.clovisbray.inject.DatabaseModule;
import com.pendsley.clovisbray.model.ItemClassType;
import com.pendsley.clovisbray.model.LocalInventoryItem;
import com.pendsley.clovisbray.model.ManifestBucketDefinition;
import com.pendsley.clovisbray.model.ManifestInventoryItem;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;

/**
 * Database operations for local inventory items.
 *
 * @author Phil Endsley
 */
public class LocalInventoryDao {

    private static final String TAG = "LocalInventoryDao";

    private final SQLiteDatabase sqLiteDatabase;
    private final SQLiteDatabase manifestDatabase;
    private final Gson gson;

    @Inject
    LocalInventoryDao(@Named(DatabaseModule.WRITABLE_DATABASE) SQLiteDatabase sqLiteDatabase,
                      @Named(DatabaseModule.MANIFEST_DATABASE) SQLiteDatabase manifestDatabase,
                      Gson gson) {
        this.sqLiteDatabase = sqLiteDatabase;
        this.manifestDatabase = manifestDatabase;
        this.gson = gson;
    }

    public Map<String, LocalInventoryItem> getAllLocalItemsMap() {

        Map<String, LocalInventoryItem> itemsMap = Maps.newConcurrentMap();

        try (Cursor cursor = sqLiteDatabase.query(false,
                InventoryContract.InventoryEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null,
                null)) {

            while (cursor.moveToNext()) {
                LocalInventoryItem item = createItem(cursor);
                itemsMap.put(item.getInstanceId(), item);
            }
        }

        return itemsMap;
    }

    public Flowable<List<LocalInventoryItem>> getLocalItemsByClassTypeFlowable(final ItemClassType itemClassType) {
        return Flowable.create(emitter -> {
            emitter.onNext(getLocalItemsByClassType(itemClassType));
            emitter.onComplete();
        }, BackpressureStrategy.BUFFER);
    }

    private List<LocalInventoryItem> getLocalItemsByClassType(ItemClassType itemClassType) {

        List<LocalInventoryItem> items = Lists.newArrayList();

        String where = null;
        String[] selectionArgs = null;

        if (ItemClassType.ALL != itemClassType) {
            where = InventoryContract.InventoryEntry.COLUMN_ITEM_CLASS_TYPE + "= ?";
            selectionArgs = new String[] {itemClassType.name()};
        }

        try (Cursor cursor = sqLiteDatabase.query(false,
                InventoryContract.InventoryEntry.TABLE_NAME,
                null,
                where,
                selectionArgs,
                null,
                null,
                null,
                null)) {

            while (cursor.moveToNext()) {
                items.add(createItem(cursor));
            }
        }

        return items;
    }

    public Flowable<List<LocalInventoryItem>> getLocalItemsByCategoryFlowable(final String itemCategory) {
        return Flowable.create(emitter -> {
            emitter.onNext(getLocalItemsByTypeDescription(itemCategory));
            emitter.onComplete();
        }, BackpressureStrategy.BUFFER);
    }

    private List<LocalInventoryItem> getLocalItemsByTypeDescription(String itemTypeDescription) {

        List<LocalInventoryItem> items = Lists.newArrayList();

        try (Cursor cursor = sqLiteDatabase.query(false,
                InventoryContract.InventoryEntry.TABLE_NAME,
                null,
                InventoryContract.InventoryEntry.COLUMN_ITEM_CATEGORY + "= ?",
                new String[]{itemTypeDescription},
                null,
                null,
                null,
                null)) {

            while (cursor.moveToNext()) {
                items.add(createItem(cursor));
            }
        }

        return items;
    }

    @Nullable
    public LocalInventoryItem getLocalItemByInstanceId(String instanceId) {
        try (Cursor cursor = sqLiteDatabase.query(InventoryContract.InventoryEntry.TABLE_NAME,
                null,
                InventoryContract.InventoryEntry.COLUMN_NAME_ITEM_INSTANCE_ID + " = ?",
                new String[]{instanceId},
                null,
                null,
                null)) {

            if (cursor.moveToNext()) {
                return createItem(cursor);
            }
        }

        return null;
    }

    public String getItemCategoryByBucketHash(Long bucketHash) {

        final String WEAPONS_SUFFIX = " Weapons";

        String whereClause = "id + 4294967296 = " + bucketHash + " OR id = " + bucketHash;

        try (Cursor cursor = manifestDatabase.query(false,
                                                    ManifestTables.DESTINY_INVENTORY_BUCKET_DEFINITION,
                                                    new String[]{ManifestTables.COLUMN_NAME_JSON},
                                                    whereClause,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null)) {

            if (cursor.moveToFirst()) {
                String json = cursor.getString(cursor.getColumnIndexOrThrow(ManifestTables.COLUMN_NAME_JSON));
                ManifestBucketDefinition manifestBucketDefinition = gson.fromJson(json, ManifestBucketDefinition.class);

                String categoryName = manifestBucketDefinition.getDisplayProperties().getName();
                if (categoryName.contains(WEAPONS_SUFFIX)) {
                    categoryName = categoryName.replace(WEAPONS_SUFFIX, "");
                }
                return categoryName;
            }
        }

        return "";
    }

    public void batchInsertItems(Set<ManifestInventoryItem> items) {

        sqLiteDatabase.beginTransaction();
        for (ManifestInventoryItem item : items) {

            ContentValues contentValues = new ContentValues();
            contentValues.put(InventoryContract.InventoryEntry.COLUMN_NAME_ITEM_INSTANCE_ID, item.getItemInstanceId());
            contentValues.put(InventoryContract.InventoryEntry.COLUMN_NAME_ITEM_HASH, item.getHash());
            contentValues.put(InventoryContract.InventoryEntry.COLUMN_NAME_ITEM_NAME, item.getDisplayProperties().getName());
            contentValues.put(InventoryContract.InventoryEntry.COLUMN_NAME_LIGHT, item.getLightLevel());
            contentValues.put(InventoryContract.InventoryEntry.COLUMN_ITEM_TYPE_NAME, item.getItemTypeDisplayName());
            contentValues.put(InventoryContract.InventoryEntry.COLUMN_CHARACTER_ID, item.getCharacterId());
            contentValues.put(InventoryContract.InventoryEntry.COLUMN_ITEM_CLASS_TYPE, item.getItemClassType().name());
            contentValues.put(InventoryContract.InventoryEntry.COLUMN_ITEM_CATEGORY, item.getItemCategory());
            contentValues.put(InventoryContract.InventoryEntry.COLUMN_EQUIPPED, item.isEquipped());
            contentValues.put(InventoryContract.InventoryEntry.COLUMN_DAMAGE_TYPE, item.getDamageType());
            contentValues.put(InventoryContract.InventoryEntry.COLUMN_DAMAGE_TYPE_ICON, item.getDamageTypeIcon());
            contentValues.put(InventoryContract.InventoryEntry.COLUMN_MASTER_WORKS, item.isMasterWorks());

            sqLiteDatabase.insert(InventoryContract.InventoryEntry.TABLE_NAME, null, contentValues);
        }
        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();
    }

    public void deleteAllItems() {

        sqLiteDatabase.delete(InventoryContract.InventoryEntry.TABLE_NAME, null, null);
    }

    public void deleteItems(Set<String> itemIds) {
        StringBuilder where = new StringBuilder(InventoryContract.InventoryEntry.COLUMN_NAME_ITEM_INSTANCE_ID + " IN (");
        int count = 0;
        for (String itemId : itemIds) {
            if (count > 0) {
                where.append(",");
            }
            where.append(itemId);
            count++;
        }
        where.append(")");

        sqLiteDatabase.delete(InventoryContract.InventoryEntry.TABLE_NAME, where.toString(), null);
    }

    public void updateWeaponCharacterIndex(String itemInstanceId,
                                           String characterId) {

        ContentValues contentValues = new ContentValues(1);
        contentValues.put(InventoryContract.InventoryEntry.COLUMN_CHARACTER_ID, characterId);

        sqLiteDatabase.update(InventoryContract.InventoryEntry.TABLE_NAME,
                contentValues,
                InventoryContract.InventoryEntry.COLUMN_NAME_ITEM_INSTANCE_ID + " = ?",
                new String[]{itemInstanceId});
    }

    public Optional<LocalInventoryItem> getCurrentlyEquippedItem(String itemCategory,
                                                                 String characterId) {

        String whereClause = InventoryContract.InventoryEntry.COLUMN_CHARACTER_ID + " = " +
                characterId + " AND " + InventoryContract.InventoryEntry.COLUMN_EQUIPPED +
                " = 1 AND " + InventoryContract.InventoryEntry.COLUMN_ITEM_CATEGORY + " = ?";

        Crashlytics.log(Log.DEBUG,
                TAG,
                String.format("getCurrentlyEquippedItem query [%s] category [%s]", whereClause, itemCategory));


        try (Cursor cursor = sqLiteDatabase.query(InventoryContract.InventoryEntry.TABLE_NAME,
                null,
                whereClause,
                new String[]{itemCategory},
                null,
                null,
                null)) {

            if (cursor.moveToNext()) {
                return Optional.of(createItem(cursor));
            }
        }

        return Optional.absent();
    }

    public void updateWeaponEquippedStatus(String itemInstanceId,
                                           String previousItemInstanceId) {

        ContentValues setEquippedValues = new ContentValues(1);
        setEquippedValues.put(InventoryContract.InventoryEntry.COLUMN_EQUIPPED, true);

        sqLiteDatabase.update(InventoryContract.InventoryEntry.TABLE_NAME,
                setEquippedValues,
                InventoryContract.InventoryEntry.COLUMN_NAME_ITEM_INSTANCE_ID + " = ?",
                new String[]{itemInstanceId});

        ContentValues setUnequippedValues = new ContentValues(1);
        setUnequippedValues.put(InventoryContract.InventoryEntry.COLUMN_EQUIPPED, false);

        if (!Strings.isNullOrEmpty(previousItemInstanceId)) {
            sqLiteDatabase.update(InventoryContract.InventoryEntry.TABLE_NAME,
                    setUnequippedValues,
                    InventoryContract.InventoryEntry.COLUMN_NAME_ITEM_INSTANCE_ID + " = ?",
                    new String[]{previousItemInstanceId});
        }
    }

    private LocalInventoryItem createItem(Cursor cursor) {

        LocalInventoryItem item = new LocalInventoryItem();
        item.setInstanceId(cursor.getString(cursor.getColumnIndexOrThrow(InventoryContract.InventoryEntry.COLUMN_NAME_ITEM_INSTANCE_ID)));
        item.setHashId(cursor.getString(cursor.getColumnIndexOrThrow(InventoryContract.InventoryEntry.COLUMN_NAME_ITEM_HASH)));
        item.setName(cursor.getString(cursor.getColumnIndexOrThrow(InventoryContract.InventoryEntry.COLUMN_NAME_ITEM_NAME)));
        item.setLight(cursor.getInt(cursor.getColumnIndexOrThrow(InventoryContract.InventoryEntry.COLUMN_NAME_LIGHT)));
        item.setItemTypeName(cursor.getString(cursor.getColumnIndexOrThrow(InventoryContract.InventoryEntry.COLUMN_ITEM_TYPE_NAME)));
        item.setItemType(cursor.getString(cursor.getColumnIndexOrThrow(InventoryContract.InventoryEntry.COLUMN_ITEM_CATEGORY)));
        item.setCharacterId(cursor.getString(cursor.getColumnIndexOrThrow(InventoryContract.InventoryEntry.COLUMN_CHARACTER_ID)));
        item.setItemClassType(ItemClassType.fromString(cursor.getString(cursor.getColumnIndexOrThrow(InventoryContract.InventoryEntry.COLUMN_ITEM_CLASS_TYPE))));
        item.setEquipped(1 == cursor.getInt(cursor.getColumnIndexOrThrow(InventoryContract.InventoryEntry.COLUMN_EQUIPPED)));
        item.setDamageType(cursor.getString(cursor.getColumnIndexOrThrow(InventoryContract.InventoryEntry.COLUMN_DAMAGE_TYPE)));
        item.setDamageIcon(cursor.getString(cursor.getColumnIndexOrThrow(InventoryContract.InventoryEntry.COLUMN_DAMAGE_TYPE_ICON)));
        item.setMasterWorks(1 == cursor.getInt(cursor.getColumnIndexOrThrow(InventoryContract.InventoryEntry.COLUMN_MASTER_WORKS)));
        return item;
    }
}

