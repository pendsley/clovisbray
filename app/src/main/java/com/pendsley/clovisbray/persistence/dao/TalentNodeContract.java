package com.pendsley.clovisbray.persistence.dao;

import android.provider.BaseColumns;

/**
 * Database table name and columns for talent nodes.
 *
 * @author Phil Endsley
 */
public class TalentNodeContract {

    private TalentNodeContract() {
    }

    public static class TalentNodeEntry implements BaseColumns {

        public static final String TABLE_NAME = "talent_nodes";
        public static final String COLUMN_NAME_ITEM_INSTANCE_ID = "item_instance_id";
        public static final String COLUMN_NAME_NODE_STEP_NAME = "node_step_name";
        public static final String COLUMN_NAME_NODE_STEP_DESCRIPTION = "node_step_description";
        public static final String COLUMN_NAME_NODE_ICON = "node_icon";
        public static final String COLUMN_NAME_STEP_INDEX = "step_index";
        public static final String COLUMN_ROW = "row";
        public static final String COLUMN_COLUMN = "column";
    }
}
