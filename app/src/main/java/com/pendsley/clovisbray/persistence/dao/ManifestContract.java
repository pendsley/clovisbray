package com.pendsley.clovisbray.persistence.dao;

import android.provider.BaseColumns;

/**
 * TODO
 */
public class ManifestContract {

    private ManifestContract() {
    }

    public static class ManifestEntry implements BaseColumns {

        public static final String TABLE_NAME = "manifest";
        public static final String COLUMN_NAME_VERSION = "version";
    }
}
