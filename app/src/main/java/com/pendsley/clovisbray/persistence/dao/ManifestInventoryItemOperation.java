package com.pendsley.clovisbray.persistence.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.gson.Gson;
import com.pendsley.clovisbray.inject.DatabaseModule;
import com.pendsley.clovisbray.model.Item;
import com.pendsley.clovisbray.model.ManifestInventoryItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.annotations.Nullable;

/**
 * TODO
 */
public class ManifestInventoryItemOperation {

    private final SQLiteDatabase sqLiteDatabase;
    private final Gson gson;

    @Inject
    public ManifestInventoryItemOperation(@Named(DatabaseModule.MANIFEST_DATABASE) SQLiteDatabase sqLiteDatabase,
                                          Gson gson) {
        this.sqLiteDatabase = sqLiteDatabase;
        this.gson = gson;
    }

    // TODO - hard code weapon bucket hashes?
    // primary: 1.498876634E9
    // special: 2.465295065E9
    // heavy: 9.53998645E8

    public List<Item> getItems(Collection<Long> itemHashes) {

        String argumentParams = buildQueryString(itemHashes);

        String selection = ManifestTables.COLUMN_NAME_ID + " + 4294967296 IN " + argumentParams + " OR " +
                ManifestTables.COLUMN_NAME_ID + " IN " + argumentParams;

//        String[] hashes = ObjectArrays.concat(itemHashes.toArray(new String[itemHashes.size()]),
//                itemHashes.toArray(new String[itemHashes.size()]),
//                String.class);

        try (Cursor cursor = sqLiteDatabase.query(ManifestTables.DESTINY_INVENTORY_ITEM_DEFINITION_TABLE_NAME,
                                                  new String[] {ManifestTables.COLUMN_NAME_JSON},
                                                  selection,
                                                  null,
                                                  null,
                                                  null,
                                                  null)) {

            List<Item> items = new ArrayList<>();

            while (cursor.moveToNext()) {
                String json = cursor.getString(cursor.getColumnIndexOrThrow(ManifestTables.COLUMN_NAME_JSON));
                items.add(gson.fromJson(json, Item.class));
            }

            return items;
        }
    }

    @Nullable
    public ManifestInventoryItem getManifestInventoryItem(Long itemHash) {

        String selection = ManifestTables.COLUMN_NAME_ID + " + 4294967296 = " + itemHash + " OR " +
                ManifestTables.COLUMN_NAME_ID + " = " + itemHash;

        try (Cursor cursor = sqLiteDatabase.query(ManifestTables.DESTINY_INVENTORY_ITEM_DEFINITION_TABLE_NAME,
                                                  new String[] {ManifestTables.COLUMN_NAME_JSON},
                                                  selection,
                                                  null,
                                                  null,
                                                  null,
                                                  null)) {


            if (cursor.moveToFirst()) {
                String json = cursor.getString(cursor.getColumnIndexOrThrow(ManifestTables.COLUMN_NAME_JSON));
                return gson.fromJson(json, ManifestInventoryItem.class);
            }

            return null;
        }
    }

    private String buildQueryString(Collection<Long> values) {

        StringBuilder clauseBuilder = new StringBuilder("(");
        int i = 0;
        for (Long value : values) {
            if (i > 0) {
                clauseBuilder.append(",");
            }
            clauseBuilder.append(value);

            i++;
        }
        clauseBuilder.append(")");

        return clauseBuilder.toString();
    }
}
