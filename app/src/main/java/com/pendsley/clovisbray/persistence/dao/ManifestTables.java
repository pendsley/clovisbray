package com.pendsley.clovisbray.persistence.dao;

/**
 * Tables and columns from the Destiny manifest.
 *
 * @author Phil Endsley
 */
class ManifestTables {

    private ManifestTables() {
    }

    static final String DESTINY_CLASS_DEFINITION = "DestinyClassDefinition";
    static final String DESTINY_DAMAGE_TYPE_DEFINITION = "DestinyDamageTypeDefinition";
    static final String DESTINY_FACTION_DEFINITION = "DestinyFactionDefinition";
    static final String DESTINY_INVENTORY_BUCKET_DEFINITION = "DestinyInventoryBucketDefinition";
    static final String DESTINY_INVENTORY_ITEM_DEFINITION_TABLE_NAME = "DestinyInventoryItemDefinition";
    static final String DESTINY_TALENT_GRID_DEFINITION = "DestinyTalentGridDefinition";
    static final String DESTINY_STAT_DEFINITION = "DestinyStatDefinition";
    static final String DESTINY_SANDBOX_PERK_DEFINITION = "DestinySandboxPerkDefinition";

    static final String COLUMN_NAME_ID = "id";
    static final String COLUMN_NAME_JSON = "json";
}
