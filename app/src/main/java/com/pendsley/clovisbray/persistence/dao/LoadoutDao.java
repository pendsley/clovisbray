package com.pendsley.clovisbray.persistence.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.pendsley.clovisbray.inject.DatabaseModule;
import com.pendsley.clovisbray.model.Loadout;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.CompletableOnSubscribe;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.annotations.NonNull;

/**
 * Database operations for loadouts.
 *
 * @author Phil Endsley
 */
public class LoadoutDao {

    private final SQLiteDatabase sqLiteDatabase;

    @Inject
    LoadoutDao(@Named(DatabaseModule.WRITABLE_DATABASE) SQLiteDatabase sqLiteDatabase) {
        this.sqLiteDatabase = sqLiteDatabase;
    }

    public Flowable<List<Loadout>> getLoadoutsFlowable(final String characterId) {
        return Flowable.create(new FlowableOnSubscribe<List<Loadout>>() {
            @Override
            public void subscribe(@NonNull FlowableEmitter<List<Loadout>> emitter) throws Exception {
                emitter.onNext(getLoadouts(characterId));
                emitter.onComplete();
            }
        }, BackpressureStrategy.BUFFER);
    }

    private List<Loadout> getLoadouts(String characterId) {

        List<Loadout> loadouts = Lists.newArrayList();

        try (Cursor cursor = sqLiteDatabase.query(false,
                LoadoutContract.LoadoutEntry.TABLE_NAME,
                null,
                LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_CHARACTER_ID + " = ?",
                new String[] {characterId},
                null,
                null,
                null,
                null)) {

            while (cursor.moveToNext()) {
                Loadout loadout = new Loadout();

                loadout.setLoadoutId(cursor.getInt(cursor.getColumnIndex(LoadoutContract.LoadoutEntry._ID)));
                loadout.setName(cursor.getString(cursor.getColumnIndexOrThrow(LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_NAME)));
                loadout.setCharacterId(cursor.getString(cursor.getColumnIndexOrThrow(LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_CHARACTER_ID)));
                loadout.setPrimaryItemInstanceId(cursor.getString(cursor.getColumnIndexOrThrow(LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_PRIMARY_INSTANCE)));
                loadout.setSpecialItemInstanceId(cursor.getString(cursor.getColumnIndexOrThrow(LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_SPECIAL_INSTANCE)));
                loadout.setHeavyItemInstanceId(cursor.getString(cursor.getColumnIndexOrThrow(LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_HEAVY_INSTANCE)));
                loadout.setGhostItemInstanceId(cursor.getString(cursor.getColumnIndexOrThrow(LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_GHOST_INSTANCE)));
                loadout.setHelmetItemInstanceId(cursor.getString(cursor.getColumnIndexOrThrow(LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_HELMET_INSTANCE)));
                loadout.setGauntletsItemInstanceId(cursor.getString(cursor.getColumnIndexOrThrow(LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_GAUNTLETS_INSTANCE)));
                loadout.setChestItemInstanceId(cursor.getString(cursor.getColumnIndexOrThrow(LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_CHEST_INSTANCE)));
                loadout.setLegsItemInstanceId(cursor.getString(cursor.getColumnIndexOrThrow(LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_LEGS_INSTANCE)));
                loadout.setClassItemItemInstanceId(cursor.getString(cursor.getColumnIndexOrThrow(LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_CLASS_ITEM_INSTANCE)));
                loadout.setArtifactItemInstanceId(cursor.getString(cursor.getColumnIndexOrThrow(LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_ARTIFACT_INSTANCE)));

                loadouts.add(loadout);
            }
        }

        return loadouts;
    }

    public Completable getSaveNewLoadoutCompletable(final Loadout loadout) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(@NonNull CompletableEmitter emitter) throws Exception {
                saveNewLoadout(loadout);
                emitter.onComplete();
            }
        });
    }

    private void saveNewLoadout(Loadout loadout) {

        sqLiteDatabase.insert(LoadoutContract.LoadoutEntry.TABLE_NAME,
                              null,
                              getLoadoutContentValues(loadout));
    }

    public Completable getUpdateLoadoutCompletable(final Loadout loadout) {
        return Completable.create(new CompletableOnSubscribe() {
            @Override
            public void subscribe(@NonNull CompletableEmitter emitter) throws Exception {
                updateLoadout(loadout);
                emitter.onComplete();
            }
        });
    }

    private void updateLoadout(Loadout loadout) {

        String where = LoadoutContract.LoadoutEntry._ID + " = " + loadout.getLoadoutId();
        sqLiteDatabase.update(LoadoutContract.LoadoutEntry.TABLE_NAME,
                              getLoadoutContentValues(loadout),
                              where,
                              null);
    }

    public void deleteLoadout(int loadoutId) {
        sqLiteDatabase.delete(LoadoutContract.LoadoutEntry.TABLE_NAME,
                LoadoutContract.LoadoutEntry._ID + " = " + loadoutId,
                null);
    }

    private static ContentValues getLoadoutContentValues(Loadout loadout) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_NAME, loadout.getName());
        contentValues.put(LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_CHARACTER_ID, loadout.getCharacterId());

        String itemInstanceId = loadout.getPrimaryItemInstanceId();

        if (!Strings.isNullOrEmpty(itemInstanceId)) {
            contentValues.put(LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_PRIMARY_INSTANCE, itemInstanceId);
        }

        itemInstanceId = loadout.getSpecialItemInstanceId();
        if (!Strings.isNullOrEmpty(itemInstanceId)) {
            contentValues.put(LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_SPECIAL_INSTANCE, itemInstanceId);
        }

        itemInstanceId = loadout.getHeavyItemInstanceId();
        if (!Strings.isNullOrEmpty(itemInstanceId)) {
            contentValues.put(LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_HEAVY_INSTANCE, itemInstanceId);
        }

        itemInstanceId = loadout.getGhostItemInstanceId();
        if (!Strings.isNullOrEmpty(itemInstanceId)) {
            contentValues.put(LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_GHOST_INSTANCE, itemInstanceId);
        }

        itemInstanceId = loadout.getHelmetItemInstanceId();
        if (!Strings.isNullOrEmpty(itemInstanceId)) {
            contentValues.put(LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_HELMET_INSTANCE, itemInstanceId);
        }

        itemInstanceId = loadout.getGauntletsItemInstanceId();
        if (!Strings.isNullOrEmpty(itemInstanceId)) {
            contentValues.put(LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_GAUNTLETS_INSTANCE, itemInstanceId);
        }

        itemInstanceId = loadout.getChestItemInstanceId();
        if (!Strings.isNullOrEmpty(itemInstanceId)) {
            contentValues.put(LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_CHEST_INSTANCE, itemInstanceId);
        }

        itemInstanceId = loadout.getLegsItemInstanceId();
        if (!Strings.isNullOrEmpty(itemInstanceId)) {
            contentValues.put(LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_LEGS_INSTANCE, itemInstanceId);
        }

        itemInstanceId = loadout.getClassItemItemInstanceId();
        if (!Strings.isNullOrEmpty(itemInstanceId)) {
            contentValues.put(LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_CLASS_ITEM_INSTANCE, itemInstanceId);
        }

        itemInstanceId = loadout.getArtifactItemInstanceId();
        if (!Strings.isNullOrEmpty(itemInstanceId)) {
            contentValues.put(LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_ARTIFACT_INSTANCE, itemInstanceId);
        }

        return contentValues;
    }
}
