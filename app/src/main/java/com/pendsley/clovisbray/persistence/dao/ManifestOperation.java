package com.pendsley.clovisbray.persistence.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.pendsley.clovisbray.inject.DatabaseModule;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * TODO
 */
public class ManifestOperation {

    private final SQLiteDatabase sqLiteDatabase;

    @Inject
    public ManifestOperation(@Named(DatabaseModule.READABLE_DATABASE) SQLiteDatabase sqLiteDatabase) {
        this.sqLiteDatabase = sqLiteDatabase;
    }

    public void deleteManifestInfo() {
        sqLiteDatabase.delete(ManifestContract.ManifestEntry.TABLE_NAME, null, null);
    }

    public void insertManifestInfo(String version) {
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(ManifestContract.ManifestEntry.COLUMN_NAME_VERSION, version);
        sqLiteDatabase.insert(ManifestContract.ManifestEntry.TABLE_NAME, null, contentValues);
    }

    public String getLocalManifestVersion() {

        try (Cursor cursor = sqLiteDatabase.query(ManifestContract.ManifestEntry.TABLE_NAME,
                new String[] {ManifestContract.ManifestEntry.COLUMN_NAME_VERSION },
                null,
                null,
                null,
                null,
                null)) {

            String localVersion = "";

            if (cursor.moveToNext()) {
                localVersion = cursor.getString(cursor.getColumnIndexOrThrow(
                        ManifestContract.ManifestEntry.COLUMN_NAME_VERSION));
            }

            return localVersion;
        }
    }
}
