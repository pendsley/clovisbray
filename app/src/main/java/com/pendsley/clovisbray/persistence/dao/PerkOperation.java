package com.pendsley.clovisbray.persistence.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.pendsley.clovisbray.inject.DatabaseModule;
import com.pendsley.clovisbray.model.ManifestPerk;
import com.pendsley.clovisbray.model.OnlinePerk;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * TODO
 */
public class PerkOperation {

    private final SQLiteDatabase sqLiteDatabase;
    private final Gson gson;

    @Inject
    PerkOperation(@Named(DatabaseModule.MANIFEST_DATABASE) SQLiteDatabase sqLiteDatabase,
                  Gson gson) {
        this.sqLiteDatabase = sqLiteDatabase;
        this.gson = gson;
    }

    public List<ManifestPerk> getPerks(List<OnlinePerk> onlinePerks) {

        List<Long> perkHashes = Lists.transform(onlinePerks, OnlinePerk::getPerkHash);

        return getPerksForHashes(perkHashes);
    }

    public List<ManifestPerk> getPerksForHashes(List<Long> perkHashes) {

        String commaDelimmited = Joiner.on(",").join(perkHashes);
        String whereClause = "id + 4294967296 IN (" + commaDelimmited + ") OR id IN (" + commaDelimmited+  ")";

        List<ManifestPerk> manifestPerks = new ArrayList<>();

        try (Cursor cursor = sqLiteDatabase.query(ManifestTables.DESTINY_SANDBOX_PERK_DEFINITION,
                                                  new String[] {ManifestTables.COLUMN_NAME_JSON},
                                                  whereClause,
                                                  null,
                                                  null,
                                                  null,
                                                  null)) {

            while (cursor.moveToNext()) {
                String json = cursor.getString(cursor.getColumnIndex(ManifestTables.COLUMN_NAME_JSON));
                ManifestPerk manifestPerk = gson.fromJson(json, ManifestPerk.class);

                if (manifestPerk.getDisplayable()) {
                    manifestPerks.add(manifestPerk);
                }
            }

        }

        return manifestPerks;
    }
}
