package com.pendsley.clovisbray.persistence.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.pendsley.clovisbray.inject.DatabaseModule;
import com.pendsley.clovisbray.model.DamageType;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * TODO
 */
public class DamageTypeOperation {

    private final SQLiteDatabase manifestDatabase;
    private final Gson gson;

    @Inject
    DamageTypeOperation(@Named(DatabaseModule.MANIFEST_DATABASE) SQLiteDatabase manifestDatabase,
                        Gson gson) {
        this.manifestDatabase = manifestDatabase;
        this.gson = gson;
    }

    @Nullable
    public DamageType getDamageType(Long damageTypeHash) {

        String whereClause = "id + 4294967296 = " + damageTypeHash + " OR id = " + damageTypeHash;

        try (Cursor cursor = manifestDatabase.query(ManifestTables.DESTINY_DAMAGE_TYPE_DEFINITION,
                                                  new String[] {ManifestTables.COLUMN_NAME_JSON},
                                                  whereClause,
                                                  null,
                                                  null,
                                                  null,
                                                  null)) {

            if (cursor.moveToFirst()) {
                String json = cursor.getString(cursor.getColumnIndex(ManifestTables.COLUMN_NAME_JSON));
                return gson.fromJson(json, DamageType.class);
            }

        }

        return null;
    }
}
