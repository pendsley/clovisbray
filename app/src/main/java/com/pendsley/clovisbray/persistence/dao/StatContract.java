package com.pendsley.clovisbray.persistence.dao;

import android.provider.BaseColumns;

/**
 * Table and column names for item stats.
 *
 * @author Phil Endsley
 */
public class StatContract {

    private StatContract() {
    }

    public static class StatEntry implements BaseColumns {

        public static final String TABLE_NAME = "stats";
        public static final String COLUMN_INSTANCE_ITEM_ID = "item_instance_id";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_VALUE = "value";
        public static final String COLUMN_MINIMUM_VALUE = "minimum_value";
        public static final String COLUMN_MAXIMUM_VALUE = "maximum_value";
    }
}
