package com.pendsley.clovisbray.persistence.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.pendsley.clovisbray.authentication.persistence.CharacterContract;
import com.pendsley.clovisbray.inject.DatabaseModule;
import com.pendsley.clovisbray.model.Character;
import com.pendsley.clovisbray.model.ClassInformation;
import com.pendsley.clovisbray.model.ManifestClass;
import com.pendsley.clovisbray.model.UserInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Database operations for user account and character information.
 *
 * @author Phil endsley
 */
public class AccountOperation {

    private final SQLiteDatabase sqLiteDatabase;
    private final SQLiteDatabase manifestDatabase;
    private final Gson gson;

    @Inject
    public AccountOperation(@Named(DatabaseModule.WRITABLE_DATABASE) SQLiteDatabase sqLiteDatabase,
                            @Named(DatabaseModule.MANIFEST_DATABASE) SQLiteDatabase manifestDatabase,
                            Gson gson) {
        this.sqLiteDatabase = sqLiteDatabase;
        this.manifestDatabase = manifestDatabase;
        this.gson = gson;
    }

    public void saveUserInfo(UserInfo userInfo) {

        ContentValues contentValues = new ContentValues(3);
        contentValues.put(AccountContract.AccountEntry.COLUMN_NAME_DISPLAY_NAME,
                          userInfo.getDisplayName());
        contentValues.put(AccountContract.AccountEntry.COLUMN_NAME_MEMBERSHIP_ID,
                          userInfo.getMembershipId());
        contentValues.put(AccountContract.AccountEntry.COLUMN_NAME_MEMBERSHIP_TYPE,
                          userInfo.getMembershipType());

        sqLiteDatabase.insert(AccountContract.AccountEntry.TABLE_NAME, null, contentValues);
    }

    public void updateUserMembershipInfo(String displayName,
                                         int membershipType,
                                         String membershipId) {

        ContentValues contentValues = new ContentValues(2);
        contentValues.put(AccountContract.AccountEntry.COLUMN_NAME_MEMBERSHIP_ID,
                          membershipId);
        contentValues.put(AccountContract.AccountEntry.COLUMN_NAME_MEMBERSHIP_TYPE,
                          membershipType);

        sqLiteDatabase.update(AccountContract.AccountEntry.TABLE_NAME,
                              contentValues,
                              AccountContract.AccountEntry.COLUMN_NAME_DISPLAY_NAME + " = ?",
                              new String[] {displayName});
    }

    public void deleteUserInfo() {
        sqLiteDatabase.delete(AccountContract.AccountEntry.TABLE_NAME, null, null);
    }

    public void saveCharacterInfo(String membershipId,
                                  List<Character> characters) {

        for (Character character : characters) {

            ContentValues contentValues = new ContentValues(7);
            contentValues.put(CharacterContract.CharacterEntry.COLUMN_NAME_MEMBERSHIP_ID, membershipId);
            contentValues.put(CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_ID, character.getCharacterId());
            contentValues.put(CharacterContract.CharacterEntry.COLUMN_NAME_EMBLEM_PATH, character.getEmblemPath());
            contentValues.put(CharacterContract.CharacterEntry.COLUMN_NAME_BACKGROUND_PATH, character.getBackgroundPath());
            contentValues.put(CharacterContract.CharacterEntry.COLUMN_NAME_LIGHT_LEVEL, character.getLightLevel());
            contentValues.put(CharacterContract.CharacterEntry.COLUMN_NAME_CLASS_HASH, character.getClassHash());

            sqLiteDatabase.insert(CharacterContract.CharacterEntry.TABLE_NAME, null, contentValues);
        }
    }

    public void updateCharacterClass(String characterId,
                                     String classDescription) {

        String whereClause = CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_ID + " = ?";

        ContentValues contentValues = new ContentValues(1);
        contentValues.put(CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_CLASS, classDescription);

        sqLiteDatabase.update(CharacterContract.CharacterEntry.TABLE_NAME,
                              contentValues,
                              whereClause,
                              new String[] {characterId});
    }

    public void deleteCharacterInfo() {
        sqLiteDatabase.delete(CharacterContract.CharacterEntry.TABLE_NAME, null, null);
    }

    public String getMembershipId() {

        try (Cursor cursor = sqLiteDatabase.query(AccountContract.AccountEntry.TABLE_NAME,
                                                  new String[] { AccountContract.AccountEntry.COLUMN_NAME_MEMBERSHIP_ID },
                                                  null,
                                                  null,
                                                  null,
                                                  null,
                                                  null)) {

            String membershipId = "";

            if (cursor.moveToNext()) {
                membershipId = cursor.getString(cursor.getColumnIndexOrThrow(AccountContract.AccountEntry.COLUMN_NAME_MEMBERSHIP_ID));
            }

            return membershipId;
        }
    }

    public Double getMembershipType(String memberShipId) {

        try (Cursor cursor = sqLiteDatabase.query(AccountContract.AccountEntry.TABLE_NAME,
                                                  new String[] { AccountContract.AccountEntry.COLUMN_NAME_MEMBERSHIP_TYPE },
                                                  AccountContract.AccountEntry.COLUMN_NAME_MEMBERSHIP_ID + " = ?",
                                                  new String[] { memberShipId },
                                                  null,
                                                  null,
                                                  null)) {

            Double membershipType = 0D;

            if (cursor.moveToNext()) {
                membershipType = cursor.getDouble(cursor.getColumnIndexOrThrow(AccountContract.AccountEntry.COLUMN_NAME_MEMBERSHIP_TYPE));
            }

            return membershipType;
        }
    }

    public List<String> getCharacterIds(String membershipId) {

        try (Cursor cursor = sqLiteDatabase.query(CharacterContract.CharacterEntry.TABLE_NAME,
                                                  new String[] { CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_ID },
                                                  CharacterContract.CharacterEntry.COLUMN_NAME_MEMBERSHIP_ID + " = ?",
                                                  new String[] { membershipId },
                                                  null,
                                                  null,
                                                  null)) {

            List<String> characterIds = new ArrayList<>();

            while (cursor.moveToNext()) {
                characterIds.add(cursor.getString(cursor.getColumnIndexOrThrow(CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_ID)));
            }

            return characterIds;
        }
    }

    public List<Character> getCharacters() {

        List<Character> characters = Lists.newArrayList();

        try (Cursor cursor = sqLiteDatabase.query(false,
                                                  CharacterContract.CharacterEntry.TABLE_NAME,
                                                  null,
                                                  null,
                                                  null,
                                                  null,
                                                  null,
                                                  null,
                                                  null)) {

            while (cursor.moveToNext()) {
                Character character = new Character();
                character.setCharacterId(cursor.getString(cursor.getColumnIndexOrThrow(CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_ID)));
                character.setEmblemPath(cursor.getString(cursor.getColumnIndexOrThrow(CharacterContract.CharacterEntry.COLUMN_NAME_EMBLEM_PATH)));
                character.setBackgroundPath(cursor.getString(cursor.getColumnIndexOrThrow(CharacterContract.CharacterEntry.COLUMN_NAME_BACKGROUND_PATH)));
                character.setLightLevel(cursor.getInt(cursor.getColumnIndexOrThrow(CharacterContract.CharacterEntry.COLUMN_NAME_LIGHT_LEVEL)));
                character.setClassHash(cursor.getLong(cursor.getColumnIndexOrThrow(CharacterContract.CharacterEntry.COLUMN_NAME_CLASS_HASH)));
                ClassInformation classInformation = new ClassInformation();
                classInformation.setClassName(cursor.getString(cursor.getColumnIndexOrThrow(CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_CLASS)));
                character.setCharacterClass(classInformation);
                character.setCharacterIndex(cursor.getInt(cursor.getColumnIndexOrThrow(CharacterContract.CharacterEntry.COLUMN_CHARACTER_INDEX)));
                characters.add(character);
            }

        }

        return characters;
    }

    public Map<String, String> getCharacterEmblemPaths() {

        Map<String, String> emblemPathMap = new HashMap<>();

        try (Cursor cursor = sqLiteDatabase.query(false,
                                                  CharacterContract.CharacterEntry.TABLE_NAME,
                                                  null,
                                                  null,
                                                  null,
                                                  null,
                                                  null,
                                                  null,
                                                  null)) {

            while (cursor.moveToNext()) {
                String characterId = cursor.getString(cursor.getColumnIndexOrThrow(CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_ID));
                String emblemPath = cursor.getString(cursor.getColumnIndexOrThrow(CharacterContract.CharacterEntry.COLUMN_NAME_EMBLEM_PATH));
                emblemPathMap.put(characterId, emblemPath);
            }
        }

        return emblemPathMap;
    }

    public String getClass(Long classHash) {

        String whereClause = "id + 4294967296 = " + classHash + " OR id = " + classHash;

        try (Cursor cursor = manifestDatabase.query(ManifestTables.DESTINY_CLASS_DEFINITION,
                                                    new String[] {ManifestTables.COLUMN_NAME_JSON},
                                                    whereClause,
                                                    null,
                                                    null,
                                                    null,
                                                    null)) {

            if (cursor.moveToFirst()) {
                String json = cursor.getString(cursor.getColumnIndex(ManifestTables.COLUMN_NAME_JSON));
                ManifestClass manifestClass = gson.fromJson(json, ManifestClass.class);

                return manifestClass.getDisplayProperties().getName();
            }

        }

        return "";
    }
}
