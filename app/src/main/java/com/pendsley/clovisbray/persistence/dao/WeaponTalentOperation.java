package com.pendsley.clovisbray.persistence.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.gson.Gson;
import com.pendsley.clovisbray.inject.DatabaseModule;
import com.pendsley.clovisbray.model.WeaponTalentNodes;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * TODO
 */
public class WeaponTalentOperation {

    private final SQLiteDatabase sqLiteDatabase;
    private final Gson gson;

    @Inject
    public WeaponTalentOperation(@Named(DatabaseModule.MANIFEST_DATABASE) SQLiteDatabase sqLiteDatabase,
                                 Gson gson) {
        this.sqLiteDatabase = sqLiteDatabase;
        this.gson = gson;
    }

    public WeaponTalentNodes getWeaponTalentNodes(Long talentGridHash) {
        try (Cursor cursor = sqLiteDatabase.query(ManifestTables.DESTINY_TALENT_GRID_DEFINITION,
                new String[] {ManifestTables.COLUMN_NAME_JSON},
                "id + 4294967296 = " + talentGridHash + " OR id = " + talentGridHash,
                null,
                null,
                null,
                null)) {

            WeaponTalentNodes weaponTalentNodes = null;

            if (cursor.moveToFirst()) {
                String json = cursor.getString(cursor.getColumnIndex(ManifestTables.COLUMN_NAME_JSON));
                weaponTalentNodes = gson.fromJson(json, WeaponTalentNodes.class);
            }

            return weaponTalentNodes;
        }
    }
}
