package com.pendsley.clovisbray.persistence.datastore;

import android.support.annotation.Nullable;

import com.google.common.base.Strings;
import com.pendsley.clovisbray.model.ItemSlot;
import com.pendsley.clovisbray.model.Loadout;
import com.pendsley.clovisbray.model.LocalInventoryItem;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.annotations.NonNull;

/**
 * Caches the current loadout being edited.
 *
 * @author Phil Endsley
 */
@Singleton
public class LoadoutEditingCache {

    private Loadout loadout;

    @Inject
    LoadoutEditingCache() {

    }

    public Loadout getLoadout() {
        return loadout;
    }

    public void setLoadout(Loadout loadout) {
        this.loadout = loadout;
    }

    public Flowable<List<ItemSlot>> getAvailableSlotsFlowable() {
        return Flowable.create(new FlowableOnSubscribe<List<ItemSlot>>() {
            @Override
            public void subscribe(@NonNull FlowableEmitter<List<ItemSlot>> emitter) throws Exception {
                List<ItemSlot> availableSlots = new ArrayList<>();

                if (Strings.isNullOrEmpty(loadout.getPrimaryItemInstanceId())) {
                    availableSlots.add(ItemSlot.KINETIC);
                }
                if (Strings.isNullOrEmpty(loadout.getSpecialItemInstanceId())) {
                    availableSlots.add(ItemSlot.ENERGY);
                }
                if (Strings.isNullOrEmpty(loadout.getHeavyItemInstanceId())) {
                    availableSlots.add(ItemSlot.POWER);
                }
//                if (Strings.isNullOrEmpty(loadout.getGhostItemInstanceId())) {
//                    availableSlots.add(ItemSlot.GHOST);
//                }
//                if (Strings.isNullOrEmpty(loadout.getHelmetItemInstanceId())) {
//                    availableSlots.add(ItemSlot.HELMET);
//                }
//                if (Strings.isNullOrEmpty(loadout.getGauntletsItemInstanceId())) {
//                    availableSlots.add(ItemSlot.GAUNTLETS);
//                }
//                if (Strings.isNullOrEmpty(loadout.getChestItemInstanceId())) {
//                    availableSlots.add(ItemSlot.CHEST);
//                }
//                if (Strings.isNullOrEmpty(loadout.getLegsItemInstanceId())) {
//                    availableSlots.add(ItemSlot.LEGS);
//                }
//                if (Strings.isNullOrEmpty(loadout.getClassItemItemInstanceId())) {
//                    availableSlots.add(ItemSlot.CLASS);
//                }

                emitter.onNext(availableSlots);
                emitter.onComplete();
            }
        }, BackpressureStrategy.BUFFER);
    }

    public void setLoadoutSlot(LocalInventoryItem item) {
        String itemInstanceId = item.getInstanceId();
        ItemSlot itemSlot = ItemSlot.fromDescription(item.getItemType());

        if (itemSlot == null) {
            return;
        }

        setItemSlotInstanceId(itemSlot, itemInstanceId);
    }

    public void clearLoadoutSlot(ItemSlot itemSlot) {
        setItemSlotInstanceId(itemSlot, null);
    }

    public void expireCache() {
        loadout = null;
    }

    private void setItemSlotInstanceId(ItemSlot itemSlot,
                                       @Nullable String itemInstanceId) {

        switch (itemSlot) {
            case KINETIC:
                loadout.setPrimaryItemInstanceId(itemInstanceId);
                break;
            case ENERGY:
                loadout.setSpecialItemInstanceId(itemInstanceId);
                break;
            case POWER:
                loadout.setHeavyItemInstanceId(itemInstanceId);
                break;
            case GHOST:
                loadout.setGhostItemInstanceId(itemInstanceId);
                break;
            case HELMET:
                loadout.setHelmetItemInstanceId(itemInstanceId);
                break;
            case GAUNTLETS:
                loadout.setGauntletsItemInstanceId(itemInstanceId);
                break;
            case CHEST:
                loadout.setChestItemInstanceId(itemInstanceId);
                break;
            case LEGS:
                loadout.setLegsItemInstanceId(itemInstanceId);
                break;
            case CLASS:
                loadout.setClassItemItemInstanceId(itemInstanceId);
                break;
        }
    }

}
