package com.pendsley.clovisbray.persistence.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.gson.Gson;
import com.pendsley.clovisbray.inject.DatabaseModule;
import com.pendsley.clovisbray.model.Faction;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * DAO for faction data.
 *
 * @author Phil Endsley
 */
public class FactionDao {

    private final SQLiteDatabase manifestDatabase;
    private final Gson gson;

    @Inject
    FactionDao(@Named(DatabaseModule.MANIFEST_DATABASE) SQLiteDatabase manifestDatabase,
               Gson gson) {
        this.manifestDatabase = manifestDatabase;
        this.gson = gson;
    }

    public Faction getFaction(Long factionHash) {
        String whereClause = "id + 4294967296 = " + factionHash + " OR id = " + factionHash;

        try (Cursor cursor = manifestDatabase.query(false,
                                                    ManifestTables.DESTINY_FACTION_DEFINITION,
                                                    new String[] {ManifestTables.COLUMN_NAME_JSON},
                                                    whereClause,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null)) {

            if (cursor.moveToNext()) {
                String json = cursor.getString(cursor.getColumnIndex(ManifestTables.COLUMN_NAME_JSON));
                return gson.fromJson(json, Faction.class);
            }

            return null;
        }
    }
}
