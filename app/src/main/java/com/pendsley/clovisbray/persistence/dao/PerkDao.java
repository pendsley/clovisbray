package com.pendsley.clovisbray.persistence.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.common.collect.Lists;
import com.pendsley.clovisbray.inject.DatabaseModule;
import com.pendsley.clovisbray.model.ManifestPerk;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Database operations for talent nodes.
 *
 * @author Phil Endsley
 */
public class PerkDao {

    private final SQLiteDatabase sqLiteDatabase;

    @Inject
    PerkDao(@Named(DatabaseModule.WRITABLE_DATABASE) SQLiteDatabase sqLiteDatabase) {
        this.sqLiteDatabase = sqLiteDatabase;
    }

    public void deleteAllPerks() {
        sqLiteDatabase.delete(TalentNodeContract.TalentNodeEntry.TABLE_NAME, null, null);
    }

    public void deletePerks(Collection<String> itemIds) {

        sqLiteDatabase.delete(TalentNodeContract.TalentNodeEntry.TABLE_NAME,
                TalentNodeContract.TalentNodeEntry.COLUMN_NAME_ITEM_INSTANCE_ID + " IN " + generateArguments(itemIds.size()),
                itemIds.toArray(new String[itemIds.size()]));

    }

    public void batchInsertPerks(Map<String, List<ManifestPerk>> talentNodesMap) {

        sqLiteDatabase.beginTransaction();
        for (Map.Entry<String, List<ManifestPerk>> entry : talentNodesMap.entrySet()) {
            for (ManifestPerk perk : entry.getValue()) {

                ContentValues contentValues = new ContentValues(5);
                contentValues.put(TalentNodeContract.TalentNodeEntry.COLUMN_NAME_ITEM_INSTANCE_ID, entry.getKey());
                contentValues.put(TalentNodeContract.TalentNodeEntry.COLUMN_NAME_NODE_STEP_NAME, perk.getDisplayProperties().getName());
                contentValues.put(TalentNodeContract.TalentNodeEntry.COLUMN_NAME_NODE_STEP_DESCRIPTION, perk.getDisplayProperties().getDescription());
                contentValues.put(TalentNodeContract.TalentNodeEntry.COLUMN_NAME_NODE_ICON, perk.getDisplayProperties().getIcon());
                contentValues.put(TalentNodeContract.TalentNodeEntry.COLUMN_NAME_STEP_INDEX, 0);//TODO perk.getStepIndex());
                contentValues.put(TalentNodeContract.TalentNodeEntry.COLUMN_ROW, 0);//TODO perk.getRow());
                contentValues.put(TalentNodeContract.TalentNodeEntry.COLUMN_COLUMN, 0);//TODO perk.getColumn());


                sqLiteDatabase.insert(TalentNodeContract.TalentNodeEntry.TABLE_NAME,
                        null,
                        contentValues);
            }
        }
        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();
    }

    public List<ManifestPerk> getTalentNodes(String itemInstanceId) {

        List<ManifestPerk> perks = Lists.newArrayList();
        String whereClause = TalentNodeContract.TalentNodeEntry.COLUMN_NAME_ITEM_INSTANCE_ID + " = " + itemInstanceId;
        try (Cursor cursor = sqLiteDatabase.query(false,
                TalentNodeContract.TalentNodeEntry.TABLE_NAME,
                null,
                whereClause,
                null,
                null,
                null,
                null,
                null)) {

            while (cursor.moveToNext()) {
                ManifestPerk perk = new ManifestPerk();
                perk.setName(cursor.getString(cursor.getColumnIndexOrThrow(TalentNodeContract.TalentNodeEntry.COLUMN_NAME_NODE_STEP_NAME)));
                perk.setDescription(cursor.getString(cursor.getColumnIndexOrThrow(TalentNodeContract.TalentNodeEntry.COLUMN_NAME_NODE_STEP_DESCRIPTION)));
                perk.setIcon(cursor.getString(cursor.getColumnIndexOrThrow(TalentNodeContract.TalentNodeEntry.COLUMN_NAME_NODE_ICON)));
//                perk.setStepIndex(cursor.getInt(cursor.getColumnIndexOrThrow(TalentNodeContract.TalentNodeEntry.COLUMN_NAME_STEP_INDEX)));
//                perk.setRow(cursor.getInt(cursor.getColumnIndexOrThrow(TalentNodeContract.TalentNodeEntry.COLUMN_ROW)));
//                perk.setColumn(cursor.getInt(cursor.getColumnIndexOrThrow(TalentNodeContract.TalentNodeEntry.COLUMN_COLUMN)));

                perks.add(perk);
            }
        }

        return perks;
    }


    private static String generateArguments(int size) {
        StringBuilder arguments = new StringBuilder("(");
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                arguments.append(",");
            }
            arguments.append("?");
        }
        arguments.append(")");

        return arguments.toString();
    }
}
