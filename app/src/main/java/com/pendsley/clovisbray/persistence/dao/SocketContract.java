package com.pendsley.clovisbray.persistence.dao;

import android.provider.BaseColumns;

/**
 * DB table and column names for sockets.
 *
 * @author Phil Endsley
 */
public class SocketContract {

    private SocketContract() {
    }

    public static class SocketEntry implements BaseColumns {
        public static final String TABLE_NAME = "sockets";
        public static final String COLUMN_ITEM_INSTANCE_ID = "instance_id";
        public static final String COLUMN_SOCKET_HASH = "socket_hash";
        public static final String COLUMN_COLUMN_ORDER = "column_order";
        public static final String COLUMN_ROW_ORDER = "row_order";
        public static final String COLUMN_ENABLED = "enabled";
    }
}
