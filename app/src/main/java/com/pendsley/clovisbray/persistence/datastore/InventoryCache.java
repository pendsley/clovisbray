package com.pendsley.clovisbray.persistence.datastore;

import android.support.annotation.Nullable;

import com.pendsley.clovisbray.model.ItemClassType;
import com.pendsley.clovisbray.model.LocalInventoryItem;
import com.pendsley.clovisbray.operation.InventoryLookupOperation;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.annotations.NonNull;
import io.reactivex.subscribers.DisposableSubscriber;

/**
 * A cache of the inventory list. If the lookup parameters are changed, the cache is expired and
 * rebuilt.
 *
 * @author Phil Endsley
 */
@Singleton
public class InventoryCache {

    private final InventoryLookupOperation inventoryLookupOperation;

    private String currentItemCategory;
    private ItemClassType currentItemClassType;

    private List<LocalInventoryItem> inventoryItems;

    @Inject
    InventoryCache(InventoryLookupOperation inventoryLookupOperation) {
        this.inventoryLookupOperation = inventoryLookupOperation;
    }

    public Flowable<List<LocalInventoryItem>> getInventoryListFlowableByCategory(String itemCategory) {

        // Requesting the same data, return what's cached
        if (itemCategory.equals(currentItemCategory)) {
            return Flowable.just(inventoryItems);
        }

        // Clear out what we had and lookup what we need
        expireCache();
        currentItemCategory = itemCategory;
        Flowable<List<LocalInventoryItem>> flowable =
                inventoryLookupOperation.getLookupByCategoryFlowable(itemCategory);

        flowable.subscribeWith(new InventoryCacheSubscriber());
        return flowable;
    }

    public Flowable<List<LocalInventoryItem>> getInventoryListFlowableByClassType(ItemClassType itemClassType) {

        if (currentItemClassType == itemClassType) {
            return Flowable.create(new FlowableOnSubscribe<List<LocalInventoryItem>>() {
                @Override
                public void subscribe(@NonNull FlowableEmitter<List<LocalInventoryItem>> emitter) throws Exception {
                    emitter.onNext(inventoryItems);
                    emitter.onComplete();
                }
            }, BackpressureStrategy.BUFFER);
        }

        expireCache();
        currentItemClassType = itemClassType;
        Flowable<List<LocalInventoryItem>> flowable =
                inventoryLookupOperation.getLookupByClassTypeFlowable(itemClassType);

        flowable.subscribeWith(new InventoryCacheSubscriber());
        return flowable;
    }

    public void updateItem(@Nullable LocalInventoryItem updatedItem) {
        if (updatedItem == null) {
            return;
        }

        inventoryItems.remove(updatedItem);
        inventoryItems.add(updatedItem);
    }

    public void expireCache() {
        inventoryItems = null;
        currentItemCategory = null;
        currentItemClassType = null;
    }

    private void loadingFinished(List<LocalInventoryItem> inventoryItems) {
        this.inventoryItems = inventoryItems;
    }

    private final class InventoryCacheSubscriber extends DisposableSubscriber<List<LocalInventoryItem>> {

        @Override
        public void onNext(List<LocalInventoryItem> inventoryItems) {
            loadingFinished(inventoryItems);
        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onComplete() {

        }
    }
}
