package com.pendsley.clovisbray.persistence.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.pendsley.clovisbray.inject.DatabaseModule;
import com.pendsley.clovisbray.model.ManifestStat;
import com.pendsley.clovisbray.model.Stat;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Database operations for item stats.
 *
 * @author Phil Endsley
 */
public class ItemStatsDao {

    private static final String TAG = "ItemStatsDao";

    private final SQLiteDatabase sqLiteDatabase;
    private final SQLiteDatabase manifestDatabase;
    private final Gson gson;

    @Inject
    public ItemStatsDao(@Named(DatabaseModule.WRITABLE_DATABASE)SQLiteDatabase sqLiteDatabase,
                        @Named(DatabaseModule.MANIFEST_DATABASE) SQLiteDatabase manifestDatabase,
                        Gson gson) {
        this.sqLiteDatabase = sqLiteDatabase;
        this.manifestDatabase = manifestDatabase;
        this.gson = gson;
    }

    public void batchInsertStats(Map<String, List<Stat>> statsMap) {

        sqLiteDatabase.beginTransaction();
        for (Map.Entry<String, List<Stat>> entry : statsMap.entrySet()) {
            for (Stat stat : entry.getValue()) {

                Long statHash = stat.getStatHash();
                ManifestStat manifestStat = getManifestStat(statHash);
                if (manifestStat == null) {
                    Log.e(TAG, "Stat not found in manifest for hash " + statHash);
                    continue;
                }

                ContentValues contentValues = new ContentValues(6);
                contentValues.put(StatContract.StatEntry.COLUMN_INSTANCE_ITEM_ID, entry.getKey());
                contentValues.put(StatContract.StatEntry.COLUMN_NAME, manifestStat.getDisplayProperties().getName());
                contentValues.put(StatContract.StatEntry.COLUMN_VALUE, stat.getValue());
                contentValues.put(StatContract.StatEntry.COLUMN_MINIMUM_VALUE, stat.getMinimum());
                contentValues.put(StatContract.StatEntry.COLUMN_MAXIMUM_VALUE, stat.getMaximumValue());

                sqLiteDatabase.insert(StatContract.StatEntry.TABLE_NAME, null, contentValues);
            }
        }

        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();
    }

    public List<Stat> getStatsForInstanceId(String instanceId) {

        List<Stat> stats = Lists.newArrayList();

        String whereClause = StatContract.StatEntry.COLUMN_INSTANCE_ITEM_ID + " = " + instanceId;
        try (Cursor cursor = sqLiteDatabase.query(false,
                StatContract.StatEntry.TABLE_NAME,
                null,
                whereClause,
                null,
                null,
                null,
                null,
                null)) {

            while (cursor.moveToNext()) {

                Stat stat = new Stat();
                stat.setName(cursor.getString(cursor.getColumnIndexOrThrow(StatContract.StatEntry.COLUMN_NAME)));
                stat.setValue(cursor.getInt(cursor.getColumnIndexOrThrow(StatContract.StatEntry.COLUMN_VALUE)));
                stat.setMinimum(cursor.getInt(cursor.getColumnIndexOrThrow(StatContract.StatEntry.COLUMN_MINIMUM_VALUE)));
                stat.setMaximumValue(cursor.getInt(cursor.getColumnIndexOrThrow(StatContract.StatEntry.COLUMN_MAXIMUM_VALUE)));
                stats.add(stat);
            }
        }

        return stats;
    }

    public void deleteAllStats() {
        sqLiteDatabase.delete(StatContract.StatEntry.TABLE_NAME, null, null);
    }

    public void deleteStats(Collection<String> itemIds) {

        sqLiteDatabase.delete(StatContract.StatEntry.TABLE_NAME,
                StatContract.StatEntry.COLUMN_INSTANCE_ITEM_ID + " IN " + generateArguments(itemIds.size()),
                itemIds.toArray(new String[itemIds.size()]));

    }

    @Nullable
    private ManifestStat getManifestStat(Long statHash) {

        String whereClause = "id + 4294967296 = " + statHash + " OR id = " + statHash;

        try (Cursor cursor = manifestDatabase.query(false,
                ManifestTables.DESTINY_STAT_DEFINITION,
                new String[] {ManifestTables.COLUMN_NAME_JSON},
                whereClause,
                null,
                null,
                null,
                null,
                null)) {

            if (cursor.moveToNext()) {
                String json = cursor.getString(cursor.getColumnIndex(ManifestTables.COLUMN_NAME_JSON));
                return gson.fromJson(json, ManifestStat.class);
            }

            return null;
        }
    }

    private static String generateArguments(int size) {
        StringBuilder arguments = new StringBuilder("(");
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                arguments.append(",");
            }
            arguments.append("?");
        }
        arguments.append(")");

        return arguments.toString();
    }
}
