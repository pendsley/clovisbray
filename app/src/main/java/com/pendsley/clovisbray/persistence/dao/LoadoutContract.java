package com.pendsley.clovisbray.persistence.dao;

import android.provider.BaseColumns;

/**
 * TODO
 */
public class LoadoutContract {

    private LoadoutContract() {
    }

    public static class LoadoutEntry implements BaseColumns {
        public static final String TABLE_NAME = "loadouts";
        public static final String COLUMN_LOADOUT_NAME = "name";
        public static final String COLUMN_LOADOUT_CHARACTER_ID = "character_id";
        public static final String COLUMN_LOADOUT_PRIMARY_INSTANCE = "primary_instance";
        public static final String COLUMN_LOADOUT_SPECIAL_INSTANCE = "special_instance";
        public static final String COLUMN_LOADOUT_HEAVY_INSTANCE = "heavy_instance";
        public static final String COLUMN_LOADOUT_GHOST_INSTANCE = "ghost_instance";
        public static final String COLUMN_LOADOUT_HELMET_INSTANCE = "helmet_instance";
        public static final String COLUMN_LOADOUT_GAUNTLETS_INSTANCE = "gauntlets_instance";
        public static final String COLUMN_LOADOUT_CHEST_INSTANCE = "chest_instance";
        public static final String COLUMN_LOADOUT_LEGS_INSTANCE = "legs_instance";
        public static final String COLUMN_LOADOUT_CLASS_ITEM_INSTANCE = "class_item_instance";
        public static final String COLUMN_LOADOUT_ARTIFACT_INSTANCE = "artifact_instance";
    }
}
