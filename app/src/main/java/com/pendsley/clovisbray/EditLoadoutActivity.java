package com.pendsley.clovisbray;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.pendsley.clovisbray.authentication.ReinjectFieldsEvent;
import com.pendsley.clovisbray.model.LocalInventoryItem;
import com.pendsley.clovisbray.persistence.datastore.LoadoutEditingCache;
import com.pendsley.clovisbray.util.BusProvider;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

/**
 * Activity for modifying character loadouts.
 *
 * @author Phil Endsley
 */
public class EditLoadoutActivity extends AppCompatActivity implements LoadoutDetailsFragment.LoadoutSlotSelectedListener,
                                                                      LoadoutDetailsFragment.AddLoadoutSlotListener,
                                                                      InventoryListFragment.ItemSelectedListener {

    public static int REQUEST_CODE = 1;

    public static final String STATE_LOADOUT = "loadout";

    @Inject
    LoadoutEditingCache loadoutEditingCache;

    @Inject
    BusProvider busProvider;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectMembers();

        ((ClovisBrayApplication) getApplication()).getApiComponent().inject(this);

        setContentView(R.layout.activity_create_loadout);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
    }

    @Override
    public void onResume() {
        super.onResume();
        busProvider.getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        busProvider.getBus().unregister(this);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.create_loadout_container, new LoadoutDetailsFragment(), LoadoutDetailsFragment.TAG)
                    .commit();
        }
    }

    @Override
    public void onSlotSelected(String slotType) {

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.create_loadout_container, InventoryListFragment.create(slotType, null), InventoryListFragment.TAG)
                .addToBackStack(InventoryListFragment.TAG)
                .commit();
    }

    @Override
    public void onAddLoadoutSlot() {

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.create_loadout_container, new NewLoadoutSlotSelectionFragment(), NewLoadoutSlotSelectionFragment.TAG)
                .addToBackStack(NewLoadoutSlotSelectionFragment.TAG)
                .commit();
    }

    @Override
    public void onItemSelected(LocalInventoryItem selectedItem) {

        loadoutEditingCache.setLoadoutSlot(selectedItem);

        getSupportFragmentManager().popBackStack(NewLoadoutSlotSelectionFragment.TAG,
                                                 FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Subscribe
    public void onRefreshToken(ReinjectFieldsEvent event) {
        injectMembers();
    }

    private void injectMembers() {
        ((ClovisBrayApplication) getApplication()).getApiComponent().inject(this);
    }
}
