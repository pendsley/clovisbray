package com.pendsley.clovisbray.network;

import com.pendsley.clovisbray.inject.AuthenticationModule;

import java.io.IOException;
import java.util.Date;

import javax.inject.Named;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Adds the API key and authorization headers to every web call.
 *
 * @author Phil Endsley
 */
public class BungieApiHeaderInterceptor implements Interceptor {

    private final String accessToken;
    private final Long accessTokenExpiration;

    public BungieApiHeaderInterceptor(@Named(AuthenticationModule.ACCESS_TOKEN) String accessToken,
                                      @Named(AuthenticationModule.ACCESS_TOKEN_EXPIRATION_TIME) Long accessTokenExpiration) {
        this.accessToken = accessToken;
        this.accessTokenExpiration = accessTokenExpiration;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request originalRequest = chain.request();

        Request.Builder requestBuilder = originalRequest.newBuilder()
                .header("X-API-Key", "0fb9a98bdaab45519d2fedff4b8db874")
                .method(originalRequest.method(), originalRequest.body());

        // Only if we have it
        if (accessToken != null && !accessToken.isEmpty() && new Date().getTime() < accessTokenExpiration) {
            requestBuilder.header("Authorization", "Bearer " + accessToken);
        }

        return chain.proceed(requestBuilder.build());
    }
}
