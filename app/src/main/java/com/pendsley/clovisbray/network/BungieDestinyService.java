package com.pendsley.clovisbray.network;

import com.pendsley.clovisbray.model.AccountSummaryWrapper;
import com.pendsley.clovisbray.model.AllItemsSummaryWrapper;
import com.pendsley.clovisbray.model.BungieResponse;
import com.pendsley.clovisbray.model.EquipItemArguments;
import com.pendsley.clovisbray.model.TransferItemArguments;
import com.pendsley.clovisbray.model.ItemDetailsResponseWrapper;
import com.pendsley.clovisbray.model.Manifest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * TODO
 */
public interface BungieDestinyService {

    String BASE_URL = "https://bungie.net";

    @GET("Manifest/")
    Call<BungieResponse<Manifest>> getManifest();


    @GET("{membershipType}/Account/{destinyMembershipId}/Summary/")
    Call<BungieResponse<AccountSummaryWrapper>> getAccountSummary(@Path("membershipType") int membershipType,
                                                                  @Path("destinyMembershipId") String destinyMembershipId);

    @GET("{membershipType}/Account/{destinyMembershipId}/Items/")
    Call<BungieResponse<AllItemsSummaryWrapper>> getAllItemsSummary(@Path("membershipType") int membershipType,
                                                                    @Path("destinyMembershipId") String membershipId);

    @GET("{membershipType}/Account/{destinyMembershipId}/Character/{characterId}/Inventory/{itemInstanceId}/")
    Call<BungieResponse<ItemDetailsResponseWrapper>> getItemDetail(@Path("membershipType") int membershipType,
                                                                   @Path("destinyMembershipId") String destinyMembershipId,
                                                                   @Path("characterId") String characterId,
                                                                   @Path("itemInstanceId") String itemInstanceId);

    @POST("TransferItem/")
    Call<BungieResponse<Object>> transferItem(@Body TransferItemArguments transferItemArguments);

    @POST("EquipItem/")
    Call<BungieResponse<Object>> equipItem(@Body EquipItemArguments equipItemArguments);
}
