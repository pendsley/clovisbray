package com.pendsley.clovisbray.network;

import com.pendsley.clovisbray.model.BungieAccountData;
import com.pendsley.clovisbray.model.BungieResponse;
import com.pendsley.clovisbray.model.UserMemberships;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * TODO
 */
public interface BungieUserService {

//    @GET("GetCurrentBungieAccount/")
//    Call<BungieResponse<BungieAccountData>> getCurrentBungieAccount();

    @GET("GetMembershipsForCurrentUser/")
    Call<BungieResponse<UserMemberships>> getMembershipData();

}
