package com.pendsley.clovisbray.network;

import com.pendsley.clovisbray.model.BungieResponse;
import com.pendsley.clovisbray.model.CharacterProfileResponse;
import com.pendsley.clovisbray.model.EquipItemArguments;
import com.pendsley.clovisbray.model.ItemResponse;
import com.pendsley.clovisbray.model.Manifest;
import com.pendsley.clovisbray.model.PostmasterTransferRequest;
import com.pendsley.clovisbray.model.ProfileResponse;
import com.pendsley.clovisbray.model.TransferItemArguments;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Bungie's Destiny 2 API.
 *
 * @author Phil Endsley
 */
public interface BungieDestiny2Service {

    String BASE_URL = "https://bungie.net";

    @GET("Manifest/")
    Call<BungieResponse<Manifest>> getManifest();

    @GET("{membershipType}/Profile/{membershipId}/")
    Call<BungieResponse<ProfileResponse>> getProfile(@Path("membershipType") int membershipType,
                                                     @Path("membershipId") String membershipId,
                                                     @Query("components") String components);


    @GET("{membershipType}/Profile/{membershipId}/")
    Call<BungieResponse<Object>> getProfilejson(@Path("membershipType") int membershipType,
                                                @Path("membershipId") String membershipId,
                                                @Query("components") String components);

    @GET("{membershipType}/Profile/{membershipId}/Character/{characterId}/")
    Call<BungieResponse<CharacterProfileResponse>> getCharacterProfile(@Path("membershipType") int membershipType,
                                                                       @Path("membershipId") String membershipId,
                                                                       @Path("characterId") String characterId,
                                                                       @Query("components") String components);

    @GET("{membershipType}/Profile/{membershipId}/Item/{itemInstanceId}/")
    Call<BungieResponse<ItemResponse>> getItem(@Path("membershipType") int membershipType,
                                               @Path("membershipId") String membershipId,
                                               @Path("itemInstanceId") String itemInstanceId,
                                               @Query("components") String components);

    @POST("Actions/Items/TransferItem/")
    Call<BungieResponse<Object>> transferItem(@Body TransferItemArguments transferItemArguments);

    @POST("Actions/Items/EquipItem/")
    Call<BungieResponse<Object>> equipItem(@Body EquipItemArguments equipItemArguments);

    @POST("Actions/Items/PullFromPostmaster/")
    Call<BungieResponse<Object>> transferFromPostmaster(@Body PostmasterTransferRequest postmasterTransferRequest);
}
