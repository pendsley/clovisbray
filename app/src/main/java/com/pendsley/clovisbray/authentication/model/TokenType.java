package com.pendsley.clovisbray.authentication.model;

/**
 * The different OAuth token types.
 *
 * @author Phil Endsley
 */
public enum TokenType {
    ACCESS,
    REFRESH
}
