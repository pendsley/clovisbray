package com.pendsley.clovisbray.authentication.persistence.operation;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.pendsley.clovisbray.authentication.model.TokenType;
import com.pendsley.clovisbray.inject.DatabaseModule;
import com.pendsley.clovisbray.authentication.persistence.TokenContract;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Database operations for getting OAuth tokens.
 *
 * @author Phil Endsley
 */
public class AuthenticationTokenOperation {

    private static final String SELECTION = TokenContract.TokenEntry.COLUMN_NAME_TYPE + " = ?";

    private final SQLiteDatabase database;

    @Inject
    public AuthenticationTokenOperation(@Named(DatabaseModule.READABLE_DATABASE) SQLiteDatabase database) {
        this.database = database;
    }

    public String getTokenByType(TokenType tokenType) {

        try (Cursor cursor = database.query(TokenContract.TokenEntry.TABLE_NAME,
                new String[] { TokenContract.TokenEntry.COLUMN_NAME_TOKEN },
                SELECTION,
                new String[] { tokenType.name() },
                null,
                null,
                null)) {

            String token = "";

            if (cursor.moveToNext()) {
                token = cursor.getString(cursor.getColumnIndexOrThrow(TokenContract.TokenEntry.COLUMN_NAME_TOKEN));
            }

            return token;
        }
    }

    public Long getTokenExpirationTimeByType(TokenType tokenType) {

        try (Cursor cursor = database.query(TokenContract.TokenEntry.TABLE_NAME,
                new String[] { TokenContract.TokenEntry.COLUMN_NAME_EXPIRES },
                SELECTION,
                new String[] { tokenType.name() },
                null,
                null,
                null)) {

            Long tokenExpiration = -1L;

            if (cursor.moveToNext()) {
                tokenExpiration = cursor.getLong(cursor.getColumnIndexOrThrow(TokenContract.TokenEntry.COLUMN_NAME_EXPIRES));
            }

            return tokenExpiration;
        }
    }
}
