package com.pendsley.clovisbray.authentication;

import com.pendsley.clovisbray.authentication.model.AccessTokenResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Services for authenticating with Bungie.
 *
 * @author Phil Endsley
 */
public interface BungieAuthenticationService {

    @FormUrlEncoded
    @POST("oauth/token/")
    Call<AccessTokenResponse> getAccessTokens(@Field("grant_type") String grantType,
                                              @Field("code") String authorizationCode,
                                              @Field("client_id") String clientId,
                                              @Field("client_secret") String clientSecret);

    @FormUrlEncoded
    @POST("oauth/token/")
    Call<AccessTokenResponse> getAccessTokensFromRefreshToken(@Field("grant_type") String grantType,
                                                              @Field("refresh_token") String refreshToken);
}
