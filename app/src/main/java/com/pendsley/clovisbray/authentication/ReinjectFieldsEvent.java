package com.pendsley.clovisbray.authentication;

/**
 * TODO
 */
public class ReinjectFieldsEvent {

    private boolean refreshAccessTokens;

    public ReinjectFieldsEvent() {
        refreshAccessTokens = false;
    }

    public ReinjectFieldsEvent(boolean refreshAccessTokens) {
        this.refreshAccessTokens = refreshAccessTokens;
    }

    public boolean refreshAccessTokens() {
        return refreshAccessTokens;
    }

    public void setRefreshAccessTokens(boolean refreshAccessTokens) {
        this.refreshAccessTokens = refreshAccessTokens;
    }
}
