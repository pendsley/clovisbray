package com.pendsley.clovisbray.authentication;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.common.base.Strings;
import com.pendsley.clovisbray.ClovisBrayApplication;
import com.pendsley.clovisbray.InitialSyncActivity;
import com.pendsley.clovisbray.PlatformSelectionActivity;
import com.pendsley.clovisbray.R;
import com.pendsley.clovisbray.authentication.model.AccessTokenResponse;
import com.pendsley.clovisbray.authentication.model.TokenType;
import com.pendsley.clovisbray.authentication.persistence.operation.AuthenticationTokenOperation;
import com.pendsley.clovisbray.authentication.persistence.operation.SaveTokenOperation;
import com.pendsley.clovisbray.inject.AccountModule;
import com.pendsley.clovisbray.inject.AuthenticationModule;
import com.pendsley.clovisbray.network.BungieUserService;
import com.pendsley.clovisbray.persistence.dao.AccountOperation;
import com.pendsley.clovisbray.util.BusProvider;
import com.pendsley.clovisbray.viewcontroller.AuthenticationViewController;
import com.squareup.otto.Subscribe;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Activity to authenticate the user.
 *
 * @author Phil Endsley
 */
public class BungieAuthenticationActivity extends AppCompatActivity implements AuthenticationViewController {

    private static final String TAG = "BungieAuthActivity";

    private static final String AUTHORIZATION_REPOSNSE_HOST = "authorization";

    @Inject
    BungieAuthenticationService authenticationService;

    @Inject
    BungieUserService userService;

    @Inject
    AccountOperation accountOperation;

    @Inject
    SaveTokenOperation saveTokenOperation;

    @Inject
    AuthenticationTokenOperation authenticationTokenOperation;

    @Inject
    @Named(AuthenticationModule.REFRESH_TOKEN)
    String refreshToken;

    @Inject
    @Named(AuthenticationModule.AUTHENTICATED)
    Boolean authenticated;

    @Inject
    BusProvider busProvider;

    @Inject
    @Named(AccountModule.MEMBERSHIP_ID)
    String membershipId;

    private LinearLayout container;
    private WebView authWebView;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_authentication);

        injectFields();

        container = findViewById(R.id.authentication_container);
        authWebView = findViewById(R.id.auth_web_view);
        progressBar = findViewById(R.id.auth_progress_bar);

        authWebView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        authWebView.getSettings().setLoadWithOverviewMode(true);

        // Upon successful authentication and app authorization, the web view will redirect
        // to a hardcoded host. If that happens, intercept and handle the response.
        authWebView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                Uri uri = Uri.parse(url);
                String host = uri.getHost();
                if (AUTHORIZATION_REPOSNSE_HOST.equals(host)) {
                    handleAuthenticationResponse(uri);
                }
            }
        });

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        if (authenticated) {
            launchManifestActivity();
            return;
        }

        Uri incomingUri = getIntent().getData();

        if (incomingUri == null && !authenticated) {
            startAuthProcess();
        } else if (!authenticated) {
            handleAuthenticationResponse(incomingUri);
        }
    }

    @SuppressLint("WrongConstant") // IntentCompat flags aren't recognized as valid
    @Override
    public void launchManifestActivity() {

        Intent intent;
        if (Strings.isNullOrEmpty(membershipId)) {
            intent = new Intent(this, PlatformSelectionActivity.class);
        } else {
            intent = new Intent(this, InitialSyncActivity.class);
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }

    @Subscribe
    public void onReinjectfields(ReinjectFieldsEvent event) {
        injectFields();

        if (authenticated) {
            launchManifestActivity();
        } else {
            startAuthThroughWebView();
        }
    }

    private void startAuthProcess() {

        Long expirationTime = authenticationTokenOperation.getTokenExpirationTimeByType(TokenType.REFRESH);

        if (expirationTime > 0 && new Date().getTime() < expirationTime) {
            refreshAccessToken();
        } else {
            startAuthThroughWebView();
        }
    }

    private void refreshAccessToken() {

        authenticationService.getAccessTokensFromRefreshToken("refresh_token", refreshToken)
                .enqueue(new Callback<AccessTokenResponse>() {
                    @Override
                    public void onResponse(Call<AccessTokenResponse> call,
                                           Response<AccessTokenResponse> response) {

                        AccessTokenResponse accessTokenResponse = response.body();

                        if (accessTokenResponse != null) {

                            handleTokenResponse(accessTokenResponse);
                        } else {

                            startAuthThroughWebView();
                        }
                    }

                    @Override
                    public void onFailure(Call<AccessTokenResponse> call,
                                          Throwable t) {
                        Log.e(TAG, "Error refreshing access token", t);
                        startAuthThroughWebView();
                    }
                });
    }

    // This is Bungie's auth page. We can trust it to not hurt us
    @SuppressLint("SetJavaScriptEnabled")
    private void startAuthThroughWebView() {

        progressBar.setVisibility(View.GONE);
        authWebView.setVisibility(View.VISIBLE);

        authWebView.getSettings().setJavaScriptEnabled(true);

        authWebView.loadUrl("https://www.bungie.net/en/OAuth/Authorize/?response_type=code&client_id=14438");
    }

    private void handleAuthenticationResponse(Uri incomingUri) {
        authWebView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        String authorizationCode = incomingUri.getQueryParameter("code");

        retrieveBungieAccessToken(authorizationCode);
    }

    private void retrieveBungieAccessToken(final String authorizationCode) {

        authenticationService.getAccessTokens("authorization_code",
                authorizationCode,
                "14438",
                "RxKZXK.IbSg3-tk5n8AEF0c6.eas1GxVTJAIKieLHTM")
                .enqueue(new Callback<AccessTokenResponse>() {
                    @Override
                    public void onResponse(Call<AccessTokenResponse> call,
                                           Response<AccessTokenResponse> response) {

                        AccessTokenResponse accessTokenResponse = response.body();

                        if (accessTokenResponse != null) {

                            handleTokenResponse(accessTokenResponse);

                        } else {

                            progressBar.setVisibility(View.INVISIBLE);

                            Log.e(TAG, "Error getting access tokens. " + response.message());
                            showErrorSnackbar();
                        }
                    }

                    @Override
                    public void onFailure(Call<AccessTokenResponse> call, Throwable t) {
                        Log.e(TAG, t.getLocalizedMessage(), t);
                        showErrorSnackbar();
                    }
                });
    }

    private void showErrorSnackbar() {
        @SuppressWarnings("deprecation") // API requirements
        final Snackbar snackbar = Snackbar.make(container,
                R.string.auth_error,
                Snackbar.LENGTH_INDEFINITE)
                .setActionTextColor(getResources().getColor(R.color.secondary_text));

        snackbar.setAction(R.string.dismiss, view -> snackbar.dismiss());

        snackbar.show();
    }

    private void handleTokenResponse(AccessTokenResponse accessTokenResponse) {

        saveTokenOperation.deleteTokens();
        saveTokenOperation.insert(accessTokenResponse);

        injectFields();

        launchManifestActivity();
    }

    private void injectFields() {

        ((ClovisBrayApplication) getApplication()).getAuthenticationComponent().inject(this);
        ((ClovisBrayApplication) getApplication()).getApiComponent().inject(this);
    }

}
