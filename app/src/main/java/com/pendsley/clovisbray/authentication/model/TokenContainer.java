package com.pendsley.clovisbray.authentication.model;

/**
 * TODO
 */
public class TokenContainer {

    private String value;
    private Integer readyIn;
    private Long expires;
    private String type;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getReadyIn() {
        return readyIn;
    }

    public void setReadyIn(Integer readyIn) {
        this.readyIn = readyIn;
    }

    public Long getExpires() {
        return expires;
    }

    public void setExpires(Long expires) {
        this.expires = expires;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
