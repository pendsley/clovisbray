package com.pendsley.clovisbray.authentication.persistence.operation;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.pendsley.clovisbray.authentication.model.AccessTokenResponse;
import com.pendsley.clovisbray.authentication.model.TokenType;
import com.pendsley.clovisbray.authentication.persistence.TokenContract;
import com.pendsley.clovisbray.inject.DatabaseModule;
import com.pendsley.clovisbray.persistence.dao.InsertOperation;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Database operation for saving OAuth token data.
 *
 * @author Phil Endsley
 */
public class SaveTokenOperation implements InsertOperation<AccessTokenResponse> {

    private final SQLiteDatabase database;

    @Inject
    SaveTokenOperation(@Named(DatabaseModule.WRITABLE_DATABASE) SQLiteDatabase database) {
        this.database = database;
    }

    @Override
    public void insert(AccessTokenResponse accessTokenResponse) {

        insertAccessToken(accessTokenResponse.getAccessToken(),
                          getExpirationTime(accessTokenResponse.getExpiresIn()));

        insertRefreshToken(accessTokenResponse.getRefreshToken(),
                           getExpirationTime(accessTokenResponse.getRefreshExpiresIn()));
    }

    private void insertAccessToken(String accessToken,
                                   long expirationTime) {

        ContentValues values = new ContentValues(3);
        values.put(TokenContract.TokenEntry.COLUMN_NAME_TYPE, TokenType.ACCESS.name());
        values.put(TokenContract.TokenEntry.COLUMN_NAME_TOKEN, accessToken);
        values.put(TokenContract.TokenEntry.COLUMN_NAME_EXPIRES, expirationTime);

        database.insert(TokenContract.TokenEntry.TABLE_NAME, null, values);
    }

    private void insertRefreshToken(String refreshToken,
                                    long expirationTime) {

        ContentValues values = new ContentValues(3);
        values.put(TokenContract.TokenEntry.COLUMN_NAME_TYPE, TokenType.REFRESH.name());
        values.put(TokenContract.TokenEntry.COLUMN_NAME_TOKEN, refreshToken);
        values.put(TokenContract.TokenEntry.COLUMN_NAME_EXPIRES, expirationTime);

        database.insert(TokenContract.TokenEntry.TABLE_NAME, null, values);
    }

    public void updateTokens(AccessTokenResponse accessTokenResponse) {

        updateAccessToken(accessTokenResponse.getAccessToken(),
                          getExpirationTime(accessTokenResponse.getExpiresIn()));

        updateRefreshToken(accessTokenResponse.getRefreshToken(),
                           getExpirationTime(accessTokenResponse.getRefreshExpiresIn()));
    }

    private void updateAccessToken(String accessToken,
                                   long expirationTime) {

        ContentValues values = new ContentValues();
        values.put(TokenContract.TokenEntry.COLUMN_NAME_TYPE, TokenType.ACCESS.name());
        values.put(TokenContract.TokenEntry.COLUMN_NAME_TOKEN, accessToken);
        values.put(TokenContract.TokenEntry.COLUMN_NAME_EXPIRES, expirationTime);

        String whereClause = TokenContract.TokenEntry.COLUMN_NAME_TYPE + " = ?";
        database.update(TokenContract.TokenEntry.TABLE_NAME,
                        values,
                        whereClause,
                        new String[] { TokenType.ACCESS.name() });
    }

    private void updateRefreshToken(String refreshToken,
                                    long expirationTime) {

        ContentValues values = new ContentValues();
        values.put(TokenContract.TokenEntry.COLUMN_NAME_TYPE, TokenType.REFRESH.name());
        values.put(TokenContract.TokenEntry.COLUMN_NAME_TOKEN, refreshToken);
        values.put(TokenContract.TokenEntry.COLUMN_NAME_EXPIRES, expirationTime);

        String whereClause = TokenContract.TokenEntry.COLUMN_NAME_TYPE + " = ?";
        database.update(TokenContract.TokenEntry.TABLE_NAME,
                        values,
                        whereClause,
                        new String[] { TokenType.REFRESH.name() });
    }

    public void deleteTokens() {

        database.delete(TokenContract.TokenEntry.TABLE_NAME, null, null);
    }

    private long getExpirationTime(int expiresIn) {

        long expirationTime = new Date().getTime();
        expirationTime += TimeUnit.MILLISECONDS.convert(expiresIn, TimeUnit.SECONDS);
        expirationTime -= TimeUnit.MILLISECONDS.convert(10, TimeUnit.MINUTES);

        return expirationTime;
    }

}
