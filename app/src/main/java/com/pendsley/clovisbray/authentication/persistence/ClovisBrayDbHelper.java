package com.pendsley.clovisbray.authentication.persistence;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.pendsley.clovisbray.persistence.dao.AccountContract;
import com.pendsley.clovisbray.persistence.dao.InventoryContract;
import com.pendsley.clovisbray.persistence.dao.LoadoutContract;
import com.pendsley.clovisbray.persistence.dao.ManifestContract;
import com.pendsley.clovisbray.persistence.dao.SocketContract;
import com.pendsley.clovisbray.persistence.dao.StatContract;
import com.pendsley.clovisbray.persistence.dao.TalentNodeContract;

/**
 * Handles opening local database.
 *
 * @author Phil Endsley
 */
public class ClovisBrayDbHelper extends SQLiteOpenHelper {

    // Changes to the database schema require an increment to the database version
    private static final int DATABASE_VERSION = 4;

    private static final String DATABASE_NAME = "ClovisBray.db";

    private static final String SQL_CREATE_AUTH_TOKENS = "CREATE TABLE " + TokenContract.TokenEntry.TABLE_NAME + " (" +
            TokenContract.TokenEntry._ID + " INTEGER PRIMARY KEY, " +
            TokenContract.TokenEntry.COLUMN_NAME_TYPE + " TEXT, " +
            TokenContract.TokenEntry.COLUMN_NAME_TOKEN + " TEXT, " +
            TokenContract.TokenEntry.COLUMN_NAME_EXPIRES + " INTEGER)";

    private static final String SQL_DELETE_AUTH_TOKENS = "DROP TABLE IF EXISTS " + TokenContract.TokenEntry.TABLE_NAME;

    private static final String SQL_CREATE_MANIFEST = "CREATE TABLE " + ManifestContract.ManifestEntry.TABLE_NAME + " (" +
            ManifestContract.ManifestEntry._ID + " INTEGER PRIMARY KEY, " +
            ManifestContract.ManifestEntry.COLUMN_NAME_VERSION + " TEXT)";

    private static final String SQL_DELETE_MANIFEST = "DROP TABLE IF EXISTS " + ManifestContract.ManifestEntry.TABLE_NAME;

    private static final String SQL_CREATE_ACCOUNT = "CREATE TABLE " + AccountContract.AccountEntry.TABLE_NAME + " (" +
            AccountContract.AccountEntry._ID + " INTEGER PRIMARY KEY, " +
            AccountContract.AccountEntry.COLUMN_NAME_DISPLAY_NAME + " TEXT, " +
            AccountContract.AccountEntry.COLUMN_NAME_MEMBERSHIP_ID + " TEXT, " +
            AccountContract.AccountEntry.COLUMN_NAME_MEMBERSHIP_TYPE + " INTEGER)";

    private static final String SQL_DELETE_ACCOUNT = "DROP TABLE IF EXISTS " + AccountContract.AccountEntry.TABLE_NAME;

    private static final String SQL_CREATE_CHARACTER = "CREATE TABLE " + CharacterContract.CharacterEntry.TABLE_NAME + " (" +
            CharacterContract.CharacterEntry._ID + " INTEGER PRIMARY KEY, " +
            CharacterContract.CharacterEntry.COLUMN_NAME_MEMBERSHIP_ID + " TEXT, " +
            CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_ID + " TEXT, " +
            CharacterContract.CharacterEntry.COLUMN_NAME_EMBLEM_PATH + " TEXT, " +
            CharacterContract.CharacterEntry.COLUMN_NAME_BACKGROUND_PATH + " TEXT, " +
            CharacterContract.CharacterEntry.COLUMN_NAME_CLASS_HASH + " TEXT, " +
            CharacterContract.CharacterEntry.COLUMN_NAME_CHARACTER_CLASS + " TEXT, " +
            CharacterContract.CharacterEntry.COLUMN_NAME_LIGHT_LEVEL + " INTEGER, " +
            CharacterContract.CharacterEntry.COLUMN_CHARACTER_INDEX + " INTEGER)";

    private static final String SQL_DELETE_CHARACTER = "DROP TABLE IF EXISTS " + CharacterContract.CharacterEntry.TABLE_NAME;

    private static final String SQL_CREATE_LOCAL_INVENTORY = "CREATE TABLE " + InventoryContract.InventoryEntry.TABLE_NAME + " (" +
            InventoryContract.InventoryEntry._ID + " INTEGER PRIMARY KEY, " +
            InventoryContract.InventoryEntry.COLUMN_NAME_ITEM_INSTANCE_ID + " TEXT, " +
            InventoryContract.InventoryEntry.COLUMN_NAME_ITEM_HASH + " TEXT, " +
            InventoryContract.InventoryEntry.COLUMN_NAME_ITEM_NAME + " TEXT, " +
            InventoryContract.InventoryEntry.COLUMN_NAME_LIGHT + " INTEGER, " +
            InventoryContract.InventoryEntry.COLUMN_CHARACTER_ID + " TEXT, " +
            InventoryContract.InventoryEntry.COLUMN_ITEM_TYPE_NAME +  " TEXT, " +
            InventoryContract.InventoryEntry.COLUMN_ITEM_CATEGORY + " TEXT, " +
            InventoryContract.InventoryEntry.COLUMN_ITEM_CLASS_TYPE + " TEXT, " +
            InventoryContract.InventoryEntry.COLUMN_EQUIPPED + " INTEGER, " +
            InventoryContract.InventoryEntry.COLUMN_DAMAGE_TYPE + " TEXT, " +
            InventoryContract.InventoryEntry.COLUMN_DAMAGE_TYPE_ICON + " TEXT, " +
            InventoryContract.InventoryEntry.COLUMN_MASTER_WORKS + " TEXT)";


    private static final String SQL_DELETE_LOCAL_INVENTORY = "DROP TABLE IF EXISTS " + InventoryContract.InventoryEntry.TABLE_NAME;

    private static final String SQL_CREATE_TALENT_NODES = "CREATE TABLE " + TalentNodeContract.TalentNodeEntry.TABLE_NAME + " (" +
            TalentNodeContract.TalentNodeEntry._ID + " INTEGER PRIMARY KEY, " +
            TalentNodeContract.TalentNodeEntry.COLUMN_NAME_ITEM_INSTANCE_ID + " TEXT, " +
            TalentNodeContract.TalentNodeEntry.COLUMN_NAME_NODE_STEP_NAME + " TEXT, " +
            TalentNodeContract.TalentNodeEntry.COLUMN_NAME_NODE_STEP_DESCRIPTION + " TEXT, " +
            TalentNodeContract.TalentNodeEntry.COLUMN_NAME_NODE_ICON + " TEXT, " +
            TalentNodeContract.TalentNodeEntry.COLUMN_NAME_STEP_INDEX + " INTEGER, " +
            TalentNodeContract.TalentNodeEntry.COLUMN_ROW + " INTEGER, " +
            TalentNodeContract.TalentNodeEntry.COLUMN_COLUMN + " INTEGER)";

    private static final String SQL_DELETE_TALENT_NODES = "DROP TABLE IF EXISTS " + TalentNodeContract.TalentNodeEntry.TABLE_NAME;

    private static final String SQL_CREATE_STATS = "CREATE TABLE " + StatContract.StatEntry.TABLE_NAME + " (" +
            StatContract.StatEntry._ID + " INTEGER PRIMARY KEY, " +
            StatContract.StatEntry.COLUMN_INSTANCE_ITEM_ID + " TEXT, " +
            StatContract.StatEntry.COLUMN_NAME + " TEXT, " +
            StatContract.StatEntry.COLUMN_VALUE + " INTEGER, " +
            StatContract.StatEntry.COLUMN_MINIMUM_VALUE + " INTEGER, " +
            StatContract.StatEntry.COLUMN_MAXIMUM_VALUE + " INTEGER)";

    private static final String SQL_DELETE_STATS = "DROP TABLE IF EXISTS " + StatContract.StatEntry.TABLE_NAME;

    private static final String SQL_CREATE_LOADOUTS = "CREATE TABLE " + LoadoutContract.LoadoutEntry.TABLE_NAME + " (" +
            LoadoutContract.LoadoutEntry._ID + " INTEGER PRIMARY KEY, " +
            LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_NAME + " TEXT, " +
            LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_CHARACTER_ID + " TEXT, " +
            LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_PRIMARY_INSTANCE + " TEXT, " +
            LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_SPECIAL_INSTANCE + " TEXT, " +
            LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_HEAVY_INSTANCE + " TEXT, " +
            LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_GHOST_INSTANCE + " TEXT, " +
            LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_HELMET_INSTANCE + " TEXT, " +
            LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_GAUNTLETS_INSTANCE + " TEXT, " +
            LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_CHEST_INSTANCE + " TEXT, " +
            LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_LEGS_INSTANCE + " TEXT, " +
            LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_CLASS_ITEM_INSTANCE + " TEXT, " +
            LoadoutContract.LoadoutEntry.COLUMN_LOADOUT_ARTIFACT_INSTANCE + " TEXT)";

    private static final String SQL_DELETE_LOADOUTS = "DROP TABLE IF EXISTS " + LoadoutContract.LoadoutEntry.TABLE_NAME;

    private static final String SQL_CREATE_SOCKETS = "CREATE TABLE " + SocketContract.SocketEntry.TABLE_NAME + "(" +
            SocketContract.SocketEntry._ID + " INTEGER PRIMARY KEY, " +
            SocketContract.SocketEntry.COLUMN_ITEM_INSTANCE_ID + " TEXT, " +
            SocketContract.SocketEntry.COLUMN_SOCKET_HASH + " TEXT, " +
            SocketContract.SocketEntry.COLUMN_COLUMN_ORDER + " INTEGER, " +
            SocketContract.SocketEntry.COLUMN_ROW_ORDER + " INTEGER, " +
            SocketContract.SocketEntry.COLUMN_ENABLED + " INTEGER)";

    private static final String SQL_DELETE_SOCKETS = "DROP TABLE IF EXISTS " + SocketContract.SocketEntry.TABLE_NAME;

    public ClovisBrayDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_AUTH_TOKENS);
        db.execSQL(SQL_CREATE_MANIFEST);
        db.execSQL(SQL_CREATE_ACCOUNT);
        db.execSQL(SQL_CREATE_CHARACTER);
        db.execSQL(SQL_CREATE_LOCAL_INVENTORY);
        db.execSQL(SQL_CREATE_TALENT_NODES);
        db.execSQL(SQL_CREATE_STATS);
        db.execSQL(SQL_CREATE_LOADOUTS);
        db.execSQL(SQL_CREATE_SOCKETS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion <= 2 && newVersion >= 3) {
            db.execSQL(SQL_CREATE_SOCKETS);
        }
        if (oldVersion <= 3 && newVersion >= 4) {
            db.execSQL(SQL_DELETE_LOCAL_INVENTORY);
            db.execSQL(SQL_CREATE_LOCAL_INVENTORY);
            db.execSQL(SQL_DELETE_SOCKETS);
            db.execSQL(SQL_CREATE_SOCKETS);
        }
    }

}
