package com.pendsley.clovisbray.authentication.model;

import com.google.gson.annotations.SerializedName;

/**
 * OAuth token data.
 *
 * @author Phil Endsley
 */
public class AccessTokenResponse {

    @SerializedName("access_token")
    private String accessToken;

    @SerializedName("token_type")
    private String tokenType;

    @SerializedName("expires_in")
    private int expiresIn;

    @SerializedName("refresh_token")
    private String refreshToken;

    @SerializedName("refresh_expires_in")
    private int refreshExpiresIn;

    @SerializedName("membership_id")
    private String bungieNetMembershipId;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public int getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(int expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public int getRefreshExpiresIn() {
        return refreshExpiresIn;
    }

    public void setRefreshExpiresIn(int refreshExpiresIn) {
        this.refreshExpiresIn = refreshExpiresIn;
    }

    public String getBungieNetMembershipId() {
        return bungieNetMembershipId;
    }

    public void setBungieNetMembershipId(String bungieNetMembershipId) {
        this.bungieNetMembershipId = bungieNetMembershipId;
    }
}
