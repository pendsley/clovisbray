package com.pendsley.clovisbray.authentication.persistence;

import android.provider.BaseColumns;

/**
 * TODO
 */
public class TokenContract {

    private TokenContract() {
    }

    public static class TokenEntry implements BaseColumns {
        public static final String TABLE_NAME = "auth_tokens";
        public static final String COLUMN_NAME_TOKEN = "token";
        public static final String COLUMN_NAME_TYPE = "type";
        public static final String COLUMN_NAME_EXPIRES = "expires";
        public static final String COLUMN_NAME_BUNGLED = "bungled";
    }
}
