package com.pendsley.clovisbray.authentication.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.pendsley.clovisbray.ClovisBrayApplication;
import com.pendsley.clovisbray.authentication.BungieAuthenticationService;
import com.pendsley.clovisbray.authentication.ReinjectFieldsEvent;
import com.pendsley.clovisbray.authentication.model.AccessTokenResponse;
import com.pendsley.clovisbray.authentication.persistence.operation.SaveTokenOperation;
import com.pendsley.clovisbray.inject.AuthenticationModule;
import com.pendsley.clovisbray.util.BusProvider;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Response;

/**
 * Broadcast receiver to handle refreshing the access tokens. 
 *
 * @author Phil Endsley
 */
public class TokenRefreshReceiver extends BroadcastReceiver {

    public static final String TOKEN_REFRESH_ACTION = "com.pendsley.clovisBray.AUTH_TOKENS";

    @Inject
    BungieAuthenticationService authenticationService;

    @Inject
    SaveTokenOperation saveTokenOperation;

    @Inject
    @Named(AuthenticationModule.REFRESH_TOKEN)
    String refreshToken;

    @Inject
    @Named(AuthenticationModule.ACCESS_TOKEN_EXPIRATION_TIME)
    Long accessExpirationTime;

    @Inject
    BusProvider busProvider;

    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
    private Future refreshFuture;

    @Override
    public void onReceive(Context context, Intent intent) {

        if (!TOKEN_REFRESH_ACTION.equals(intent.getAction())) {
            return;
        }

        ClovisBrayApplication application = (ClovisBrayApplication) context.getApplicationContext();
        application.getAuthenticationComponent().inject(this);

        long delay = accessExpirationTime - System.currentTimeMillis();

        if (refreshFuture != null) {
            refreshFuture.cancel(true);
        }

        refreshFuture = executorService.schedule(new RefreshTokensRunnable(authenticationService,
                                                                           refreshToken,
                                                                           saveTokenOperation,
                                                                           busProvider),
                                                 delay,
                                                 TimeUnit.MILLISECONDS);
    }

    private static final class RefreshTokensRunnable implements Runnable {

        private static final int MAX_RETRY_ATTEMPTS = 5;

        private final BungieAuthenticationService authenticationService;
        private final String refreshToken;
        private final SaveTokenOperation saveTokenOperation;
        private final BusProvider busProvider;

        private int retryCount = 0;

        RefreshTokensRunnable(BungieAuthenticationService authenticationService,
                              String refreshToken,
                              SaveTokenOperation saveTokenOperation,
                              BusProvider busProvider) {
            this.authenticationService = authenticationService;
            this.refreshToken = refreshToken;
            this.saveTokenOperation = saveTokenOperation;
            this.busProvider = busProvider;
        }

        @Override
        public void run() {

            try {
                Response<AccessTokenResponse> response =
                        authenticationService.getAccessTokensFromRefreshToken("refresh_token",
                                                                              refreshToken).execute();

                AccessTokenResponse accessTokenResponse = response.body();

                saveTokenOperation.updateTokens(accessTokenResponse);

                busProvider.getBus().post(new ReinjectFieldsEvent(true));

            } catch (IOException e) {
                Log.e("TokenRefreshReceiver", "IOException on attempt: " + retryCount, e);
                retryCount++;
                if (retryCount < MAX_RETRY_ATTEMPTS) {
                    run();
                }
            }
        }
    }

}
