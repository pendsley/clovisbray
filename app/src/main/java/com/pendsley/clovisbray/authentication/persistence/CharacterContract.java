package com.pendsley.clovisbray.authentication.persistence;

import android.provider.BaseColumns;

/**
 * TODO
 */
public class CharacterContract {

    private CharacterContract() {
    }

    public static class CharacterEntry implements BaseColumns {

        public static final String TABLE_NAME = "character";
        public static final String COLUMN_NAME_MEMBERSHIP_ID = "membership_id";
        public static final String COLUMN_NAME_CHARACTER_ID = "character_id";
        public static final String COLUMN_NAME_EMBLEM_PATH = "emblem_path";
        public static final String COLUMN_NAME_BACKGROUND_PATH = "background_path";
        public static final String COLUMN_NAME_CLASS_HASH = "class_hash";
        public static final String COLUMN_NAME_CHARACTER_CLASS = "character_class"; // TODO - Delete
        public static final String COLUMN_NAME_LIGHT_LEVEL = "light_level";
        public static final String COLUMN_CHARACTER_INDEX = "character_index";
    }
}
