package com.pendsley.clovisbray;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pendsley.clovisbray.model.UserInfo;
import com.pendsley.clovisbray.persistence.dao.AccountOperation;
import com.pendsley.clovisbray.presenter.PlatformSelectionPresenter;
import com.pendsley.clovisbray.ui.DividerItemDecorator;
import com.pendsley.clovisbray.util.BusProvider;
import com.pendsley.clovisbray.viewcontroller.PlatformSelectionViewController;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

/**
 * Activity for changing the user's platform.
 *
 * @author Phil Endsley
 */
public class PlatformSelectionActivity extends AppCompatActivity implements PlatformSelectionViewController {

    @Inject
    BusProvider busProvider;

    @Inject
    AccountOperation accountOperation;

    @Inject
    PlatformSelectionPresenter platformSelectionPresenter;


    private View container;
    private RecyclerView platformRecyclerView;
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        injectFields();
        
        setContentView(R.layout.activity_platform_selection);

        container  = findViewById(R.id.platform_container);
        platformRecyclerView = findViewById(R.id.platform_list);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        platformRecyclerView.setLayoutManager(layoutManager);
        platformRecyclerView.addItemDecoration(new DividerItemDecorator(this));

        
        platformSelectionPresenter.setPlatformSelectionViewController(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        busProvider.getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        busProvider.getBus().unregister(this);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        platformSelectionPresenter.loadPlatformUsers();
    }

    @Override
    public void displayPlatforms(List<UserInfo> platformUserInfos) {

        PlatformUserAdapter adapter = new PlatformUserAdapter(platformUserInfos);
        platformRecyclerView.setAdapter(adapter);
    }

    @SuppressLint("WrongConstant")
    @Override
    public void launchManifestActivity() {

        Intent intent = new Intent(this, InitialSyncActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }

    @Override
    public void displayError(int errorMessageResourceId) {
        @SuppressWarnings("deprecation") // API requirements
        final Snackbar snackbar = Snackbar.make(container,
                errorMessageResourceId,
                Snackbar.LENGTH_INDEFINITE)
                .setActionTextColor(getResources().getColor(R.color.secondary_text));

        snackbar.setAction(R.string.dismiss, view -> snackbar.dismiss());

        snackbar.show();
    }

    @SuppressLint("WrongConstant")
    private void platformSelected(UserInfo userInfo) {

        accountOperation.deleteUserInfo();
        accountOperation.deleteCharacterInfo();

        accountOperation.saveUserInfo(userInfo);
        platformSelectionPresenter.retrieveAndSaveCharacters(userInfo);
    }

    private void injectFields() {
        ((ClovisBrayApplication) getApplication()).getApiComponent().inject(this);
    }

    private class PlatformUserAdapter extends RecyclerView.Adapter<PlatformUserHolder> {

        private final List<UserInfo> platformUserInfos;

        PlatformUserAdapter(List<UserInfo> platformUserInfos) {
            this.platformUserInfos = platformUserInfos;
        }

        @Override
        public PlatformUserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_platform_user, parent, false);
            return new PlatformUserHolder(view);
        }

        @Override
        public void onBindViewHolder(PlatformUserHolder holder, int position) {
            holder.bindData(platformUserInfos.get(position));
        }

        @Override
        public int getItemCount() {
            return platformUserInfos.size();
        }
    }

    private final class PlatformUserHolder extends RecyclerView.ViewHolder {

        private final ImageView platformIconView;
        private final TextView userView;
        private final TextView platformNameView;

        PlatformUserHolder(View itemView) {
            super(itemView);

            platformIconView = itemView.findViewById(R.id.platform_icon);
            userView = itemView.findViewById(R.id.platform_user_name);
            platformNameView = itemView.findViewById(R.id.platform_name);
        }

        void bindData(UserInfo userInfo) {
            userView.setText(userInfo.getDisplayName());
            bindPlatformIcon(userInfo.getMembershipType());

            itemView.setOnClickListener(null);
            itemView.setOnClickListener(view -> platformSelected(userInfo));
        }

        void bindPlatformIcon(int membershipType) {

            int platformLogoResourceId = 0;
            int platformNameResource = 0;
            switch (membershipType) {
                case 1:
                    platformLogoResourceId = R.drawable.ic_xbox_logo;
                    platformNameResource = R.string.xbox;
                    break;
                case 2:
                    platformLogoResourceId = R.drawable.ic_playstation_logo;
                    platformNameResource = R.string.playstation;
                    break;
                case 4:
                    platformLogoResourceId = R.drawable.ic_battlenet_logo;
                    platformNameResource = R.string.battlenet;
                    break;
            }

            Picasso.with(platformIconView.getContext()).load(platformLogoResourceId).into(platformIconView);
            platformNameView.setText(platformNameResource);
        }
    }
}
